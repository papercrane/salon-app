from django.db import models
from erp_core.models import ErpAbstractBaseModel


class Discount(ErpAbstractBaseModel):
    name = models.CharField(max_length=255)
    percentage = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    minimum_amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    offer_count = models.IntegerField(null=True, blank=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        permissions = (
            ("view_discount_list", "Can view discount list"),
            
        )
