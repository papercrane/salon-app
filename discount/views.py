from django.shortcuts import render
from discount.models import Discount
from .filters import DiscountFilter
from erp_core.pagination import paginate
from django.contrib.auth.decorators import login_required, permission_required

# Create your views here.

@login_required
@permission_required('discount.view_discount_list', raise_exception=True)
def list(request):

	f = DiscountFilter(request.GET, queryset=Discount.objects.all().order_by('-id'))

	discounts, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
	context = {
		"discounts": discounts,
		'pagination': pagination,
		'filter': f,
	}
	return render(request, 'discount/list.html', context)


