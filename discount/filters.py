import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from .models import *
# from asset.stock.models import ItemAttribute
# from employee.models import Employee


class DiscountFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Discount
        fields = ['name']
