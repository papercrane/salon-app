import math

from django.conf import settings

from globalconfig.models import *
# setting up the configuration


def globalconfigs(request):
    global_var, created = GlobalConfig.objects.get_or_create(pk=1)
    ENQUIRY = global_var.enquiry
    QUOTATION = global_var.quotation
    SO = global_var.so,
    PO = global_var.po,
    INVOICE = global_var.invoice
    INVENTORY_PO = global_var.inventory_po
    ONLY_INVENTORY = global_var.only_inventory
    TIMESHEET = global_var.timesheet

    context = {
        'global_var': global_var,
        'ENQUIRY': ENQUIRY,
        'QUOTATION': QUOTATION,
        'SO': SO,
        'PO': PO,
        'INVOICE': INVOICE,
        'INVENTORY_PO': INVENTORY_PO,
        'ONLY_INVENTORY': ONLY_INVENTORY,
        'TIMESHEET': TIMESHEET,
    }
    return context

# def isQuotation():
#     global_value = GlobalConfig.objects.first()
#     return global_value.quotation
