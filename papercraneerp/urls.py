from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from sitetree.sitetreeapp import compose_dynamic_tree, register_dynamic_trees
from erp_admin import views as erpAdminViews
from rest_framework.authtoken import views

# from employee import views as employeeViews
from papercraneerp import allauth

urlpatterns = [
                  url(r'^$', erpAdminViews.dashboard),
                  url(r'^api-token-auth/', views.obtain_auth_token),
                  url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                  url(r'^admin/', admin.site.urls),
                  url(r'^accounts/password/change/$', allauth.login_after_password_change,
                      name='account_change_password'),  # Place before allauth urls to override
                  url(r'^accounts/', include('allauth.urls')),
                  url(r'^', include('employee.urls', namespace="employee")),
                  url(r'^', include('customer.urls', namespace="customer")),
                  url(r'^', include('product.urls', namespace="product")),
                  url(r'^chaining/', include('smart_selects.urls')),
                  url(r'^', include('service.urls', namespace="service")),
                  url(r'^', include('location.urls', namespace="location")),
                  url(r'^', include('invoice.urls', namespace="invoice")),
                  url(r'^', include('supplier.urls', namespace="supplier")),
                  url(r'^expense/', include('expense.urls', namespace="expense")),
                  url(r'^company/', include('company.urls', namespace="company")),
                  url(r'^vendor/', include('vendor.urls', namespace="vendor")),
                  url(r'^bill/', include('bill.urls', namespace="bill")),
                  url(r'^calendar/', include('calendarApp.urls', namespace="calendarApp")),
                  url(r'^discount/', include('discount.urls', namespace="discount")),
                  url(r'^package/', include('package.urls', namespace="package")),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
admin.site.site_header = 'Saloon Administration'

register_dynamic_trees(
    compose_dynamic_tree('erp_admin'),
    compose_dynamic_tree('employee'),
    compose_dynamic_tree('service'),
    compose_dynamic_tree('product'),
    compose_dynamic_tree('package'),
    compose_dynamic_tree('discount'),
    compose_dynamic_tree('supplier'),
    compose_dynamic_tree('vendor'),
    compose_dynamic_tree('customer'),
    compose_dynamic_tree('calendarApp'),
    compose_dynamic_tree('invoice'),
    compose_dynamic_tree('expense'),

)
