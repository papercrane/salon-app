from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.views import PasswordChangeView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.urls import reverse_lazy


class AccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request, success_url=None):
        if success_url is not None:
            return success_url

        if request.user:
            return reverse('employee:profile', args=[request.user.employee.code])
        else:
            return reverse('home')

class LoginAfterPasswordChangeView(PasswordChangeView):
    @property
    def success_url(self):
        return reverse_lazy('account_login')

login_after_password_change = login_required(LoginAfterPasswordChangeView.as_view())