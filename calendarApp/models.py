from django.contrib.auth.models import User
from django.db import models

from employee.models import Employee
from erp_core.models import ErpAbstractBaseModel
from location.models import Office
from service.models import Service
from erp_core.models import ErpAbstractBaseModel, PaymentTerm, BaseModel, BaseCodeSetting

# class AppointmentCodeSetting(ErpAbstractBaseModel):
#     count = models.IntegerField(default=0)
#
#     @classmethod
#     def new_code(cls):
#         code = ""
#         if cls.objects.exists():
#             code_setting = cls.objects.last()
#             code = code_setting.generateCode()
#         return code
#
#     def generateCode(self):
#         count = self.count
#         return str("#AP") + str(format(count, '0'))
#
#     @classmethod
#     def incrementCountIndex(cls):
#         if cls.objects.exists():
#             code_setting = cls.objects.last()
#             code_setting.count = models.F('count') + 1
#             code_setting.save()

class AppointmentCodeSetting(BaseCodeSetting):
    pass

class Appointment(ErpAbstractBaseModel):
    NEW = 'New'
    STARTED = 'Started'
    ONGOING = 'Ongoing'
    COMPLETED = 'Completed'
    CANCELLED = 'Cancelled'
    NOSHOWS = 'Noshows'
    CLOSED = 'Closed'
    STATUS = (
        (NEW, 'New'),
        (STARTED, 'Started'),
        (ONGOING, 'Ongoing'),
        (COMPLETED, 'Completed'),
        (CANCELLED, 'Cancelled'),
        (NOSHOWS, 'Noshows'),
        (CLOSED, 'Closed')
    )

    def newCode():
        return AppointmentCodeSetting.new_code()

    code = models.CharField(max_length=100, unique=True, default=newCode)
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE,
                                 related_name="date_customers")
    # employee = models.ForeignKey('employee.Employee')
    # service = models.ForeignKey('service.Service')
    appointment_date_time = models.DateTimeField()
    other_details = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(max_length=200, choices=STATUS, default=NEW)
    office = models.ForeignKey(Office)
    created_by = models.ForeignKey(User, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.code:
            AppointmentCodeSetting.incrementCountIndex()
        return super(Appointment, self).save(*args, **kwargs)

    class Meta:
        ordering = ('appointment_date_time',)

    class Meta:
        permissions = (
            ("view_appointment_list", "Can view appointment list"),

        )


class AppointmentService(ErpAbstractBaseModel):
    employee = models.ForeignKey(Employee, null=True, blank=True)
    service = models.ForeignKey(Service)
    appointment = models.ForeignKey(Appointment, related_name='services')
