from django.contrib import admin
from .models import Appointment, AppointmentCodeSetting,AppointmentService

# Register your models here.
admin.site.register(Appointment)
admin.site.register(AppointmentCodeSetting)
admin.site.register(AppointmentService)
