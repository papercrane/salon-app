from django.conf.urls import url
from . import views

app_name = 'calendarapp'

urlpatterns = [
    url(r'^send_whatsapp/(?P<invoice>[^/]+)/$', views.send_whatsapp, name="send_whatsapp"),
    url(r'^send_sms/(?P<invoice>[^/]+)/$', views.send_sms, name="send_sms"),
    url(r'^appointments/history/$', views.dateList, name='list'),
    url(r'^editstatus/(?P<pk>[^/]+)/$', views.editstatus, name='editstatus'),
    url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<pk>[^/]+)/detail/$', views.detail, name='detail'),
    url(r'^(?P<pk>[^/]+)/delete/$', views.delete, name='delete'),
    url(r'^dashboard/$', views.date, name='date'),
    url(r'^appointment/$', views.dashboard, name='dashboard'),
    url(r'^create/$', views.create, name="create"),
    url(r'^(?P<date>[-\w]+)/$', views.dateList, name='list'),
    url(r'^appointment/code/settings/$', views.code_settings, name='code_settings'),
]
