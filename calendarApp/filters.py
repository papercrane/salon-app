import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from calendarApp.models import Appointment


class DateFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(label="title", lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))


    created_at = django_filters.DateFilter(label="Created date", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    appoinment_date_time = django_filters.ChoiceFilter(label="Created time", name="appoinment_date_time", method='order_by_field', choices=(
        ('appoinment_date_time', 'Descending')), widget=forms.Select(attrs={'class': 'form-control'}))

    customer = django_filters.CharFilter(label="Customer name", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    phone = django_filters.CharFilter(label="Customer phone", lookup_expr='icontains',
                                         widget=forms.TextInput(attrs={'class': 'form-control'}))

    email = django_filters.CharFilter(label="Customer email", lookup_expr='icontains',
                                         widget=forms.TextInput(attrs={'class': 'form-control'}))


    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    search = django_filters.CharFilter(label="Customer name", method="filter_search", lookup_expr='icontains',
                                       widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Appointment
        fields = ['search', ]

    def filter_search(self, queryset, search, value):
        return queryset.filter(
            Q(customer__name__icontains=value) | Q(customer__phone__contains=value) | Q(customer__email__icontains=value)
        )

    # def filter_by_contact(self, queryset, name, value):
    #     return queryset.filter(
    #         Q(contacts__name__icontains=value) | Q(contacts__customer__name__icontains=value)
    #     )

    # def order_by_field(self, queryset, name, value):
        # return queryset.order_by(value)
