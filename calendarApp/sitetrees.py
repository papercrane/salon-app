import datetime

from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Calendar',
            'calendarApp:date',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="calendar",
        ),
        item(
            'Appointments',
            'calendarApp:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="check",
        ),

    ]),
    # ... You can define more than one tree for your app.

    tree('sidebar_qusais_admin', items=[
        # Then define items and their children with `item` function.
        item(
            'Calendar',
            'calendarApp:date',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="calendar",
        ),
        item(
            'Appointments',
            'calendarApp:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="check",
        ),


    ]),
)
