from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django import forms

from customer.models import Customer
from employee.models import Employee
from .models import Appointment, AppointmentService, AppointmentCodeSetting


def clean_status(status):
   try:
       if status == "closed":
           raise ValidationError('You cannot select "closed" without creating an invoice!')

   except (ValueError, TypeError):
       raise ValidationError('You cannot select "closed" without creating an invoice!')

   return status

class DateForm(ModelForm):

    status = forms.ChoiceField(choices=Appointment.STATUS, widget=forms.Select(attrs={'class': "form-control sel2", 'required': "required", 'id': "status"}))

    class Meta:
        model = Appointment
        widgets = {
            "other_details": forms.TextInput(attrs={'class': "form-control", 'id': "note"}),
            "customer": forms.Select(attrs={'class': "form-control sel2", 'required': "required", 'id': "customer"}),
            "appointment_date_time": forms.DateInput(attrs={'class': "form-control tdate", "id": "required_date"}),
            # "status": forms.Select(attrs={'class': "form-control sel2", 'required': "required", 'id': "status"}),
        }
        fields = ['customer', 'other_details', 'status', 'appointment_date_time']

    def __init__(self, *args, **kwargs):
        customer = kwargs.pop('customer', None)
        super(DateForm, self).__init__(*args, **kwargs)
        if customer is not None:
            choices = []
            customer = Customer.objects.get(pk=customer)
            choices.append((customer.id, customer.name))
            self.fields['customer'].choices = choices


class AppointmentForm(ModelForm):
    class Meta:
        model = Appointment
        widgets = {
            "other_details": forms.TextInput(attrs={'class': "form-control", }),
            "appointment_date_time": forms.DateInput(attrs={'class': "form-control tdate", }),
            "status": forms.Select(attrs={'class': "form-control sel2", }),
        }
        fields = ['other_details', 'status', 'appointment_date_time']


class AppointmentServiceForm(ModelForm):
    class Meta:
        widgets = {
            "service": forms.Select(attrs={'class': "form-control"}),
            "employee": forms.Select(attrs={'class': "form-control"}),
        }
        model = AppointmentService
        fields = ['employee', 'service']

    def __init__(self,*args, **kwargs):
        super(AppointmentServiceForm, self).__init__(*args, **kwargs)
        self.fields['employee'].queryset = Employee.objects.exclude(primary_designation__name__in=["Qusais_Admin","Admin"])

# class AppointmentCodeSettingsForm(ModelForm):
#     class Meta:
#         model = AppointmentCodeSetting
#
#         widgets = {
#             "count": forms.NumberInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
#         }
#
#         fields = ['count']


class AppointmentCodeSettingsForm(ModelForm):
    class Meta:
      model = AppointmentCodeSetting
      widgets = {"prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
                 "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
                 "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
                 }
      fields = ['prefix', 'count_index', 'no_of_characters']
