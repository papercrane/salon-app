# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2019-03-18 10:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calendarApp', '0006_auto_20190318_1005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='status',
            field=models.CharField(choices=[('New', 'New'), ('Started', 'Started'), ('Ongoing', 'Ongoing'), ('Completed', 'Completed'), ('Cancelled', 'Cancelled'), ('Noshows', 'Noshows'), ('Closed', 'Closed')], default='New', max_length=200),
        ),
    ]
