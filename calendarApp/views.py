import datetime
import string
import random

import requests
from django.contrib import messages
from django.db import transaction
from django.forms import inlineformset_factory

from erp_core.pagination import paginate
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect, HttpResponse
from datetime import datetime as dtime
from django.shortcuts import get_object_or_404, render

from invoice.models import Invoice
from .models import Appointment, AppointmentService, AppointmentCodeSetting
from .forms import DateForm, AppointmentServiceForm, AppointmentCodeSettingsForm
from .filters import DateFilter
from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import EmailMessage


@login_required
def date(request):
    dates = Appointment.objects.all()
    context = {
        'dates': dates,
    }
    return render(request, 'calapp/calapp.html', context)


@login_required
# @permission_required('calendarapp.view_appointment_list', raise_exception=True)
def detail(request, pk):
    dates = Appointment.objects.get(pk=pk)
    context = {
        'dates': dates,
        'statuses': Appointment.STATUS,
    }
    return render(request, 'calapp/detail.html', context)


@login_required
# @permission_required('calendarapp.view_appointment_list', raise_exception=True)
def dateList(request, date=None):
    reqDate = None
    if date:
        qs = Appointment.objects.filter(appointment_date_time__date=date)
        reqDate = '/calendar/' + date + '/'
    else:
        qs = Appointment.objects.all().order_by("-created_at")
        reqDate = ''
    f = DateFilter(request.GET, queryset=qs)
    dates, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    context = {
        'dates': dates,
        'pagination': pagination,
        'filter': f,
        'reqDate': reqDate,
        'current_date': date
    }
    return render(request, "calapp/list.html", context)


@login_required
def dashboard(request):
    now = datetime.datetime.now()
    today = datetime.datetime.strptime(str(now.date()), "%Y-%m-%d")
    tomorrow = today + datetime.timedelta(days=1)
    # invoices = Invoice.objects.filter(paid_date__year=now.year)
    appointments = Appointment.objects.filter(office=request.user.employee.office, appointment_date_time__year=now.year)
    today_appointments = appointments.filter(appointment_date_time__date=today.date()).exclude(
        status=Appointment.CANCELLED)
    tomorrow_appointments = appointments.filter(appointment_date_time__date=tomorrow.date(), status=Appointment.NEW)[
                            :10]
    upcoming_appointments = today_appointments.filter(appointment_date_time__gte=now, status=Appointment.NEW)[:10]
    context = {
        'todays_appointments': today_appointments[:10],
        'tomorrow_appointments': tomorrow_appointments,
        'upcoming_appointments': upcoming_appointments,
        'todays_appointments_count': today_appointments.count(),
        'tomorrow_appointments_count': tomorrow_appointments.count(),
        'upcoming_appointments_count': upcoming_appointments.count(),
        'today': today.date(),
        'tomorrow': tomorrow.date(),

    }
    return render(request, 'calapp/dashboard.html', context)


@login_required
def send_payment_email(request, html_content, subject, email):
    try:
        msg = EmailMessage(subject, html_content, 'info@sinora.com',
                           [email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send(fail_silently=False)
    except Exception as e:
        print(e)


@login_required
# @permission_required('calendarapp.change_appointment', raise_exception=True)
@transaction.atomic
def create(request):
    if request.GET.get('customer'):
        appointment_form = DateForm(request.POST or None, customer=request.GET.get('customer'))
    else:
        appointment_form = DateForm(request.POST or None, customer=None)

    ServiceInlineFormSet = inlineformset_factory(Appointment, AppointmentService, form=AppointmentServiceForm,
                                                 extra=0, can_delete=True, min_num=1, validate_min=True)

    service_formset = ServiceInlineFormSet(request.POST or None)
    if request.method == "POST":
        if appointment_form.is_valid() and service_formset.is_valid():
            appointment = appointment_form.save(commit=False)
            appointment.office = request.user.employee.office
            appointment.created_by = request.user
            appointment.save()
            services = service_formset.save(commit=False)

            for service in services:
                service.appointment = appointment
                service.save()

            html_content = "<p>Hi " + str(
                appointment.customer) + ",</p><p>Thank you! Your appointment on  " + str(
                appointment.appointment_date_time.date()) + "  at  " + str(
                appointment.appointment_date_time.time()) + " has been booked successfully.</p>"
            subject = "Appointment Booked"
            send_payment_email(request, html_content, subject, appointment.customer.email)
            message = "Hi " + str(
                appointment.customer) + ",Thank you! Your appointment with Sinora Beauty Salon  on  " + str(
                appointment.appointment_date_time.date()) + "  at  " + str(
                appointment.appointment_date_time.time()) + " has been booked successfully."

            send_sms(request, message, appointment.customer.phone)
            messages.add_message(request, messages.SUCCESS, 'Appointment added successfully!')

            return HttpResponseRedirect(reverse('calendarapp:list', args=(appointment.appointment_date_time.date(),)))

    context = {
        'appointment_form': appointment_form,
        'form_url': reverse_lazy('calendarapp:create'),
        'type': "Create",
        "service_formset": service_formset
    }
    return render(request, "calapp/edit.html", context)


@login_required
# @permission_required('calendarapp.change_appointment', raise_exception=True)
@transaction.atomic
def edit(request, pk):
    appointment = get_object_or_404(Appointment, id=pk)

    ServiceInlineFormSet = inlineformset_factory(Appointment, AppointmentService, form=AppointmentServiceForm,
                                                 extra=0, can_delete=True, min_num=1, validate_min=True)
    appointment_form = DateForm(request.POST or None, instance=appointment)
    service_formset = ServiceInlineFormSet(request.POST or None, instance=appointment)
    if request.method == "POST":
        if appointment_form.is_valid() and service_formset.is_valid():
            appointment = appointment_form.save(commit=False)
            appointment.office = request.user.employee.office
            appointment.save()

            services = service_formset.save(commit=False)
            for service in services:
                service.appointment = appointment
                service.save()
            messages.add_message(request, messages.SUCCESS, "Appointment edited successfully")
            return HttpResponseRedirect(reverse('calendarapp:list', args=(appointment.appointment_date_time.date(),)))

    context = {
        'appointment_form': appointment_form,
        'form_url': reverse_lazy('calendarapp:edit', args=[appointment.pk]),
        'service_formset': service_formset,
        'type': "Edit",
        'appointment': appointment,
    }
    return render(request, "calapp/edit.html", context)


@login_required
# @permission_required('calendarapp.change_appointment', raise_exception=True)
def editstatus(request, pk):
    if request.method == "POST":
        dateobj = Appointment.objects.get(pk=pk)
        status = request.POST.get('newstatus', None)
        dateobj.status = status
        dateobj.save()
        return HttpResponse(status)


@login_required
@permission_required('calendarapp.delete_appointment', raise_exception=True)
def delete(request, pk):
    dateobj = Appointment.objects.get(pk=pk)
    dateobj.delete()
    messages.add_message(request, messages.SUCCESS, "Appointment deleted successfully")
    return HttpResponseRedirect(reverse('calendarapp:date'))


@login_required
# @permission_required('calendarapp.view_appointment_list', raise_exception=True)
def upcoming_appointment(request):
    dateToday = datetime.date.today()
    date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
    DateObject = Appointment.objects.all()
    upcoming_appointments = DateObject.filter(appointment_date_time__gte=dtime.now(),
                                              appointment_date_time__date=date_1.date(),
                                              office=request.user.employee.office)[:10]
    context = {
        'upcoming_appointments': upcoming_appointments,
    }
    return render(request, 'calapp/upcomingapp.html', context)


@login_required
# @permission_required('calendarapp.view_appointment_list', raise_exception=True)
def todays_appointment(request):
    dateToday = datetime.date.today()
    date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
    DateObject = Appointment.objects.all()
    todays_appointments = DateObject.filter(appointment_date_time__date=date_1.date(),
                                            office=request.user.employee.office)[:10]
    context = {
        'todays_appointments': todays_appointments,
    }
    return render(request, 'calapp/todaysappointment.html', context)


@login_required
# @permission_required('calendarapp.view_appointment_list', raise_exception=True)
def tomorrows_appointment(request):
    dateToday = datetime.date.today()
    date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
    next_day = date_1 + datetime.timedelta(days=1)
    DateObject = Appointment.objects.all()
    tomorrow_appointments = DateObject.filter(appointment_date_time__date=next_day.date(),
                                              office=request.user.employee.office)[:10]
    context = {
        'tomorrow_appointments': tomorrow_appointments,
    }
    return render(request, 'calapp/tommorowapp.html', context)


@login_required
def code_settings(request):
    if AppointmentCodeSetting.objects.exists():
        code_form = AppointmentCodeSettingsForm(request.POST or None, instance=AppointmentCodeSetting.objects.last())
    else:
        code_form = AppointmentCodeSettingsForm(request.POST or None)
    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return HttpResponseRedirect(reverse('calendarapp:date'))

    context = {
        'code_form': code_form,
    }

    return render(request, "calapp/code_setting.html", context)


def send_sms(request, message=None, number=None, invoice=None):
    if invoice:
        invoice = get_object_or_404(Invoice, pk=invoice)
        message = "Hi " + str(
            invoice.customer) + ",Thank you for choosing Sinora Beauty Salon. Billable amount - AED" + str(
            round(invoice.amount, 2)) + " VAT - AED" + str(
            round(invoice.tax_amount, 2)) + "  Total Amount - AED" + str(
            round(invoice.grand_total, 2))
        return_element = HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

        number = invoice.customer.phone
        messages.add_message(request, messages.SUCCESS, 'Sms Sent!!')

    else:
        return_element = True
    payload = {'username': 'Sinora', 'password': 'iAmAwesome@123@', 'api_key': '49jiic53yca0566', 'from': 'DEFAULT',
               'to': number, 'text': message, 'type': 'text'}
    try:
        response = requests.get(
            'https://www.experttexting.com/ExptRestApi/sms/json/Message/Send', params=payload)

    except requests.exceptions.Timeout:
        messages.add_message(request, messages.ERROR, 'Sms not send due to bad connection')
    except requests.exceptions.TooManyRedirects:
        messages.add_message(request, messages.ERROR, 'Sms not send due to too many redirects')
    except requests.exceptions.RequestException as e:
        messages.add_message(request, messages.ERROR, 'Sms not send due to bad connection')

    return return_element


def send_whatsapp(request, invoice):
    my_token = 'b0443f10140e0210f89ae2e943313ac45c2603915a9ea'
    uid = '971501631921'
    invoice = get_object_or_404(Invoice, pk=invoice)
    to = invoice.customer.phone
    custom_id1 = ''.join(random.choices(string.ascii_letters + string.digits, k=4))

    custom_id = ''.join(random.choices(string.ascii_letters + string.digits, k=4))
    msg = "Thank you for visting Sinora Beauty Salon. Please find the Tax Invoice below."
    url_msg = 'https://www.waboxapp.com/api/send/chat?token=' + my_token + '&uid=' + uid + '&to='+ to + '&custom_uid=' + custom_id1 + '&text=' + msg
    response = requests.post(url_msg)
    url = 'https://arcane-spire-27023.herokuapp.com/invoice/' + str(invoice.pk) + '/detail.pdf/'
    url = 'https://www.waboxapp.com/api/send/media?token=' + my_token + '&uid=' + uid + '&to=' + to + '&custom_uid=' + custom_id + '&url=' + url
    response = requests.post(url)
    if response.status_code == 200:
        messages.add_message(request, messages.SUCCESS, 'Shared via whatsapp!!')
    else:
        messages.add_message(request, messages.ERROR, 'Try Again!!')

    return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))
