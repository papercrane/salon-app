from django.conf.urls import include, url
from product import api, views

app_name = "product"


urlpatterns = [
    url(r'^api/', include([
        # router.urls,
        url(r'^products/', include([
            url(r'^select_list/$', api.product_select_options, name="select_list"),
        ])),
        #get active products -api
        url(r'^products$', api.all_products, name="all_products"),

    ], namespace='api')),
    url(r'^product/add/$', views.add_products, name='add_products'),
    url(r'^products/$', views.view_products, name='view_products'),
    url(r'^product/(?P<pk>[^/]+)/$', views.detail, name='detail'),
    url(r'^product/(?P<product_pk>[^/]+)/edit/$',
        views.edit_product, name='edit_product'),
    url(r'^product/code/settings/$', views.code_settings, name='code_settings'),
    url(r'^product/(?P<pk>[^/]+)/delete/$', views.delete, name='delete'),
    url(r'^reports/salesbyproduct/$', views.sales_by_product, name='sales_by_product'),
    url(r'^reports/salesbyproduct/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', views.sales_by_product, name="sales_by_product"),



]
