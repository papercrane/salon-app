from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.db.models import Sum

from .forms import *
from erp_core.pagination import paginate
from .filters import ProductFilter
from invoice.models import InvoiceItem
from django.contrib import messages

# Add The Products

@login_required
@permission_required('product.change_product', raise_exception=True)
def add_products(request):
    form = ProductForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            product = form.save(commit=False)
            product.consumable = True
            product.save()
            form.save_m2m()
            messages.add_message(request, messages.SUCCESS, "Product added successfully")
            return HttpResponseRedirect(reverse('product:view_products'))
    context = {
        "form": form,
        'form_url': reverse_lazy('product:add_products'),
        "type": "Create"
    }
    return render(request, 'product/add_products.html', context)

# Details the Products

@login_required
@permission_required('product.view_product_list', raise_exception=True)
def view_products(request):

    f = ProductFilter(request.GET, queryset=Product.objects.all().order_by("-created_at"))

    # products = Product.objects.all().order_by("-created_at")
    products, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    # for page in pagination:
    #     print(page)
    context = {
        "products": products,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, 'product/view_products.html', context)

@login_required
@permission_required('product.view_product_list', raise_exception=True)
def detail(request, pk):
    product = Product.objects.get(id=pk)
    context = {
        "product": product,
        "pk": pk
    }
    return render(request, "product/detail.html", context)

@login_required
@permission_required('product.change_product', raise_exception=True)
def edit_product(request, product_pk):

    product = Product.objects.get(id=product_pk)
    form = ProductForm(
        request.POST or None, instance=product)

    if request.method == "POST":
        form.save()
        messages.add_message(request, messages.SUCCESS, "Product edited successfully")
        return HttpResponseRedirect(reverse('product:view_products'))
    context = {
        "form": form,
        'form_url': reverse_lazy('product:edit_product', args=[product_pk]),
        "product_pk": product_pk,
        "type": "Edit",
        'product':product,
    }
    return render(request, 'product/add_products.html', context)


def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return HttpResponseRedirect(reverse('product:view_products'))

    context = {
        'code_form': code_form,
    }

    return render(request, "product/code_setting.html", context)

@login_required
@permission_required('product.delete_product', raise_exception=True)
def delete(request, pk):
    if request.method == 'POST':
        product = get_object_or_404(Product, pk=pk)
        product.delete()
        messages.add_message(request, messages.SUCCESS, "Product deleted successfully")
        return HttpResponseRedirect(reverse('product:view_products'))

@login_required
@permission_required('employee.view_report', raise_exception=True)
def sales_by_product(request,from_date=None,to_date=None):
    if not from_date:
        from_date = timezone.now().date()
        to_date = timezone.now().date()
    products = InvoiceItem.objects.filter(invoice__created_at__date__range=[from_date, to_date],
                                          content_type__model='product').values('products__name').annotate(
        quantity=Sum('hours_or_sqfeet'), total_cp=Sum('total_price'))
    context = {
        'from_date': from_date,
        'to_date': to_date,
        'products': products
    }
    return render(request, "product/sales_by_product.html",context)


