from django import forms
from django.forms import ModelForm
from .models import Product
from .models import CodeSetting


class ProductForm(ModelForm):
    class Meta:
        model = Product

        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "supplier": forms.Select(attrs={'class': 'form-control', "multiple": False}),

        }
        fields = ['name', 'code', 'price', 'supplier']


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


# class PackageForm(ModelForm):
#     class Meta:
#         model = Product
#
#         widgets = {
#
#             "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
#             "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
#             "price": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
#             "validity_days": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
#             "minimum_purchase_amount": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
#
#         }
#         fields = ['name', 'code', 'price', 'validity_days', 'minimum_purchase_amount']
