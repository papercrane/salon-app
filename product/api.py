from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.db.models import F
from .serializers import ProductViewSerializer
from product.models import Product

# import json


@ensure_csrf_cookie
def product_select_options(request):
    name = request.GET.get('name', False)
    if name:
        products = Product.objects.filter(
            name__contains=name
        ).annotate(
            text=F('name')
        ).values('id', 'text')
        return JsonResponse(list(products), safe=False)
    return JsonResponse(list(), safe=False)


@ensure_csrf_cookie
def all_products(request):
    products = Product.objects.filter(active=True)
    serializer = ProductViewSerializer(products, many=True)
    return JsonResponse(serializer.data, safe=False)
