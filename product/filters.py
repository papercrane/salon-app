import django_filters
from django_filters import widgets
from django import forms
from product.models import Product


class ProductFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    price = django_filters.NumberFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))
    cost_price = django_filters.NumberFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))

    active = django_filters.BooleanFilter(
        name="active",
        widget=widgets.BooleanWidget(attrs={'class': 'form-control'})
    )
    order_by_created_at = django_filters.ChoiceFilter(
        label="Created date",
        name="created_at",
        method='order_by_field',
        choices=(('created_at', 'Ascending'), ('-created_at', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Product
        fields = ['code', 'name', 'price', 'cost_price', 'active', 'created_at']

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
