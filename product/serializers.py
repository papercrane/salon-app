from rest_framework import serializers
from product.models import Product


class ProductViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'code', 'category')
        # read_only_fields = ('name', 'code', 'price', 'category')
