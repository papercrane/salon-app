from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.

        # item(
        #     'Product',
        #     'product:view_products',
        #     in_menu=True,
        #     in_sitetree=True,
        #     access_loggedin=True,
        #     icon_class="product-hunt",
        # ),


    ]),

    # ... You can define more than one tree for your app.
)
