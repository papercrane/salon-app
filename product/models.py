from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from erp_core.models import BaseCodeSetting
from erp_core.models import ErpAbstractBaseModel
from invoice.models import InvoiceItem
from supplier.models import Supplier
from django.core.validators import MinValueValidator


class Category(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return str(self.name)


class RawMaterialManager(models.Manager):
    def get_queryset(self):
        return super(RawMaterialManager, self).get_queryset().filter(raw_material=True)


class ConsumableManager(models.Manager):
    def get_queryset(self):
        return super(ConsumableManager, self).get_queryset().filter(consumable=True)


class Product(ErpAbstractBaseModel):
    objects = models.Manager()
    # raw_materials = RawMaterialManager()
    consumables = ConsumableManager()

    def newCode():
        return CodeSetting.new_code()

    name = models.CharField(max_length=255, unique=True)
    code = models.CharField(max_length=30, unique=True, default=newCode)
    price = models.DecimalField("Selling Price", max_digits=20, decimal_places=2, null=True, blank=True,
                                validators=[MinValueValidator(0)])
    active = models.BooleanField(default=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE, null=True, blank=True, related_name="suppliers")
    invoice_products = GenericRelation(InvoiceItem, related_query_name='products')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()

        return super(Product, self).save(*args, **kwargs)

    class Meta:
        permissions = (
            ("view_product_list", "Can view product list"),

        )


class CodeSetting(BaseCodeSetting):
    pass
