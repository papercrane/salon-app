from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy


# Create your views here.


@login_required
def dashboard(request):
    # if request.user.is_staff:
    #     return render(request, 'can/dashboard.html', {})
    # elif request.user.employee.primary_designation.name in ["Supervisor"]:
    #     return HttpResponseRedirect(reverse('job:list'))
    # else:
    # if request.user.employee.primary_designation.name in ["Qusais_Admin"]:
    #     return HttpResponseRedirect(reverse('employee:profile', args=[request.user.employee.code]))

    return HttpResponseRedirect("calendar/appointment")
    # return HttpResponseRedirect("/" + str(request.user.employee.code) + "/profile")


