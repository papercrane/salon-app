

class CreateStoreAPIView(mixins.RetrieveModelMixin,
                         mixins.DestroyModelMixin,
                         GenericAPIView):
    """
    Concrete view for retrieving or deleting a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
