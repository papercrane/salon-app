from django.contrib import admin
from .models import *
admin.site.register(PaymentType)
admin.site.register(PaymentTerm)
