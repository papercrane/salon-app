# -*- coding: utf-8 -*-
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


def make_pagination_html(current_page, total_pages):

    pagination_string = ""

    if current_page > 1:

        pagination_string += '<a href="?page=%s">previous</a>' % (current_page - 1)

    pagination_string += '<span class="current"> Page %s of %s </span>' % (current_page, total_pages)

    if current_page < total_pages:

        pagination_string += '<a href="?page=%s">next</a>' % (current_page + 1)

    return pagination_string


def make_range_pagination_html(current_page, total_pages):
    # print("total pages: ", total_pages)
    # print("current page: ", current_page)
    pagination_string = "<nav><ul class='pagination'>"
    # pagination_string = ""
    if current_page == 1:
        pagination_string += "<li class='disabled'><span aria-hidden='true'>«</span></li>"
    else:
        pagination_string += "<li><a href='?page=%s' aria-label='Previous'><span aria-hidden='true'>«</span></a></li>" % (current_page - 1)

    count_limit = 1

    value = current_page - 1
    before_string = ""
    while value > 0 and count_limit < 5:
        bf_str = "<li><a href='?page=%s'>%s</a></li>" % (value, value)
        before_string = bf_str + before_string

        value -= 1

        count_limit += 1

    pagination_string += before_string

    pagination_string += "<li class='active'><a href='?page=%s'>%s</a></li>" % (current_page, current_page)

    value = current_page + 1

    while value <= total_pages and count_limit < 10:
        # print("value: ", value, "count_limit: ", count_limit)

        pagination_string = pagination_string + "<li><a href='?page=%s'>%s</a></li>" % (value, value)

        value += 1

        count_limit += 1

    if current_page < total_pages:
        pagination_string += '<li><a href="?page=%s" aria-label="Next"><span aria-hidden="true">»</span></a></li>' % (current_page + 1)
    else:
        pagination_string += '<li class="disabled"><span aria-hidden="true">»</span></li>'
        # pagination_string += '<a href="?page=%s">→</a>' % (current_page + 1)

    return pagination_string


def paginate(request, queryset, per_page=10, default=True):
    current_page = int(request.GET.get('page', '1'))

    limit = per_page * current_page

    offset = limit - per_page

    models = queryset.all()[offset:limit]  # limiting objects based on current_page

    total_models = queryset.all().count()
    # print('total_models: ', total_models)
    # print('per page: ', per_page)
    total_pages = total_models // per_page
    if total_models % per_page != 0:
        total_pages += 1  # adding one more page if the last page will contains less contacts

    if default:
        pagination_string = make_range_pagination_html(current_page, total_pages)
    else:
        pagination_string = make_pagination_html(current_page, total_pages)

    return models, pagination_string


class LargeResultsSetPagination(PageNumberPagination):
    # page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 100


class CustomPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response({
            'links': {
               'next': self.get_next_link(),
               'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'page_size': self.page_size,
            'results': data
        })



def cust_paginate(request, queryset, per_page=10, default=True):
    current_page = int(request.GET.get('page', '1'))

    limit = per_page * current_page

    offset = limit - per_page

    models = queryset.all()[offset:limit]  # limiting objects based on current_page

    total_models = queryset.all().count()
    # print('total_models: ', total_models)
    # print('per page: ', per_page)
    total_pages = total_models // per_page
    if total_models % per_page != 0:
        total_pages += 1  # adding one more page if the last page will contains less contacts

    if default:
        pagination_string = make_range_pagination_html(current_page, total_pages)
    else:
        pagination_string = make_pagination_html(current_page, total_pages)

    return models, pagination_string,current_page,limit
