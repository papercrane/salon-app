import enquiryApi from 'enquiry/api.js'
import * as types from '../mutation-types'
// import isEmpty from '../../contrib/utils'
import Vue from 'vue'

const state = {
	enquiries: [],
	enquiry: {
		id: null,
		type: null,
		status: null,
		customer: '',
		note: '',
		reason_for_lost: '',
		follow_up_date: null,
		items: [],
		activities: [],
		amount: 0.00,  
	},
	pagination:{
		count: 0,
		current: 1,
		page_size: 3,
		link_range: 3,
	},
	choices: {
		statuses: [],
		default_status: '',
		types: [],
		default_type: '',
		customers: [],
		products: [],
	},
	
	saving: false,
	enquiry_list: true,
	edit_form: false,
	view_enquiry: false,
}

const getters = {
	allEnquiries: state => state.enquiries,
	enquiry: state => state.enquiry,
	items: state => state.enquiry.items,
	choices: state => state.choices,
	nonSelectedProducts: state => {
		return state.choices.products.filter( product => { 
			if(product.selected === false)
				return product
		})
	},
	products: state => state.choices.products,
	pagination: state => state.pagination,
}

const actions = {
	getEnquiries ({ commit, rootState }) {
		// let url = 
		console.log('/api'+rootState.route.path)
		commit('loading')
		return new Promise((resolve) => {
			enquiryApi.getEnquiries('/api'+rootState.route.fullPath)
				.then( response => {
					commit(types.SET_PAGINATION,{
						count: response.data.count,
						page: rootState.route.query.page,
						page_size: response.data.page_size
					})
					commit(types.SET_ENQUIRIES, {enquiries: response.data.results })
					commit('loaded')
					resolve()
				})
				.catch( error => {
					commit('loaded')
					console.log(error)
				})
		})
	},

	getEnquiryOptions ({ commit }) { 
		return new Promise((resolve) => {
			commit('loading')
			enquiryApi.getEnquiryOptions()
				.then( response => {
					commit(types.SET_ENQUIRY_OPTIONS, response.data )
					commit('loaded')
					resolve()
				})
				.catch(error => {
					commit('loaded')
					console.log(error)
				})
		})
	},

	getEnquiry ({ commit, state }, id = null) {
		if(id){
			commit('loading')
			enquiryApi.get(id)
				.then((response) => {
					commit(types.SET_ENQUIRY, { enquiry: response.data })
					commit('loaded')
				})
				.catch( error => console.log(error) )
		}
	},

	deleteEnquiry( {commit, state}, id = null) {
		if(id){
			commit('loading')
			enquiryApi.delete('/api/enquiries/' + id + '/')
				.then((response) => {
					console.log(response)
					commit(types.DELETE_ENQUIRY, { id })
					commit('loaded')
					return Promise(resolve => resolve())
				})
				.catch((error) => {
					commit('loaded')
					commit('API_FAILURE', error)
				})
		}
	},

	postEnquiry ({ commit, state }, enquiry) {
		console.log(enquiry)
		return new Promise(( resolve, reject) => {
			commit('loading')
			enquiryApi.post('/api/enquiries/', enquiry)
				.then( response => {
					commit(types.SET_ENQUIRY, response.data)
					// commit(types.INSERT_ENQUIRY, response.data)
					commit('loaded')

					resolve()
				})
				.catch( error => {
					commit('loaded')
					reject(error) 
				})
		})
	},

	// addItemToEnquiryItem ({commit}, payload) {
	//   // commit(types.UPDATE_ITEM_FIELD, payload )
	//   // commit(types.UPDATE_ENQUIRY_AMOUNT)

	// }

}

const mutations = {
	

	[types.SET_ENQUIRIES] (state, { enquiries }) {
		state.enquiries = enquiries
	},
	[types.SET_ENQUIRY] (state, { enquiry }) {
		state.enquiry = enquiry
	},
	[types.INSERT_ENQUIRY] (state, { enquiry, created }) {
		if(created)
			state.enquiries = [enquiry].concat(state.enquiries)
		// state.enquiries.push(enquiry)
	},

	[types.SET_ENQUIRY_OPTIONS] (state, {statuses= [],default_status= '',types= [],default_type= '',customers= [],products= [],
		activity_types = []}) {
		state.choices.statuses = statuses
		state.choices.default_status = default_status
		state.choices.types = types
		state.choices.default_type = default_type
		state.choices.customers = customers
		products.forEach( product => { 
			Vue.set(product, 'disabled', false)
		})
		state.choices.products = products
		state.choices.activity_types = activity_types
	},

	[types.DELETE_ENQUIRY] (state, { id }) {
		var index = state.enquiries.findIndex((enquiry) => {
			return enquiry.id == id
		})
		state.enquiries.splice(index, 1)
	},
	// [types.SET_ENQUIRY_OPTION_DEFAULTS] ( state ) {
	//   if(!state.enquiry.type)
	//       state.enquiry.type = state.choices.default_type[0]
	//   if(!state.enquiry.status)
	//     state.enquiry.status = state.choices.default_status[0]
	// },

	[types.API_REQUEST] (state) {
		state.saving = true
	},

	[types.API_SUCCESS] (state) {
		state.saving = false
	},

	[types.API_FAILURE] (state, { response }) {
		console.log(response)
	},

	[types.SET_ENQUIRY_FIELD] (state, { field, value }) {
		state.enquiry[field] = value
	},

	[types.UPDATE_ITEM] (state, { item, index }) {
		console.log(item, index)
		// debugger
		state.enquiry.items.splice(index, 1, item)
		
	},

	[types.PRODUCT_SELECTED] (state, product) {
		Vue.set(product, 'disabled', true)
	},

	[types.PRODUCT_REMOVED] (state, product) {
		Vue.set(product, 'disabled', false)
	},

	[types.SET_PAGINATION] (state, {count = 0, page = 1, page_size = 3}) {
		console.log(page_size)
		if(count){
			state.pagination.count = count
			state.pagination.page_size = page_size
		}
		state.pagination.current = Number(page)
	},
}

export default {
	state,
	getters,
	actions,
	mutations,
	// modules,
}