import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from salary.models import SalaryRulesForDesignation, MonthlySalary
from company.models import Designation, Grade
import datetime
from employee.models import Employee


class SalaryRuleFilter(django_filters.FilterSet):
    designation = django_filters.ModelChoiceFilter(
        queryset=Designation.objects.all(),
        method='filter_by_designation',
        widget=forms.Select(attrs={'class': 'form-control1', 'style': "width: 100%;"})
    )
    grade = django_filters.ModelChoiceFilter(
        queryset=Grade.objects.all(),
        method='filter_by_grade',
        widget=forms.Select(attrs={'class': 'form-control1', 'style': "width: 100%;"})
    )

    class Meta:
        model = SalaryRulesForDesignation
        fields = ['designation', ]

    def filter_by_designation(self, queryset, name, value):
        print("desig: ", value)
        return queryset.filter(
            Q(designation=value)
        )

    def filter_by_grade(self, queryset, name, value):
        return queryset.filter(
            Q(grade__name=value)
        )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


class MonthlySalaryFilter(django_filters.FilterSet):
    month_year = django_filters.CharFilter(method='filterbymonth_year',
                                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    date = django_filters.DateFilter(widget=forms.DateInput(attrs={'class': 'form-control date'}))
    paid_date = django_filters.DateFilter(widget=forms.DateInput(attrs={'class': 'form-control date'}))
    paid = django_filters.BooleanFilter(name='paid', widget=widgets.BooleanWidget(attrs={'class': 'form-control'}))
    # paid_date = django_filters.DateFilter(name='paid_date', widget=forms.DateInput(attrs={'hidden': 'hidden'}))
    employee = django_filters.ModelChoiceFilter(
        queryset=Employee.objects.all(),
        method='filter_by_employee',
        widget=forms.Select(attrs={'class': 'form-control1', 'style': "width: 100%;"})
    )

    class Meta:
        model = MonthlySalary
        fields = ['paid', 'paid_date', 'date', 'employee']

    def filter_by_contact(self, queryset, name, value):
        # print("contact name: ", value)
        return queryset.filter(
            Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
        )

    def filterbymonth_year(self, queryset, name, value):
        value = datetime.datetime.strptime(value, "%B %Y").date()
        return MonthlySalary.objects.filter(date__month=value.month, date__year=value.year)

    def filter_by_employee(self, queryset, name, value):
        print("em: ", value)
        return queryset.filter(
            Q(employee=value)
        )
