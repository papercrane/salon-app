from django.conf.urls import include, url

from salary import views
app_name = "salary"


urlpatterns = [
    url(r'^salary/', include([
        url(r'^dashboard/$', views.dashboard, name='dashboard'),
        url(r'^add_rules/$', views.add_SalaryRules, name='add_SalaryRules'),
        url(r'^rules/$', views.view_SalaryRules, name='view_SalaryRules'),
        url(r'^edit_rule/(?P<pk>[^/]+)/$', views.edit_SalaryRules, name='edit_SalaryRules'),
        url(r'^generate/$', views.generate_salary, name='generate_salary'),
        url(r'^salaryDetails/$', views.salary_details, name='salary_details'),
        url(r'^(?P<pk>[-\w]+)/$', views.single_salary, name='single_salary'),
        url(r'^edit_salary/(?P<pk>[^/]+)/$', views.edit_monthly_salary, name='edit_monthly_salary'),
        url(r'^pay/(?P<monthly_salary>[-\w]+)/$', views.pay_salary, name='pay_salary'),
        url(r'^generatePayRoll/(?P<pk>[^/]+)/detail.pdf/$',
            views.GeneratePayRollPDFView.as_view(), name="generate_pay_roll_pdf"),
        # url(r'^pay/(?P<pk>[-\w]+)/$', views.pay_salary, name='pay_salary'),
        # url(r'^employee/(?P<employee>[-\w]+)/$', views.pay_salary, name='pay_salary'),
        url(r'^employee/(?P<employee>[^/]+)/(?P<date>[-\w]+)/$',
            views.generate_monthly_salary, name='generate_monthly_salary'),
        url(r'^monthly/salaryDetails/$', views.monthly_salary_details, name='monthly_salary_details'),




    ])),

]
