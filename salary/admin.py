
from django.contrib import admin
from .models import *
admin.site.register(SalaryRulesForDesignation)
admin.site.register(Deduction)
admin.site.register(MonthlySalary)
