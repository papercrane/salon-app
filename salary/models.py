from django.db import models
from employee.models import Employee
from company.models import Designation, Grade
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator
from django.utils import timezone
# Create your models here.


class Deduction(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, unique=True)
    value = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return str(self.name)


class SalaryRulesForDesignation(ErpAbstractBaseModel):
    # employee = models.ForeignKey(Employee)

    # house_rent_allowances = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, validators=[MinValueValidator(0)])
    # miscellaneous_allowances = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, validators=[MinValueValidator(0)])

    designation = models.OneToOneField(Designation, on_delete=models.CASCADE)
    grade = models.ForeignKey(Grade, on_delete=models.CASCADE, null=True, blank=True)
    basic_pay = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, validators=[MinValueValidator(0)])
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                validators=[MinValueValidator(0)], null=True, blank=True)

    # deductions = models.ManyToManyField(Deduction, related_name="salary_rules")

    # class Meta:
    #     unique_together = ("designation", "grade")

    def __str__(self):
        return str(self.designation)


class MonthlySalary(ErpAbstractBaseModel):
    employee = models.ForeignKey(Employee)
    month_year = models.CharField(max_length=15, null=True, blank=True)
    date = models.DateField(default=timezone.now)
    # house_rent_allowances = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, validators=[MinValueValidator(0)])
    # miscellaneous_allowances = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, validators=[MinValueValidator(0)])
    # other = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                # validators=[MinValueValidator(0)], null=True, blank=True)
    # ta_allowances = models.DecimalField("TA", max_digits=20, decimal_places=2,
    #                                     default=0.00, validators=[MinValueValidator(0)], null=True, blank=True)
    # sales_reimbursement = models.DecimalField(max_digits=20, decimal_places=2,
    #                                           default=0.00, validators=[MinValueValidator(0)], null=True, blank=True)
    # designation = models.ForeignKey(Designation, on_delete=models.CASCADE, null=True, blank=True)
    # grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
    basic_pay = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                    validators=[MinValueValidator(0)], null=True, blank=True)
    basic_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                 validators=[MinValueValidator(0)], null=True, blank=True)
    # deduction_amount = models.DecimalField(max_digits=20, decimal_places=2,
    #                                        default=0.00, validators=[MinValueValidator(0)])
    paid = models.BooleanField(default=False)
    paid_date = models.DateField(null=True, blank=True)
    # deductions = models.ManyToManyField(Deduction, related_name="employee_deductions")
    # incentives = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                     # validators=[MinValueValidator(0)], null=True, blank=True)
    # fine = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
    #                            validators=[MinValueValidator(0)], null=True, blank=True,)
    overtime_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0.00,
                                   validators=[MinValueValidator(0)], null=True, blank=True)
    total_working_hours = models.IntegerField(null=True,blank=True)


    class Meta:
        unique_together = ("employee", "date")

    def __str__(self):
        # return str(self.employee) + " - " + str(self.month_year)
        return str(self.employee) + " - " + str(self.date)
