from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V, Sum
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from guardian.shortcuts import get_objects_for_user
import datetime
from easy_pdf.views import PDFTemplateView
from salary.filters import MonthlySalaryFilter
from django.contrib import messages
from erp_core.pagination import paginate
from .models import *
from salary.filters import SalaryRuleFilter
from decimal import Decimal
# from expense.models import SalesReimbursement
# from employee.models import Targets
from timesheet.models import TimeSheet, Overtime
from expense.models import Expense
from django.db import IntegrityError, transaction
# from job.models import JobFine
# Create your views here.


def add_SalaryRules(request):

	form = SalaryRulesForm(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			salaryrule = form.save(commit=False)
			# calculate total salary for designation
			salaryrule.total = salaryrule.basic_pay
			salaryrule.save()
			# form.save_m2m()

			# reduce deduction amount from total salary
			# for deduction in salaryrule.deductions.all():
			#     salaryrule.total = salaryrule.total - deduction.value
			# salaryrule.save()
			return HttpResponseRedirect(reverse('salary:view_SalaryRules'))
	context = {
		"form": form,
		'form_url': reverse_lazy('salary:add_SalaryRules'),
		"type": "add"
	}
	return render(request, 'salary/add_SalaryRules.html', context)


def view_SalaryRules(request):

	# total deduction value for each salary rules is also calculated
	salaryrules = SalaryRulesForDesignation.objects.all().order_by('-id')

	if request.method == "POST":
		designation = request.POST.get('designation')
		salaryrules = SalaryRulesForDesignation.objects.filter(designation=designation).order_by('-id')

	salaryrules, pagination = paginate(request, queryset=salaryrules, per_page=20, default=True)
	context = {
		"salaryrules": salaryrules,
		'pagination': pagination,
	}
	return render(request, 'salary/view_SalaryRules.html', context)


def edit_SalaryRules(request, pk):

	salaryrule = SalaryRulesForDesignation.objects.get(id=pk)
	form = SalaryRulesForm(request.POST or None, instance=salaryrule)
	if request.method == "POST":
		salaryrule = form.save()
		salaryrule.total = salaryrule.basic_pay
		salaryrule.save()

		return HttpResponseRedirect(reverse('salary:view_SalaryRules'))
	context = {
		"form": form,
		'form_url': reverse_lazy('salary:edit_SalaryRules', args=[pk]),
		"salaryrule_pk": pk,
		"type": "edit"
	}
	return render(request, 'salary/add_SalaryRules.html', context)


def generate_salary(request):

	if request.method == "POST":
		employee = request.POST.get('employee')
		employee_object = Employee.objects.get(id=employee)
		# month_year = request.POST.get('month_year')
		date = request.POST.get('date')

		# formated_date = datetime.datetime.strptime(month_year, "%B %Y").date()
		# formated_date = datetime.datetime.strptime(date, "%Y-%m-%d").date()

		try:
			# monthly_salary = MonthlySalary.objects.get(employee_id=employee, month_year=month_year)
			daily_salary = MonthlySalary.objects.get(employee_id=employee, date=date)

		except MonthlySalary.DoesNotExist:
			try:
				# if salary of the employee for the given month and year is not there in
				# db then get the salary rule for the employee's primary designation
				salary_rule = SalaryRulesForDesignation.objects.get(
					designation=employee_object.primary_designation)
			except Exception as e:
				messages.add_message(request, messages.ERROR, e)
				messages.add_message(request, messages.ERROR, "Create Salary rules for the designation " +
									 str(employee_object.primary_designation) + " and grade " + str(employee_object.primary_designation.grade))
				return HttpResponseRedirect(reverse('salary:generate_salary'))
			# create monthly salary for the employee based on salary rule
			# monthly_salary = MonthlySalary.objects.create(employee=request.user.employee,
			#                                               month_year=month_year,
			#                                               house_rent_allowances=salary_rule.house_rent_allowances,
			#                                               miscellaneous_allowances=salary_rule.miscellaneous_allowances,
			#                                               other=0.00,
			#                                               ta_allowances=0.00,
			#                                               sales_reimbursement=0.00,
			#                                               designation=salary_rule.designation,

			#                                               grade=salary_rule.grade,
			#                                               basic_pay=salary_rule.basic_pay,
			#                                               amount=salary_rule.total,
			#                                               deduction_amount=0.00,
			#                                               incentives=0.00)

			try:
				timesheet = TimeSheet.objects.get(employee_id=employee, date=date,)
				# timesheet = TimeSheet.objects.get(employee=request.user.employee, date=date, holiday=True)

			except Exception as e:
				messages.add_message(request, messages.ERROR, "No timesheet entry for this employee")
				return HttpResponseRedirect(reverse('timesheet:bulk_create'))

			# calculate the leave
			if timesheet.holiday == True:
				# pay for can
				basic_pay = 90.00
				amount = salary_rule.total - salary_rule.basic_pay
			else:
				basic_pay = salary_rule.basic_pay
				amount = salary_rule.total

			# fine = JobFine.objects.filter(date=date).aggregate(total_fine=Sum('fine_amount'))['total_fine']
			# if fine:
			#     amount = amount - fine
			# else:
			#     fine = 0
			try:
				overtime = Overtime.objects.get(date=date, employee=employee_object)
				overtime_amount = overtime.hours * overtime.amount_per_hour
				amount = amount + overtime_amount
			except Exception as e:
				overtime_amount = 0

			daily_salary = MonthlySalary.objects.create(employee=employee_object,
														date=date,
														# house_rent_allowances=salary_rule.house_rent_allowances,
														# miscellaneous_allowances=salary_rule.miscellaneous_allowances,
														other=0.00,
														ta_allowances=0.00,
														sales_reimbursement=0.00,
														designation=salary_rule.designation,
														grade=salary_rule.grade,
														basic_pay=basic_pay,
														amount=amount,
														deduction_amount=0.00,
														incentives=0.00,
														fine=fine,
														overtime=overtime_amount)

			# adding sales reimbursement amount from the SalesReimbursement table
			# reimbursement = SalesReimbursement.objects.filter(employee=request.user.employee, approved_date__month=formated_date.month,
			#                                                   approved_date__year=formated_date.year, ).aggregate((Sum('amount')))

			# if reimbursement['amount__sum']:
			#     monthly_salary.sales_reimbursement = reimbursement['amount__sum']
			#     monthly_salary.amount = Decimal(monthly_salary.amount) + Decimal(monthly_salary.sales_reimbursement)
			#     monthly_salary.save()

			# incentive = Targets.objects.filter(employee=request.user.employee,from_date__month = formated_date.month,from_date__year = formated_date.year,to_date__month = formated_date.month,to_date__year = formated_date.year).aggregate((Sum('incentive_amount')))

			# if incentive['incentive_amount__sum']:
			#     monthly_salary.incentives = incentive['incentive_amount__sum']
			#     monthly_salary.amount = Decimal(monthly_salary.amount) + Decimal(monthly_salary.incentives)
			#     monthly_salary.save()

			# add deductions from the employee's salary  from salary rule
			# for deduction in salary_rule.deductions.all():
			#     monthly_salary.deductions.add(deduction)
			#     monthly_salary.deduction_amount = Decimal(monthly_salary.deduction_amount) + deduction.value
			# monthly_salary.save()

			for deduction in salary_rule.deductions.all():
				daily_salary.deductions.add(deduction)
				daily_salary.deduction_amount = Decimal(daily_salary.deduction_amount) + deduction.value
			daily_salary.save()

		# return HttpResponseRedirect(reverse('salary:single_salary', args=[monthly_salary.pk]))
		return HttpResponseRedirect(reverse('salary:single_salary', args=[daily_salary.pk]))

	else:
		pass
	return render(request, 'salary/generate_salary.html')


def single_salary(request, pk):

	monthly_salary = MonthlySalary.objects.get(pk=pk)
	context = {
		"monthly_salary": monthly_salary,
	}
	return render(request, 'salary/monthly_salary_single.html', context)


def edit_monthly_salary(request, pk):

	monthly_salary = MonthlySalary.objects.get(id=pk)
	basic_pay = monthly_salary.basic_pay

	if not monthly_salary.paid:
		form = MonthlySalaryForm(request.POST or None, instance=monthly_salary)
		if request.method == "POST" and form.is_valid():
			salary = form.save(commit=False)
			form.save_m2m()
			salary.deduction_amount = Decimal(0.00)
			salary.amount = salary.house_rent_allowances + salary.miscellaneous_allowances + \
				salary.ta_allowances + salary.sales_reimbursement + salary.other + basic_pay + salary.incentives
			salary.save()

			for deduction in salary.deductions.all():
				salary.deduction_amount = salary.deduction_amount + deduction.value
			salary.save()
			if salary.amount > salary.deduction_amount:
				salary.amount = salary.amount - salary.deduction_amount
			else:
				salary.amount = salary.deduction_amount - salary.amount
			salary.amount = salary.amount - salary.fine
			salary.amount = salary.amount + salary.overtime
			salary.save()

			return HttpResponseRedirect(reverse('salary:single_salary', args=[monthly_salary.pk]))

		context = {
			"form": form,
			'form_url': reverse_lazy('salary:edit_monthly_salary', args=[pk]),
			"monthly_salary": monthly_salary

		}
		return render(request, 'salary/edit_salary.html', context)
	messages.add_message(request, messages.ERROR, "Salary Already Paid")
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def monthly_salary_details(request):
	print ("hi")

	f = MonthlySalaryFilter(request.GET, queryset=MonthlySalary.objects.order_by("-created_at"))


	# today = datetime.date.today()
	# month_year = ""
	# if request.GET.get('month_year'):
	# 	month_year = request.GET.get('month_year')
	# else:
	# 	month_year = today.strftime('%B') + " " + str(today.year)
	# # f = MonthlySalaryFilter(request.GET, queryset=MonthlySalary.objects.filter(month_year=month_year).order_by("-created_at"))
	# f = MonthlySalaryFilter(request.GET, queryset=MonthlySalary.objects.filter(date__month=today.month, date__year=today.year).order_by("-created_at"))
	# salary_details = f.qs

	# if request.method == "POST":
	# 	employee = request.POST.get('employee')
	# 	if employee:
	# 		# salary_details = MonthlySalary.objects.filter(employee=employee, month_year=month_year)
	# 		salary_details = MonthlySalary.objects.filter(employee=employee, date__month=today.month, date__year=today.year)

	context = {
		'salary_details': f.qs,
		'filter': f,
		# 'month_year': month_year,
	}

	return render(request, 'salary/monthly_salary_details.html', context)


# def pay_salary(request, pk):

# 	if request.method == "POST":
# 		salary = MonthlySalary.objects.get(id=pk)
# 		salary.paid = True
# 		salary.paid_date = request.POST.get('date')
# 		salary.save()
# 	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# Generating pdf


class GeneratePayRollPDFView(PDFTemplateView):

	template_name = "salary\generatepayroll_pdf.html"

	def get_context_data(self, **kwargs):
		pk = kwargs.get('pk', False)

		return super(GeneratePayRollPDFView, self).get_context_data(
			pagesize="A4",
			title="Pay Roll",
			monthly_salary=MonthlySalary.objects.get(pk=pk),
		)


def dashboard(request):
	return render(request, "salary/dashboard.html")


def salary_details(request):

	# today = datetime.date.today()
	# month_year = ""
	# if request.GET.get('month_year'):
	#     month_year = request.GET.get('month_year')
	# else:
	#     month_year = today.strftime('%B') + " " + str(today.year)

	# if request.method == "POST":
	employee_id = request.GET.get('employee')
	print (employee_id)
	date_check = request.GET.get('month_year')

	if date_check:
		date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
		crnt_date_for_check = date_check
	else:
		date_for_check = datetime.date.today()
		tdate = datetime.date.today()
		crnt_date_for_check = datetime.datetime.strptime(str(tdate), '%Y-%m-%d').strftime('%B %Y')


	if employee_id:
		if employee_id != "Select Employee":
			print ("iooo")  
			employee = Employee.objects.get(id=employee_id)
			if not employee.primary_designation:
				messages.add_message(request, messages.ERROR,"Please add designation for employee")
				return HttpResponseRedirect(reverse('salary:salary_details'))


			timesheets = TimeSheet.objects.filter(employee=employee, date__month=date_for_check.month, date__year=date_for_check.year)
			if timesheets:
				if SalaryRulesForDesignation.objects.filter(designation__name=employee.primary_designation.name).count() > 0:
					salary_rule = SalaryRulesForDesignation.objects.get(designation__name=employee.primary_designation.name).basic_pay
					if timesheets:
						total_hours = timesheets.aggregate(Sum('actual_work_hours'))
						overtime_hours = timesheets.aggregate(Sum('overtime_hours'))
						total_hours = total_hours['actual_work_hours__sum']
						over_time_hours = overtime_hours['overtime_hours__sum']
						
					else :
						total_hours = 0
						over_time_hours = 0

					normal_wage = (total_hours/ 3600) * salary_rule
					overtime_wage = (over_time_hours / 3600) * salary_rule * 2
					total_working_hours = total_hours + over_time_hours
					total_wage = normal_wage + overtime_wage

					travelling_expense = Expense.objects.filter(date__month=date_for_check.month, date__year=date_for_check.year,payee_id=employee.pk,expense_type=Expense.TRAVELING)
					if travelling_expense: 
						total_expense = travelling_expense.aggregate(Sum('amount'))
						total_expense = total_expense['amount__sum']
					else:
						total_expense = 0

					advance_payments = Expense.objects.filter(date__month=date_for_check.month, date__year=date_for_check.year,payee_id=employee.pk,expense_type=Expense.STAFF_ADVANCE)

					if advance_payments: 
						total_advance = advance_payments.aggregate(Sum('amount'))
						total_advance = total_advance['amount__sum'] 
					else:
						total_advance = 0

					if timesheets:
						if total_wage > 0 :
							salary = total_wage-total_advance-total_expense
						else:
							salary = 0
					else:
						salary = 0

					context = {
						'crnt_date_for_check': crnt_date_for_check,
						'timesheets': timesheets,
						'employee': employee,
						'total_hours': total_hours,
						'over_time_hours': over_time_hours,
						'salary_rule': salary_rule,
						'normal_wage': normal_wage,
						'overtime_wage': overtime_wage,
						'total_working_hours': total_working_hours,
						'total_wage': total_wage,
						'total_expense':total_expense,
						'travelling_expense':travelling_expense,
						'advance_payments':advance_payments,
						'total_advance':total_advance,
						'salary':salary,
						'date_for_check':date_for_check
						# 'today':today


					}

					return render(request, 'salary/salary_view.html', context)
				else:
					return render(request, 'salary/salary_view.html')

		else:
			return render(request, 'salary/salary_view.html')

	else:
		return render(request, 'salary/salary_view.html')

		
	# context = {
	# 	# 'today': today,
	# 	# 'timesheets': timesheets,
	# 	# 'employee': employee
	# }
	# return render(request, 'salary/salary_view.html', context)

def pay_salary(request,monthly_salary):
	monthly_salary = get_object_or_404(MonthlySalary,pk=monthly_salary)
	monthly_salary.paid=True
	monthly_salary.paid_date=timezone.now().date()
	monthly_salary.save()

	return HttpResponseRedirect(reverse('salary:generate_monthly_salary', args=[monthly_salary.employee.pk,monthly_salary.date]))

	



		



def generate_monthly_salary(request,employee,date):
	employee  = get_object_or_404(Employee,pk=employee)
	if not employee.primary_designation:
		messages.add_message(request, messages.ERROR,"Please add designation for employee")
		return HttpResponseRedirect(reverse('salary:generate_monthly_salary',args=[employee,date]))
	date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
	timesheets = TimeSheet.objects.filter(employee=employee, date__month=date.month, date__year=date.year)
	if timesheets:
		if SalaryRulesForDesignation.objects.filter(designation__name=employee.primary_designation.name).count() > 0:
			salary_rule = SalaryRulesForDesignation.objects.get(designation__name=employee.primary_designation.name).basic_pay
			if timesheets:
				total_hours = timesheets.aggregate(Sum('actual_work_hours'))
				overtime_hours = timesheets.aggregate(Sum('overtime_hours'))
				total_hours = total_hours['actual_work_hours__sum']
				over_time_hours = overtime_hours['overtime_hours__sum']
				
			else :
				total_hours = 0
				over_time_hours = 0

			normal_wage = (total_hours/ 3600) * salary_rule
			overtime_wage = (over_time_hours / 3600) * salary_rule * 2
			total_working_hours = total_hours + over_time_hours
			total_wage = normal_wage + overtime_wage

			travelling_expense = Expense.objects.filter(date__month=date.month, date__year=date.year,payee_id=employee.pk,expense_type=Expense.TRAVELING)
			if travelling_expense: 
				total_expense = travelling_expense.aggregate(Sum('amount'))
				total_expense = total_expense['amount__sum']
			else:
				total_expense = 0

			advance_payments = Expense.objects.filter(date__month=date.month, date__year=date.year,payee_id=employee.pk,expense_type=Expense.STAFF_ADVANCE)

			if advance_payments: 
				total_advance = advance_payments.aggregate(Sum('amount'))
				total_advance = total_advance['amount__sum'] 
			else:
				total_advance = 0

			if timesheets:
				if total_wage > 0 :
					salary = total_wage-total_advance-total_expense
				else:
					salary = 0
			else:
				salary = 0

			try:
				with transaction.atomic():
					monthly_salary = MonthlySalary.objects.create(employee=employee,basic_amount=normal_wage,overtime_amount=overtime_wage,total_working_hours=total_working_hours,date=date,paid=False)
					
			except IntegrityError:
				monthly_salary = MonthlySalary.objects.get(employee=employee,date=date) 
		


	context = {
		'date': date,
		'timesheets': timesheets,
		'employee': employee,
		'total_hours': total_hours,
		'over_time_hours': over_time_hours,
		'salary_rule': salary_rule,
		'normal_wage': normal_wage,
		'overtime_wage': overtime_wage,
		'total_working_hours': total_working_hours,
		'total_wage': total_wage,
		'total_expense':total_expense,
		'travelling_expense':travelling_expense,
		'advance_payments':advance_payments,
		'total_advance':total_advance,
		'salary':salary,
		'monthly_salary':monthly_salary,
		# 'today':today


	}

	return render(request, 'salary/employee_salary_detail.html', context)




	


	





