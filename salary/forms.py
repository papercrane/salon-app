from django import forms
from .models import SalaryRulesForDesignation, MonthlySalary
from django.forms import ModelForm


class SalaryRulesForm(ModelForm):
    class Meta:
        model = SalaryRulesForDesignation

        widgets = {

            "designation": forms.Select(attrs={'id': "select1", 'class': 'form-control desig', "multiple": False}),
            # "house_rent_allowances": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            # "miscellaneous_allowances": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "basic_pay": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            # "grade": forms.Select(attrs={'class': 'form-control', "multiple": False}),
            # "deductions": forms.SelectMultiple(attrs={'class': "form-control", "data-toggle": "select", "id": "deductions", 'multiple': "multiple", 'required': "required"}),
        }
        fields = ['designation', 'basic_pay']


class MonthlySalaryForm(ModelForm):
    class Meta:
        model = MonthlySalary

        widgets = {

            "house_rent_allowances": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "miscellaneous_allowances": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "deductions": forms.SelectMultiple(attrs={'class': "form-control", "data-toggle": "select", "id": "deductions", 'multiple': "multiple", 'required': "required"}),
            "other": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "ta_allowances": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "sales_reimbursement": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "incentives": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "overtime": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
            "fine": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),

        }
        exclude = ('employee', 'month_year', 'designation', 'grade', 'basic_pay', 'amount', 'deduction_amount', 'date')
