from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.

        item(
            'Salary',
            'salary:dashboard',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="inr",

            # children=[
            #     item(
            #         'Add Salary Rule',
            #         'salary:add_SalaryRules',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="print",
            #         # access_by_perms=['enquiry.view_quote_list'],

            #     ),
            #     item(
            #         'View Salary Rule',
            #         'salary:view_SalaryRules',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="print",
            #         # access_by_perms=['enquiry.view_quote_list'],

            #     ),
            #     item(
            #         'Deductions',
            #         '/admin/salary/deduction/',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="print",
            #         # access_by_perms=['enquiry.view_quote_list'],

            #     ),
            #     item(
            #         'Generate Salary',
            #         'salary:generate_salary',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="print",
            #         # access_by_perms=['enquiry.view_quote_list'],

            #     ),

            #     # item(
            #     #     'Stock',
            #     #     'inventory:stock:list',
            #     #     in_menu=True,
            #     #     in_sitetree=True,
            #     #     access_loggedin=True,
            #     #     icon_class="print",
            #     #     # access_by_perms=['enquiry.view_quote_list'],

            #     # ),

            #     # item(
            #     #     'Check Stock',
            #     #     'inventory:stock:checkStock',
            #     #     in_menu=True,
            #     #     in_sitetree=True,
            #     #     access_loggedin=True,
            #     #     icon_class="print",
            #     #     # access_by_perms=['enquiry.view_quote_list'],

            #     # ),

            #     # item(
            #     #     'Reports',
            #     #     'inventory:reports:index',
            #     #     in_menu=True,
            #     #     in_sitetree=True,
            #     #     access_loggedin=True,
            #     #     icon_class="print",
            #     #     # access_by_perms=['enquiry.view_quote_list'],

            #     # ),

            #     item(
            #         'View Salary',
            #         'salary:salary_details',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="print",
            #         # access_by_perms=['enquiry.view_quote_list'],

            #     ),



            # ]

        )

    ]),

    # ... You can define more than one tree for your app.
)
