import Vue from 'vue'

Vue.config.debug = true

import Cookies from 'js-cookie'
var csrftoken = Cookies.get('csrftoken')

// var VueResource = require('vue-resource')
// Vue.use(VueResource)
import axios from 'axios'
axios.defaults.headers.common['X-CSRFToken'] = csrftoken
Vue.prototype.$http = axios

import App from './components/DetailApp.vue'


new Vue({
	el: '#app',
	render: h => h(App)
})