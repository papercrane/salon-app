from django.db import transaction
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Sum, Count, F
from django.utils import timezone

from calendarApp.forms import AppointmentForm, AppointmentServiceForm
from calendarApp.models import Appointment, AppointmentService
from erp_core.pagination import paginate
from customer.models import Address, Customer, CodeSetting
from customer.forms import ContactAddressForm, CustomerForm, CodeSettingsForm
from customer.filters import CustomerFilter
from django.db.models import Sum
from invoice.models import InvoiceItem, CustomerPurchasedPackage
from datetime import datetime
from django.contrib.auth.decorators import login_required, permission_required
from invoice.models import InvoiceItem, Invoice
from django.contrib import messages


@login_required
@permission_required('customer.view_customer_list', raise_exception=True)
def list(request):
    f = CustomerFilter(request.GET, queryset=Customer.objects.all().order_by("name"))

    customers, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)

    context = {
        'customers': customers,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "customer/list.html", context)


@login_required
@permission_required('customer.change_customer', raise_exception=True)
@transaction.atomic
def create(request):
    customer_form = CustomerForm(request.POST or None)
    customer_address_form = ContactAddressForm(request.POST or None)
    appointment_form = AppointmentForm(request.POST or None)

    ServiceInlineFormSet = inlineformset_factory(Appointment, AppointmentService, form=AppointmentServiceForm,
                                                 extra=1, can_delete=True)
    service_formset = ServiceInlineFormSet(request.POST or None)
    if request.method == "POST":

        if customer_form.is_valid() and customer_address_form.is_valid() and appointment_form.is_valid() and service_formset.is_valid():
            customer = customer_form.save()

            address = customer_address_form.save(commit=False)
            address.customer = customer
            address.save()

            services = service_formset.save(commit=False)
            if services:
                appointment = appointment_form.save(commit=False)
                appointment.customer = customer
                appointment.office = request.user.employee.office
                appointment.created_by = request.user
                appointment.save()

            for service in services:
                service.appointment = appointment
                service.save()



            if request.GET.get('product'):
                messages.add_message(request, messages.SUCCESS, "Customer created successfully")
                return HttpResponseRedirect(
                    reverse('invoice:createinvoice', args=[customer.id]))
            else:
                messages.add_message(request, messages.SUCCESS, "Customer created successfully")
                return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'customer_address_form': customer_address_form,
        'form_url': reverse_lazy('customer:create'),
        'type': "Create",
        'appointment_form': appointment_form,
        'service_formset': service_formset

    }
    return render(request, "customer/edit.html", context)


@login_required
@permission_required('customer.view_customer_list', raise_exception=True)
def detail(request, pk):
    customer = get_object_or_404(Customer, id=pk)
    address = customer.addresses
    invoices = Invoice.objects.filter(customer=customer)
    appointments = Appointment.objects.filter(customer=customer)
    completed = appointments.filter(status=Appointment.COMPLETED)
    cancelled = appointments.filter(status=Appointment.CANCELLED)
    no_shows = appointments.filter(status=Appointment.NOSHOWS)
    upcoming_appointments = appointments.filter(appointment_date_time__gte=timezone.now()).exclude(status=Appointment.CLOSED)
    past_appointments = appointments.filter(appointment_date_time__lt=timezone.now())
    purchased = CustomerPurchasedPackage.objects.filter(invoice__customer=customer)
    context = {
        'customer': customer,
        'address': address,
        'invoices': invoices,
        'sales': invoices.aggregate(Sum('grand_total')),
        'all_bookings': appointments.count(),
        'completed': completed.count(),
        'cancelled': cancelled.count(),
        'no_shows': no_shows.count(),
        'upcoming_appointments': upcoming_appointments,
        'past_appointments': past_appointments,
        'upcoming': upcoming_appointments.count(),
        'past': past_appointments.count(),
        'purchased':purchased,
    }
    return render(request, "contact/detail.html", context)


@login_required
@permission_required('customer.change_customer', raise_exception=True)
@transaction.atomic
def edit(request, pk=None):
    customer = get_object_or_404(Customer, id=pk)
    customer_address = Address.objects.filter(customer=customer)[0]

    customer_form = CustomerForm(request.POST or None, instance=customer)
    customer_address_form = ContactAddressForm(request.POST or None, instance=customer_address)

    # appointment_form = AppointmentForm(request.POST or None)
    #
    # ServiceInlineFormSet = inlineformset_factory(Appointment, AppointmentService, form=AppointmentServiceForm,
    #                                              extra=1, can_delete=True)
    # service_formset = ServiceInlineFormSet(request.POST or None)

    if request.method == "POST":

        if customer_form.is_valid() and customer_address_form.is_valid():
            customer = customer_form.save()

            address = customer_address_form.save(commit=False)
            address.customer = customer
            address.save()

            messages.add_message(request, messages.SUCCESS, "Customer edited successfully")
            return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'customer_address_form': customer_address_form,
        'form_url': reverse_lazy('customer:edit', args=[customer.pk]),
        'type': "Edit",
        'customer': customer,
        # 'appointment_form': appointment_form,
        # 'service_formset': service_formset

    }
    return render(request, "customer/edit.html", context)


@login_required
@permission_required('customer.delete_customer', raise_exception=True)
def delete(request, pk):
    if request.method == "POST":
        customer = get_object_or_404(Customer, id=pk)
        customer.delete()
    messages.add_message(request, messages.SUCCESS, "Customer deleted successfully")
    return HttpResponseRedirect(reverse('customer:list'))


def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return HttpResponseRedirect(reverse('customer:list'))

    context = {
        'code_form': code_form,
    }

    return render(request, "customer/code_setting.html", context)


@login_required
@permission_required('employee.view_report', raise_exception=True)
def sales_by_customer(request, from_date=None, to_date=None):
    if not from_date:
        from_date = timezone.now().date()
        to_date = timezone.now().date()
    invoices = Invoice.objects.filter(created_at__date__range=[from_date, to_date]).values('customer__name').annotate(
        cp=Sum('amount'), tax=Sum('tax_amount'),
        discount_given=Sum('discount_amount'), after_discount=Sum('grand_total'), selling_price=Sum('after_adjustment'),
        total_cp=Sum(F('amount') + F('tax_amount')))
    # invoices = Invoice.objects.filter(created_at__date__range=[from_date, to_date]).values('customer__name').annotate(cp=Sum('amount')).annotate(item_count=Count(F('items'))).values('item_count','cp','customer__name')
    # invoices = InvoiceItem.objects.values('invoice__customer__name') \
    #     .annotate(cp=Sum('total_price'), item_count=Count('id'),tax=Sum('tax_amount'),).values('invoice__customer__name', 'item_count', 'cp','tax')
    context = {
        'from_date': from_date,
        'to_date': to_date,
        'invoices': invoices,
    }
    return render(request, "customer/sales_by_customer.html", context)

def package_redeem(request):
    purchased = CustomerPurchasedPackage.objects.all().order_by('-id')
    context={'purchased':purchased}
    return render(request, "customer/package_redeem.html", context)
