from django.conf.urls import url
from customer import customer

app_name = "customer"


urlpatterns = [
    url(r'^customers/packageredeem/$', customer.package_redeem, name="packageredeem"),
    url(r'^customers/$', customer.list, name="list"),
    url(r'^customers/create/$', customer.create, name="create"),
    url(r'^customers/(?P<pk>[^/]+)/$', customer.detail, name="detail"),
    url(r'^customers/(?P<pk>[^/]+)/edit/$', customer.edit, name="edit"),
    url(r'^customers/(?P<pk>[^/]+)/delete/$', customer.delete, name="delete"),
    url(r'^code/settings/$', customer.code_settings, name='code_settings'),

    url(r'^reports/salesbycustomer/$', customer.sales_by_customer, name='sales_by_customer'),
    url(r'^reports/salesbycustomer/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', customer.sales_by_customer, name="sales_by_customer"),

]


