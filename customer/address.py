# Create your views here.
from django.db.models import F
from django.db import transaction
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.forms import inlineformset_factory
from django.template.loader import get_template
from django.template import Context
# from django.utils import timezone


from customer.models import Address
from customer.forms import ContactAddressForm


@transaction.atomic
def create(request, pk):
    pass

    # contact = get_object_or_404(Contact, pk=pk)
    # # Sales Order Form
    # address_form = ContactAddressForm(request.POST or None)

    # # delivery_address_select_form = DeliveryAddressSelectForm(request.POST or None, contact=quotation.contact)
    # if request.method == "POST":
    #     if address_form.is_valid():
    #         # Save Billing address
    #         address = address_form.save(commit=False)
    #         address.contact = contact
    #         address.save()

    #         return redirect(reverse('customer:contact-detail', args=[contact.id]))

    # context = {
    #     'address_form': address_form,
    #     'type': 'Edit',
    #     'contact': contact
    # }
    # return render(request, "contact/address/edit.html", context)


@transaction.atomic
def edit(request, pk):
    pass
    # address = get_object_or_404(Address, pk=pk)
    # contact = address.contact
    # # Sales Order Form
    # address_form = ContactAddressForm(request.POST or None, instance=address)

    # # delivery_address_select_form = DeliveryAddressSelectForm(request.POST or None, contact=quotation.contact)
    # if request.method == "POST":
    #     if address_form.is_valid():
    #         # Save Billing address
    #         address_form.save()

    #         return redirect(reverse('customer:contact-detail', args=[contact.id]))

    # context = {
    #     'address_form': address_form,
    #     'type': 'Edit',
    #     'contact': contact
    # }
    # return render(request, "contact/address/edit.html", context)


def delete(request, pk):
    address = get_object_or_404(Address, id=pk)
    contact = address.contact
    if request.method == "POST":
        address.delete()
    return HttpResponseRedirect(reverse('customer:contact-detail', args=[contact.pk]))
