from django import forms
from django.forms import ModelForm
from customer.models import *

class CustomerOrderForm(ModelForm):
    pass


class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "customer_name",
                                           'name': "customer_name"}),
            "email": forms.TextInput(attrs={'class': "form-control"}),
            "phone": forms.TextInput(attrs={'class': "form-control", 'required': "required","value":"971"}),
            "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['name', 'email', 'phone', 'gender']
        help_texts = {
            'phone': 'Phone number with country prefix.',
        }


class ContactAddressForm(ModelForm):
    class Meta:
        model = Address
        widgets = {
            "addressline1": forms.TextInput(
                attrs={'class': "form-control", "id": "addressline1", 'required': "required"}),
            "addressline2": forms.TextInput(attrs={'class': "form-control", "id": "addressline2"}),
            "zipcode": forms.NumberInput(attrs={'class': "form-control", "id": "zipcode", 'min': 0}),
            "country": forms.Select(attrs={'class': "form-control", 'id': "country"}),
            "state": forms.Select(attrs={'class': "form-control chained", 'id': "state"}),
            "city": forms.Select(attrs={'class': "form-control chained ", 'id': "city"}),
            "billing": forms.CheckboxInput(attrs={'id': "billing"}),
        }
        exclude = ['customer', 'area']



class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']
