import django_filters
from django import forms
from django.db.models import Q
from customer.models import Customer


class CustomerFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(label="Customer name", method="custom_search", lookup_expr='icontains',
                                       widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Customer
        fields = ['search', ]

    def custom_search(self, queryset, search, value):
        return queryset.filter(
            Q(name__icontains=value) | Q(email__icontains=value) | Q(phone__icontains=value)
        )
