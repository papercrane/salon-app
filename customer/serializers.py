# from django.db import transaction

# from rest_framework.validators import UniqueTogetherValidator
# from rest_framework.fields import SkipField
# from rest_framework import serializers
# from customer.models import Customer, Address, Contact, Phase
# from location.serializers import CountrySerializer, CitySerializer, StateSerializer


# class AddressViewSerializer(serializers.ModelSerializer):
#     country = CountrySerializer()
#     state = StateSerializer()
#     city = CitySerializer()

#     class Meta:
#         model = Address
#         fields = ('id', 'addressline1', 'addressline2', 'area', 'zipcode', 'country', 'state', 'city', 'billing')


# class AddressSerializer(serializers.ModelSerializer):
#     billing = serializers.BooleanField(default=False, required=False)

#     class Meta:
#         model = Address
#         fields = ('id', 'addressline1', 'addressline2', 'area', 'zipcode', 'country', 'state', 'city', 'billing')

#     @transaction.atomic
#     def create(self, validated_data):
#         contact = validated_data['contact']
#         if contact.addresses.exists():
#             return Address.objects.create(**validated_data)
#         validated_data['billing'] = True
#         return Address.objects.create(**validated_data)

#     @transaction.atomic
#     def update(self, instance, validated_data):
#         instance.addressline1 = validated_data.get('addressline1', instance.addressline1)
#         instance.addressline2 = validated_data.get('addressline2', instance.addressline2)
#         instance.area = validated_data.get('area', instance.area)
#         instance.zipcode = validated_data.get('zipcode', instance.zipcode)
#         instance.country = validated_data.get('country', instance.country)
#         instance.state = validated_data.get('state', instance.state)
#         instance.city = validated_data.get('city', instance.city)
#         instance.billing = validated_data.get('billing', instance.billing)
#         instance.save()

#         instance.contact.addresses.exclude(id=instance.id).update(billing=False)
#         return instance


# class CustomerContactSerializer(serializers.ModelSerializer):
#     designation = serializers.CharField(default="Owner", required=False, allow_null=True)

#     class Meta:
#         model = Contact
#         fields = ('id', 'name', 'phone', 'alternate_phone', 'email', 'designation', 'dob', 'gender')


# class ContactSerializer(serializers.ModelSerializer):
#     addresses = AddressViewSerializer(many=True)
#     designation = serializers.CharField(default="Owner", required=False, allow_null=True)

#     class Meta:
#         model = Contact
#         fields = ('id', 'name', 'phone', 'alternate_phone', 'email', 'designation', 'dob', 'gender', 'addresses')


# class PhaseSerializer(serializers.ModelSerializer):
#     created_by = serializers.StringRelatedField()

#     class Meta:
#         model = Phase
#         fields = ('name', 'created_by')


# class CustomerSelect2Serializer(serializers.ModelSerializer):
#     class Meta:
#         model = Customer
#         fields = ('name', 'id')


# class CustomerSerializer(serializers.ModelSerializer):
#     contacts = CustomerContactSerializer(many=True)
#     code = serializers.ReadOnlyField()

#     class Meta:
#         model = Customer
#         fields = ('name', 'code', 'id', 'contacts')

#     @transaction.atomic
#     def create(self, validated_data):
#         contacts_data = validated_data.pop('contacts')
#         customer = Customer.objects.create(**validated_data)

#         for contact_data in contacts_data:
#             if "addresses" in contact_data:
#                 contact_data.pop('addresses')
#             Contact.objects.create(customer=customer, **contact_data)

#         # phase = Phase.objects.create(customer=customer, created_by=self.context['request'].user.employee, **phase[0])
#         return customer

#     @transaction.atomic
#     def update(self, customer, validated_data):
#         contacts_data = self.initial_data.pop('contacts')
#         contacts_id_list = customer.contacts.values_list('id')
#         validated_data.pop('contacts')
#         for contact_data in contacts_data:
#             if "addresses" in contact_data:
#                 contact_data.pop('addresses')
#             if 'id' in contact_data:
#                 id = contact_data.pop('id')
#                 if id in contacts_id_list:
#                     contacts_id_list.remove(id)
#                     Contact.objects.get(id=id).update(**contact_data)
#         Contact.objects.filter(id__in=contacts_id_list).delete()

#         for contact_data in contacts_data:
#             if 'id' not in contact_data:
#                 Contact.objects.create(customer=customer, **contact_data)

#         if "phases" in validated_data:
#             validated_data.pop('phases')
#         customer.update(**validated_data)

#         return customer


# class ContactViewSerializer(serializers.ModelSerializer):
#     addresses = AddressViewSerializer(many=True)
#     designation = serializers.CharField(default="Owner", required=False, allow_null=True)
#     customer = CustomerSelect2Serializer(read_only=True)

#     class Meta:
#         model = Contact
#         fields = ('id', 'name', 'phone', 'alternate_phone', 'email', 'designation', 'dob', 'gender', 'addresses', 'customer')
