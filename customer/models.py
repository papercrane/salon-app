from django.db import models

from address.models import Address as AbstractAddress
from erp_core.models import BaseModel, BaseCodeSetting
from django.db import transaction


class Customer(BaseModel):
    GENDER = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )

    def newCode():
        return CodeSetting.new_code()

    code = models.CharField(max_length=100, unique=True, blank=True, null=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField("Phone Number", max_length=20, null=True, blank=True)
    buddy_deal = models.BooleanField(default=False)
    designer_sinora_package_date = models.DateField("Expiry Date",null=True, blank=True)
    gender = models.CharField(max_length=20, choices=GENDER, blank=True)

    @transaction.atomic
    def delete(self, force_delete=False, *args, **kwargs):
        self.addresses.delete()
        return super(BaseModel, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()
        return super(Customer, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        permissions = (
            ("view_customer_list", "Can view customer list"),

        )


class Address(AbstractAddress):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=False, related_name="addresses")
    billing = models.BooleanField(default=False)

    def __str__(self):
        address_fields = ['addressline1', 'addressline2', 'area', 'country', 'state', 'city']

        address_strings = []
        for field in address_fields:
            if hasattr(self, field):
                if getattr(self, field, None):
                    address_strings.append(str(getattr(self, field)))
        connector = " ,"
        return connector.join(address_strings)

    def save(self, *args, **kwargs):
        if self.billing is True:
            self.customer.addresses.update(billing=False)
        return super(Address, self).save(*args, **kwargs)


class CodeSetting(BaseCodeSetting):
    pass


