from django import forms
from .models import Bill
from django.forms import ModelForm


class BillForm(ModelForm):
    class Meta:
        model = Bill

        widgets = {

            "bill_type": forms.Select(attrs={'id': "bill_type", 'class': 'form-control', 'required': "required"}),
            "name": forms.TextInput(attrs={'class': "form-control"}),
            "amount": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "date": forms.DateInput(attrs={'class': "form-control tdate", "type": "text", 'required': "required"}),
        }
        fields = ['bill_type', 'name', 'amount', 'date']
