import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from .models import *
# from asset.stock.models import ItemAttribute
# from employee.models import Employee


class BillFilter(django_filters.FilterSet):
    bill_type = django_filters.ChoiceFilter(choices=Bill.TYPES, widget=forms.Select(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    amount = django_filters.NumberFilter(label="Amount", name='amount',
                                         widget=forms.NumberInput(attrs={'class': 'form-control'}))
    date = django_filters.DateFilter(label="Given date", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    class Meta:
        model = Bill
        fields = ['bill_type', 'name', 'amount', 'date']
