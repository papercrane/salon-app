from django.conf.urls import url, include
# from expense.bill.urls import urlpatterns as bill_patterns
from . import views

app_name = 'bill'

urlpatterns = [
    # url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^list/$', views.list, name='list'),
    url(r'^add/$', views.add_bill, name='add_bill'),
    url(r'^(?P<pk>[^/]+)/edit/$', views.edit_bill, name='edit_bill'),
    url(r'^(?P<pk>[^/]+)/details/$', views.detail, name='detail'),

]
