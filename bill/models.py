from django.db import models
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator


class Bill(ErpAbstractBaseModel):

    ELECTRICITYBILL = "Electricity Bill"
    OTHER = "Other"

    TYPES = (
        (ELECTRICITYBILL, "Electricity Bill"),
        (OTHER, "Other"),

    )
    bill_type = models.CharField(max_length=65, choices=TYPES)
    name = models.CharField(max_length=255, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[
                                 MinValueValidator(0)], null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    paid = models.BooleanField(default=False)

    def __str__(self):
        return str(self.bill_type) + str(self.date)
