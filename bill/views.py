from django.shortcuts import get_object_or_404, render, redirect
from .models import Bill
from erp_core.pagination import paginate
from .forms import BillForm
from .filters import BillFilter
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse, reverse_lazy
from .models import *
import datetime
from django.contrib import messages


def list(request):
    bill = Bill.objects.all()
    remainder = request.GET.get('remainder')
    due = request.GET.get('due')
    if remainder:
        today = datetime.date.today()
        end_week = today + datetime.timedelta(7)
        f = BillFilter(request.GET, queryset=bill.filter(date__gte=today, date__lte=end_week).order_by('date'))
        remainder = "remainder"
    elif due:
        today = datetime.date.today()
        f = BillFilter(request.GET, queryset=bill.filter(date__lte=today).order_by('date'))
        due = "due"

    else:
        f = BillFilter(request.GET, queryset=bill)
        remainder = ""

    bills, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    context = {
        "bills": bills,
        'pagination': pagination,
        'filter': f,
        'remainder': remainder,
        'due': due
    }
    return render(request, 'bills/list.html', context)


def add_bill(request):

    form = BillForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            bill = form.save(commit=False)
            bill.save()
            messages.add_message(request, messages.SUCCESS, "Bill created successfully")
            return HttpResponseRedirect(reverse('bill:detail', args=[bill.pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('bill:add_bill'),
        "type": "add"
    }
    return render(request, 'bills/create.html', context)


def detail(request, pk):
    bill = get_object_or_404(Bill, pk=pk)
    context = {
        'bill': bill,
    }

    return render(request, "bills/detail.html", context)


def edit_bill(request, pk):

    bill = Bill.objects.get(id=pk)
    form = BillForm(request.POST or None, instance=bill)
    if request.method == "POST":
        bill = form.save()
        messages.add_message(request, messages.SUCCESS, "Bill edited successfully")
        return HttpResponseRedirect(reverse('bill:detail', args=[bill.pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('bill:edit_bill', args=[pk]),
        "bill": bill,
        "type": "edit"
    }
    return render(request, 'bills/create.html', context)
