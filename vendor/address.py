from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.db import transaction
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.forms import inlineformset_factory
from django.template.loader import get_template
from django.template import Context
# from django.utils import timezone


from vendor.models import VendorAddress, VendorContact
from vendor.forms import VendorContactAddressForm


# create address of a contact
@login_required
@transaction.atomic
def create(request, pk):
    contact = get_object_or_404(VendorContact, pk=pk)
    # Sales Order Form
    address_form = VendorContactAddressForm(request.POST or None)

    # delivery_address_select_form = DeliveryAddressSelectForm(request.POST or None, contact=quotation.contact)
    if request.method == "POST":
        if address_form.is_valid():
            # Save Billing address
            address = address_form.save(commit=False)
            address.contact = contact
            address.save()
            messages.add_message(request, messages.INFO, 'Address added successfully')
            return redirect(reverse('vendor:vendor-contact-detail', args=[contact.id]))

    context = {
        'address_form': address_form,
        'type': 'Add',
        'contact': contact
    }
    return render(request, "vendor_address/edit.html", context)


# edit the address of contact
@login_required
@transaction.atomic
def edit(request, pk):
    address = get_object_or_404(VendorAddress, pk=pk)
    contact = address.contact
    # Sales Order Form
    address_form = VendorContactAddressForm(request.POST or None, instance=address)

    # delivery_address_select_form = DeliveryAddressSelectForm(request.POST or None, contact=quotation.contact)
    if request.method == "POST":
        if address_form.is_valid():
            # Save Billing address
            address_form.save()
            messages.add_message(request, messages.INFO, 'Edit successful')
            return redirect(reverse('vendor:vendor-contact-detail', args=[contact.id]))

    context = {
        'address_form': address_form,
        'type': 'Edit',
        'contact': contact
    }
    return render(request, "vendor_address/edit.html", context)


# delete the address of contact
@login_required
def delete(request, pk):
    if request.method == "POST":
        address = get_object_or_404(VendorAddress, id=pk)

        address.delete()
        messages.add_message(request, messages.INFO, 'Deletion successful')
    return HttpResponseRedirect(reverse('vendor:vendor-contact-detail', args=[address.contact.id]))
