from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import inlineformset_factory

from erp_core.pagination import paginate
from vendor.models import VendorContact, Vendor, CodeSetting
from vendor.forms import VendorForm, VendorContactForm, CodeSettingsForm
from vendor.filters import VendorFilter
from django.contrib import messages

@login_required
def index(request):
    f = VendorFilter(request.GET, queryset=Vendor.objects.all().order_by("-created_at"))
    vendors, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
    context = {
        'vendors': vendors,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "vendor/list.html", context)


# create vendor
@login_required
@transaction.atomic
def create(request):
    ContactInlineFormSet = inlineformset_factory(
        Vendor, VendorContact, form=VendorContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    contact_formset = ContactInlineFormSet(request.POST or None)

    vendor_form = VendorForm(request.POST or None)
    if request.method == "POST":
        if vendor_form.is_valid() and contact_formset.is_valid():
            vendor = vendor_form.save(commit=False)
            contacts = contact_formset.save(commit=False)
            for contact in contacts:
                contact.vendor = vendor
                contact.save()
            vendor.save()
            vendor_form.save_m2m()
            messages.add_message(request, messages.SUCCESS, "Vendor created successfully")
            return HttpResponseRedirect(reverse('vendor:detail', args=[vendor.pk]))

    context = {
        'vendor_form': vendor_form,
        'contact_formset': contact_formset,
        'form_url': reverse_lazy('vendor:create'),
        'type': "Create"

    }
    return render(request, "vendor/edit.html", context)


# details of vendor
@login_required
def detail(request, pk):
    vendor = get_object_or_404(Vendor, id=pk)
    contacts = vendor.vendor_contacts
    context = {
        'vendor': vendor,
        'contacts': contacts,

    }
    return render(request, "vendor/detail.html", context)


# edit vendor
@login_required
@transaction.atomic
def edit(request, pk=None):
    vendor = get_object_or_404(Vendor, id=pk)

    vendor_form = VendorForm(request.POST or None, instance=vendor)

    ContactInlineFormSet = inlineformset_factory(
        Vendor, VendorContact, form=VendorContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    contact_formset = ContactInlineFormSet(request.POST or None, instance=vendor)

    # pprint.pprint(request.POST)
    if request.method == "POST":

        if vendor_form.is_valid() and contact_formset.is_valid():
            vendor_form.save()
            contact_formset.save()
            messages.add_message(request, messages.SUCCESS, "Vendor edited successfully")
            return HttpResponseRedirect(reverse('vendor:detail', args=[vendor.id]))

    context = {
        'vendor_form': vendor_form,
        'contact_formset': contact_formset,
        'form_url': reverse_lazy('vendor:edit', args=[vendor.pk]),
        'type': "Edit",
        'vendor': vendor,

    }
    return render(request, "vendor/edit.html", context)


# delete vendor
@login_required
def delete(request, pk):
    if request.method == "POST":
        vendor = get_object_or_404(Vendor, id=pk)
        vendor.delete()
    messages.add_message(request, messages.SUCCESS, "Vendor deleted successfully")
    return HttpResponseRedirect(reverse('vendor:list'))


# code setting for vendor
@login_required
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return HttpResponseRedirect(reverse('vendor:list'))

    context = {
        'code_form': code_form,
    }

    return render(request, "vendor/code_setting.html", context)
