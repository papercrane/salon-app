from django import forms
from django.forms import ModelForm
from vendor.models import VendorContact, Vendor, VendorAddress, CodeSetting


class VendorForm(ModelForm):

    class Meta:
        model = Vendor
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "cst": forms.TextInput(attrs={'class': "form-control"}),
            "pan": forms.TextInput(attrs={'class': "form-control"}),
            "type": forms.Select(attrs={'class': "form-control", 'id': "type", 'required': "required"}),
            "services": forms.SelectMultiple(attrs={'class': "form-control", 'id': "services", 'required': "required", "data-toggle": "select", 'multiple': "multiple"}),

        }
        fields = ['name', 'code', 'cst', 'pan', 'type', 'services']


class VendorContactForm(ModelForm):

    class Meta:
        model = VendorContact
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "email": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "phone": forms.TextInput(attrs={'class': "form-control",}),
            "designation": forms.TextInput(attrs={'class': "form-control"}),
            "gender": forms.Select(attrs={'class': "form-control"}),
        }
        fields = ['name', 'gender', 'email', 'designation', 'phone', ]
        help_texts = {
            'phone': 'Phone number with country prefix.',
        }


class VendorContactAddressForm(ModelForm):

    class Meta:
        model = VendorAddress
        widgets = {
            "addressline1": forms.TextInput(attrs={'class': "form-control", "id": "addressline1",'required':True}),
            "addressline2": forms.TextInput(attrs={'class': "form-control", "id": "addressline2",'required':True}),
            "area": forms.TextInput(attrs={'class': "form-control", "id": "area",}),
            "zipcode": forms.NumberInput(attrs={'class': "form-control", "id": "zipcode", 'min': 0}),
        }
        fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'country', 'state', 'city']


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }
        fields = ['prefix', 'count_index', 'no_of_characters']
