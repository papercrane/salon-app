import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from vendor.models import Vendor


class VendorFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(label="code", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    name = django_filters.CharFilter(label="Vendor name", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    contact = django_filters.CharFilter(label="Contact Name", method='filter_by_contact', widget=forms.TextInput(attrs={'class': 'form-control'}))

    # created_at = django_filters.DateTimeFromToRangeFilter(
    #     name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Vendor
        fields = ['code', 'name', 'contact']

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(vendor_contacts__name__icontains=value) | Q(vendor_contacts__vendor__name__icontains=value)
        ).distinct()

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
