from django.contrib import admin
from .models import *

admin.site.register(Vendor)
admin.site.register(VendorContact)
admin.site.register(VendorAddress)
admin.site.register(Services)

# Register your models here.
