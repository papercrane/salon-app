from django.conf.urls import url
from vendor import vendor, contact, address

app_name = "vendor"


urlpatterns = [
    url(r'^$', vendor.index, name="list"),
    url(r'^create/$', vendor.create, name="create"),
    url(r'^(?P<pk>[^/]+)/details/$', vendor.detail, name="detail"),
    url(r'^(?P<pk>[^/]+)/edit/$', vendor.edit, name="edit"),
    url(r'^contacts/(?P<pk>[^/]+)/$', contact.detail, name="vendor-contact-detail"),
    url(r'^contacts/(?P<pk>[^/]+)/addresses/create/$', address.create, name="address_create"),
    url(r'^contact/addresses/(?P<pk>[^/]+)/edit/$', address.edit, name="address_edit"),
    url(r'^contact/addresses/(?P<pk>[^/]+)/delete/$', address.delete, name="address_delete"),
    url(r'^contact/(?P<pk>[^/]+)/delete/$', contact.delete, name="contact_delete"),
    url(r'^(?P<pk>[^/]+)/delete/$', vendor.delete, name="vendor_delete"),
    url(r'^code/settings/$', vendor.code_settings, name='code_settings'),
]
