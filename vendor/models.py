from django.conf import settings
from django.db import models
from address.models import Address as AbstractAddress
from address.models import Contact as AbstractContact
from erp_core.models import BaseModel, BaseCodeSetting
from django.db import transaction
from django.shortcuts import reverse


class Services(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Vendor(BaseModel):
    TYPES = (
        ('prospects', "PROSPECTS"),
        ('blacklisted', "BLACKLISTED"),
        ('active', "ACTIVE"),
    )

    def newCode():
        return CodeSetting.new_code()

    code = models.CharField(max_length=100, unique=True, default=newCode)
    name = models.CharField(max_length=255)
    cst = models.CharField(max_length=255, null=True, blank=True)
    pan = models.CharField(max_length=255, null=True, blank=True)
    type = models.CharField(max_length=30, choices=TYPES, default=TYPES[0])
    services = models.ManyToManyField('vendor.Services', related_name="vendor_services")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    refered_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        name = self.name
        if self.code:
            name += "(" + str(self.code) + ")"
        return name

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()

        return super(Vendor, self).save(*args, **kwargs)

    @transaction.atomic
    def delete(self, force_delete=False, *args, **kwargs):
        self.vendor_contacts.delete()
        return super(Vendor, self).delete(*args, **kwargs)


class VendorContact(AbstractContact):

    GENDER = (
        ('MALE', 'MALE'),
        ('FEMALE', 'FEMALE'),
    )

    gender = models.CharField(max_length=20, choices=GENDER, null=True,blank=True)

    name = models.CharField(max_length=255, null=True, blank=False)
    email = models.EmailField(unique=True)
    vendor = models.ForeignKey('vendor.Vendor', on_delete=models.CASCADE, related_name="vendor_contacts")
    designation = models.CharField(max_length=100, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)

    tin = models.CharField(max_length=255, null=True, blank=True)
    gstin_provisional_no = models.CharField(max_length=255, null=True, blank=True)
    gstin_actual_no = models.CharField(max_length=255, null=True, blank=True)

    # def __str__(self):
    #     return str(self.name + '(' + self.designation + '), ' + self.vendor.name)

    label = "Job Title"

    @transaction.atomic
    def delete(self, force_delete=False, *args, **kwargs):
        self.vendor_addresses.delete()
        return super(BaseModel, self).delete(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("vendor:vendor-contact-detail", args=[self.id])


class VendorAddress(AbstractAddress):
    contact = models.ForeignKey(VendorContact, on_delete=models.CASCADE,
                                null=True, blank=False, related_name="vendor_addresses")

    def __str__(self):
        address_fields = ['addressline1', 'addressline2', 'area', 'country', 'state', 'city']

        address_strings = []
        for field in address_fields:
            if hasattr(self, field):
                if getattr(self, field, None):
                    address_strings.append(str(getattr(self, field)))
        connector = " ,"
        return connector.join(address_strings)

    def save(self, *args, **kwargs):
        return super(VendorAddress, self).save(*args, **kwargs)


class CodeSetting(BaseCodeSetting):
    pass
