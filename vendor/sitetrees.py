from sitetree.utils import item, tree

sitetrees = (

    tree('sidebar', items=[
        item(
            'Vendor',
            'vendor:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user-circle-o",
        ),
    ]),
)
