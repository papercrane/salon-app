from django.conf.urls import url, include
from expense.bill.urls import urlpatterns as bill_patterns
from . import views

app_name = 'expense'

urlpatterns = [
    url(r'^(?P<pk>[^/]+)/detail/$', views.detail, name='detail'),
    url(r'^bills/', include(bill_patterns, namespace="bill")),
    url(r'^recordExpense/create/(?P<payee_type>[\w-]+)/$', views.record_expense_create, name='record_expense_create'),
    url(r'^bulkAddExpense/create/(?P<payee_type>[\w-]+)/$', views.bulk_expense_create, name='bulk_expense_create'),
    url(r'^$', views.index, name='index'),
    url(r'^(?P<pk>[^/]+)/details/$', views.view_expense, name='view_expense'),
    url(r'^(?P<pk>[^/]+)/edit/$', views.edit_Expense, name='edit_Expense'),
    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    url(r'^expense_type_list/$', views.expense_type_list, name='expense_type_list'),
    url(r'^(?P<pk>[^/]+)/delete/$', views.delete, name="delete"),
    url(r'^supplier_or_vendor/$', views.supplier_or_vendor, name="supplier_or_vendor"),
    url(r'^supplier_or_vendor/bulk_create/$', views.supplier_or_vendor, {'bulk': True}, name="bulk_supplier_or_vendor"),
]
