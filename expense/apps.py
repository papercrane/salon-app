from django.apps import AppConfig


class ExpenseConfig(AppConfig):
	name = 'expense'

	def ready(self):
		from .models import TypeOfExpense
		TypeOfExpense.objects.update_or_create(name="Reimbursement")