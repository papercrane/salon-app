import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from expense.models import Expense, TypeOfExpense
from vendor.models import Vendor
from customer.models import Customer
from expense.bill.models import PAYEE_TYPES
from django.apps import apps
from django.contrib.contenttypes.models import ContentType


class ExpenseFilter(django_filters.FilterSet):
    # date = django_filters.DateRangeFilter(label="Date", lookup_expr='icontains',
    #                                       widget=forms.TextInput(attrs={'class': 'form-control tdate'}))

    expense_type = django_filters.ModelChoiceFilter(
        label="Expense Type",
        queryset=TypeOfExpense.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )

    amount = django_filters.NumberFilter(label='Amount',
                                         widget=forms.NumberInput(attrs={'class': 'form-control'}))

    paid = django_filters.BooleanFilter(
        name="paid",
        widget=widgets.BooleanWidget(attrs={'class': 'form-control '})
    )

    payee_type = django_filters.ChoiceFilter(
        method="filter_by_payee_type",
        choices=PAYEE_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    payee_name = django_filters.CharFilter(
        label="Payee Name",
        name="payee_id",
        method='filter_by_payee',
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )

    refernce = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))

    customer = django_filters.ModelChoiceFilter(
        label='Customer',
        queryset=Customer.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )

    created_at = django_filters.DateTimeFromToRangeFilter(
        name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_amount = django_filters.ChoiceFilter(label="amount", name="amount", method='order_by_field', choices=(
        ('amount', 'Ascending'), ('-amount', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Expense
        fields = ['expense_type', 'amount', 'paid', 'refernce', 'customer']

    def __init__(self, *args, **kwargs):
        super(ExpenseFilter, self).__init__(*args, **kwargs)
        # self.fieldsi\

    def filter_by_payee_type(self, queryset, name, value):
        ct = ContentType.objects.get(model=value)
        return queryset.filter(payee_type_id=ct.id)

    def filter_by_payee(self, queryset, name, value):
        payee_type = self.request.GET.get('payee_type', False)
        if payee_type:
            ct = ContentType.objects.get(model=payee_type)
            payed_to = apps.get_model(ct.app_label, payee_type)
            payee_ids = payed_to.objects.filter(name__icontains=value).values_list('id')
            return queryset.filter(payee_id__in=payee_ids)

        return queryset

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
