from rest_framework import serializers
from .models import TypeOfReimbursement, SalesReimbursement, SalesExpense, TypeOfExpense
from django.shortcuts import get_object_or_404, render
from enquiry.models import Enquiry, Activity
from django.core.files.base import ContentFile
from enquiry.serializers import ActivityViewSerializer,EnquiryViewSerializer
from customer.serializers import ContactSerializer
import base64
import uuid
import json


class TypeOfReimbursementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeOfReimbursement
        fields = ('id', 'name')


class EnquiryForReimbursementSerializer(serializers.Serializer):
    contact = ContactSerializer()

    class Meta:
        model = Enquiry
        fields = ('id', 'contact')


# class EnquiryViewSerializer(serializers.ModelSerializer):
#     contact = ContactSerializer()

#     class Meta:
#         model = Enquiry
#         fields = ('id', 'contact', 'code')


class SalesReimbursementSerializer(serializers.Serializer):
    reimbursement_type = serializers.CharField()
    photo = serializers.CharField()
    enquiry = serializers.CharField()
    activity = serializers.CharField()
    amount = serializers.DecimalField(max_digits=200, decimal_places=2)
    date_and_time = serializers.DateTimeField()
    title = serializers.CharField()

    def create(self, validated_data):
        photo = self.validated_data['photo']
        request = self.context['request']
        image = base64.b64decode(photo)
        image = ContentFile(image, str(uuid.uuid4()) + '.png')
        s = SalesReimbursement(reimbursement_type_id=self.validated_data['reimbursement_type'],
                               photo=image, enquiry_id=self.validated_data['enquiry'],
                               activity_id=self.validated_data['activity'], amount=self.validated_data['amount'], date_and_time=self.validated_data['date_and_time'], title=self.validated_data['title'])
        s.employee = request.user.employee
        s.save()
        SalesExpense.objects.create(sales_expense_type=TypeOfExpense.objects.get(name="Reimbursement"),
                                    total_amount=self.validated_data['amount'], date_and_time=self.validated_data['date_and_time'])

        return validated_data


class ReimbursementListSerializer(serializers.ModelSerializer):
    reimbursement_type = TypeOfReimbursementSerializer()
    enquiry = EnquiryViewSerializer()
    activity = ActivityViewSerializer()

    class Meta:
        model = SalesReimbursement
        fields = ('id', 'enquiry', 'activity', 'reimbursement_type',
                  'date_and_time', 'amount', 'title', 'approved_by','enquiry','photo')


class ReimbursementViewSerializer(serializers.ModelSerializer):
    reimbursement_type = TypeOfReimbursementSerializer()
    enquiry = EnquiryForReimbursementSerializer()
    activity = ActivityViewSerializer()

    class Meta:
        model = SalesReimbursement
        exclude = ('employee',)
