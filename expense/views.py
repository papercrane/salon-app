from django.contrib.auth.decorators import login_required
from .forms import *
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse, reverse_lazy
from .models import Employee
from django.db.models import Q, F
from erp_core.pagination import paginate

from django.forms import modelformset_factory
from expense.models import TypeOfExpense
from expense.filters import ExpenseFilter
from django.contrib import messages

@login_required
def add_SalesReimbursement(request, pk):
    user = request.user.employee
    form = SalesReimbursementForm(
        request.POST or None, request.FILES or None, current_user=user)
    if request.method == "POST":
        if form.is_valid():
            salesreimbursement = form.save(commit=False)
            salesreimbursement.employee = get_object_or_404(Employee, id=pk)
            salesreimbursement.save()
            return HttpResponseRedirect(reverse('expense:view_SalesReimbursement', args=[pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('expense:add_SalesReimbursement', args=[pk]),
        "pk": pk,
        "type": "add"
    }
    return render(request, 'SalesReimbursement/add_SalesReimbursement.html', context)


@login_required
def view_SalesReimbursement(request, pk):
    salesreimbursements = SalesReimbursement.objects.filter(employee_id=pk).order_by("-created_at")
    salesreimbursements, pagination = paginate(request, queryset=salesreimbursements, per_page=10, default=True)
    context = {
        "salesreimbursements": salesreimbursements,
        "pk": pk,
        'pagination': pagination
    }
    return render(request, 'SalesReimbursement/view_SalesReimbursement.html', context)

@login_required
def edit_SalesReimbursement(request, pk, salesreimbursement_pk):

    # target = get_object_or_404(Targets, id=target_pk)
    user = request.user.employee
    salesreimbursement = SalesReimbursement.objects.get(
        id=salesreimbursement_pk)
    form = SalesReimbursementForm(
        request.POST or None, request.FILES or None, instance=salesreimbursement, current_user=user)
    if request.method == "POST":
        form.save()
        return HttpResponseRedirect(reverse('expense:view_SalesReimbursement', args=[pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('expense:edit_SalesReimbursement', args=[pk, salesreimbursement_pk]),
        "pk": pk,
        "salesreimbursement_pk": salesreimbursement_pk,
        "type": "edit"
    }
    return render(request, 'SalesReimbursement/add_SalesReimbursement.html', context)

@login_required
def detail(request, pk):
    # quotation = get_object_or_404(Quotation, id=pk)
    salesreimbursements = SalesReimbursement.objects.get(id=pk)
    context = {
        "salesreimbursement": salesreimbursements,
        "pk": pk
    }
    return render(request, "SalesReimbursement/detail.html", context)

# @login_required
# def approval_SalesReimbursement(request, pk):
#     date = datetime.datetime.now()
#
#     salesreimbursement = SalesReimbursement.objects.get(id=pk)
#     if salesreimbursement.employee.reporting_officer == request.user.employee:
#         salesreimbursement.approved_by = request.user.employee
#         salesreimbursement.approved_date = date
#
#         salesreimbursement.save()
#
#         month_year = date.strftime("%B %Y")
#
#         try:
#             salary = MonthlySalary.objects.get(paid=False, employee=request.user.employee, month_year=month_year)
#         except Exception as e:
#             messages.add_message(request, messages.INFO, e)
#             return HttpResponseRedirect(reverse('expense:detail', args=[pk]))
#         salary.sales_reimbursement = salary.sales_reimbursement + salesreimbursement.amount
#         salary.amount = salary.amount + salesreimbursement.amount
#         salary.save()
#
#     else:
#         # messages.info(request, 'No Permission to approve')
#         messages.add_message(request, messages.INFO, 'No Permission to approve')
#     return HttpResponseRedirect(reverse('expense:detail', args=[pk]))

@login_required
def view_approvalSalesReimbursement(request, pk):
    salesreimbursements = SalesReimbursement.objects.filter(
        employee__reporting_officer=request.user.employee).distinct().order_by("-created_at")
    salesreimbursements, pagination = paginate(request, queryset=salesreimbursements, per_page=20, default=True)
    context = {
        "salesreimbursements": salesreimbursements,
        "pk": pk,
        'pagination': pagination
    }
    return render(request, 'SalesReimbursement/view_SalesReimbursement.html', context)

@login_required
def record_expense_create(request, payee_type):

    expense_form = ExpenseForm(request.POST or None, request.FILES or None)
    payee_form = PayeeForm(request.POST or None, payee_type=payee_type)

    if request.method == "POST":
        if expense_form.is_valid() and payee_form.is_valid():
            expense = expense_form.save(commit=False)
            expense.payee = payee_form.cleaned_data['payee']
            expense.save()
            expense_form.save_m2m()
            messages.add_message(request, messages.SUCCESS, "Expense created successfully")
            return HttpResponseRedirect(reverse('expense:view_expense', args=[expense.pk]))
    context = {
        "expense_form": expense_form,
        'form_url': reverse_lazy('expense:record_expense_create', args=[payee_type]),
        "type": "add",
        "payee_form": payee_form,
    }
    return render(request, 'expense/record_expense_create.html', context)


@login_required
def bulk_expense_create(request, payee_type):

    ExpenseFormSet = modelformset_factory(Expense, form=ExpenseFormsetForm, extra=0, min_num=2, max_num=100, can_delete=True)
    expense_formset = ExpenseFormSet(queryset=Expense.objects.none())
    payee_form = PayeeForm(request.POST or None, payee_type=payee_type)

    if request.method == "POST":
        expense_formset = ExpenseFormSet(request.POST)
        if expense_formset.is_valid() and payee_form.is_valid():
            # expense.payee = payee_form.cleaned_data['payee']
            expenses = expense_formset.save(commit=False)
            for expense in expenses:
                expense.payee = payee_form.cleaned_data['payee']
                expense.save()
            messages.add_message(request, messages.SUCCESS, "Expense created successfully")
            return HttpResponseRedirect(reverse('expense:index'))

    context = {
        "expense_formset": expense_formset,
        'form_url': reverse_lazy('expense:bulk_expense_create', args=[payee_type]),
        "type": "add",
        "payee_form": payee_form,
    }
    return render(request, 'expense/bulk_expense_create.html', context)

@login_required
def index(request, payee_type=None):

    f = ExpenseFilter(request.GET, request=request, queryset=Expense.objects.all().order_by("-created_at"))
    # expenses = Expense.objects.all().order_by("-created_at")

    expenses, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    # print("expenses...........", expenses)
    context = {
        'expenses': f.qs,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "expense/list.html", context)

@login_required
def view_expense(request, pk):
    expense = Expense.objects.get(id=pk)
    context = {
        "expense": expense,
        "pk": pk
    }
    return render(request, "expense/details.html", context)

@login_required
def edit_Expense(request, pk):

    expense = Expense.objects.get(id=pk)
    expense_form = ExpenseForm(request.POST or None, request.FILES or None, instance=expense)
    if request.method == "POST":
        expense_form.save()
        messages.add_message(request, messages.SUCCESS, "Expense edited successfully")
        return HttpResponseRedirect(reverse('expense:view_expense', args=[pk]))
    context = {
        "expense_form": expense_form,
        "expense": expense,
        'form_url': reverse_lazy('expense:edit_Expense', args=[pk]),
        "pk": pk,
        "type": "edit"
    }
    return render(request, 'expense/record_expense_create.html', context)

@login_required
def dashboard(request):
    return render(request, "expense/dashboard.html")

@login_required
def expense_type_list(request):
    expense_name = request.GET.get('expense_name', False)
    # print("expense_name........", expense_name)
    if expense_name:
        expense = TypeOfExpense.objects.filter(
            name__icontains=expense_name
        ).annotate(
            text=F('name')
        ).values('id', 'name')

        # print("batch list.......", expense)
        # x = list(expense)
        # print("x..............", x)
        return JsonResponse(list(expense), safe=False)
    return JsonResponse(list(), safe=False)


@login_required
def delete(request, pk):
    if request.method == "POST":
        expense = get_object_or_404(Expense, id=pk)
        expense.delete()
        messages.add_message(request, messages.SUCCESS, "Expense deleted successfully")

    return HttpResponseRedirect(reverse('expense:index'))


@login_required
def supplier_or_vendor(request, bulk=False):
    form = ExpenserForm(request.POST or None)
    if bulk:
        form_url = reverse('expense:bulk_supplier_or_vendor')
    else:
        form_url = reverse('expense:supplier_or_vendor')

    if request.method == 'POST':
        if form.is_valid():
            payee_type = form.cleaned_data['payee_type']
            if bulk:
                return redirect(reverse("expense:bulk_expense_create", args=[payee_type]))
            else:
                return redirect(reverse("expense:record_expense_create", args=[payee_type]))

    context = {
        'form': form,
        'form_url': form_url,
    }
    return render(request, "expense/supplier_or_vendor.html", context)
