from django.contrib import admin
from .models import *


# admin.site.register(TypeOfReimbursement)
admin.site.register(TypeOfExpense)
# admin.site.register(SalesReimbursement)
# admin.site.register(SalesExpense)
admin.site.register(Expense)
