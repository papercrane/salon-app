from django.conf.urls import include, url
from . import views, receipt


urlpatterns = [
    url(r'^$', views.index, name="list"),
    url(r'^pre_create/$', views.pre_create, name="pre-create"),
    url(r'^create/(?P<payee_type>[^/]+)/(?P<order_type>[^/]+)/$', views.create, name="create"),

    url(r'^code/settings/$', views.code_settings, name='code_settings'),

    url(r'^(?P<bill_pk>[^/]+)/', include([
        url(r'^edit/$', views.edit, name="edit"),
        url(r'^delete/$', views.delete, name="delete"),
        url(r'^add_payment_term/$', views.add_payment_terms, name='add-payment-terms'),
        url(r'^$', views.detail, name='detail'),
    ])),

    # url(r'^(?P<payee_type>[^/]+)/(?P<order_type>[^/]+)/$', views.index, name="filtered-list"),
    url(r'^payment_term/(?P<bill_payment_term_pk>[^/]+)/', include([
        url(r'^delete/$', views.delete_payment_term, name="delete"),

        url(r'^receipts/', include([
            url(r"^$", receipt.index, name="list"),
            url(r"^add/$", receipt.create, name="create"),
            url(r'^(?P<receipt_pk>[^/]+)/', include([
                url(r"download/$", receipt.download, name="download"),
                url(r"^edit/$", receipt.edit, name="edit"),
                url(r"^delete/$", receipt.delete, name="delete"),
            ])),
        ], namespace="receipt")),
    ], namespace='payment_term'))
]
