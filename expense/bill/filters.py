import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from .models import Bill, PAYEE_TYPES, ORDER_TYPES
from customer.models import Customer
from django.apps import apps
from django.contrib.contenttypes.models import ContentType

class BillFilter(django_filters.FilterSet):

    code = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))

    amount = django_filters.NumberFilter(widget=forms.Select(attrs={'class': 'form-control'}))
    amount__gt = django_filters.NumberFilter(
        name='amount', lookup_expr='gt',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    amount__lt = django_filters.NumberFilter(
        name='amount', lookup_expr='lt',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )

    status = django_filters.ChoiceFilter(
        name="status",
        choices=Bill.STATUSES,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    payee_type = django_filters.ChoiceFilter(
        method="filter_by_payee_type",
        choices=PAYEE_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    payee_name = django_filters.CharFilter(
        label="Payee Name",
        name="payee_id",
        method='filter_by_payee',
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )

    order_type = django_filters.ChoiceFilter(
        method="filter_by_order_type",
        choices=ORDER_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    order_code = django_filters.CharFilter(
        label="Order Number/Code",
        name="order_id",
        method='filter_by_order',
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )


    created_at = django_filters.DateTimeFromToRangeFilter(
        name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    due_date = django_filters.DateTimeFromToRangeFilter(
        name='due_date', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}),
    )

    order_by_amount = django_filters.ChoiceFilter(
        label="amount", name="amount", method='order_by_field',
        choices=(('amount', 'Ascending'), ('-amount', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    order_by_due_date = django_filters.ChoiceFilter(
        label="Due Date", name="due_date", method='order_by_field',
        choices=(('due_date', 'Ascending'), ('-due_date', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'}))

    order_by_credit_periods = django_filters.ChoiceFilter(
        label="Credit Periods", name="credit_periods", method='order_by_field', choices=(
            ('credit_periods', 'Ascending'), ('-credit_periods', 'Descending'),
        ),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    order_by_created_at = django_filters.ChoiceFilter(
        label="Created At", name="created_at", method='order_by_field', choices=(
            ('created_at', 'Ascending'), ('-created_at', 'Descending'),
        ),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Bill
        fields = ['code', 'amount', 'due_date', 'status', 'created_at']

    def __init__(self, *args, **kwargs):
        super(BillFilter, self).__init__(*args, **kwargs)
        # self.fieldsi\


    @property
    def qs(self):
        queryset = super(BillFilter, self).qs

        # obj = parent['queryset']
        if self.request.GET.get('vendor_type', False):
            queryset = queryset.filter(vendor_type__name=self.request.GET['vendor_type'])
        return queryset


        # print("contact name: ", value.id)
        # return queryset.filter(
        #     Q(contact__name=value) | Q(contact__id=value.id)
        # )
    def filter_by_payee_type(self, queryset, name, value):
        ct = ContentType.objects.get(model=value)
        return queryset.filter(payee_type_id=ct.id)

    def filter_by_payee(self, queryset, name, value):
        payee_type = self.request.GET.get('payee_type', False)
        if payee_type:
            ct = ContentType.objects.get(model=payee_type)
            payed_to = apps.get_model(ct.app_label, payee_type)
            payee_ids = payed_to.objects.filter(name__icontains=value).values_list('id')
            return queryset.filter(payee_id__in=payee_ids)

        return queryset

    def filter_by_order_type(self, queryset, name, value):
        ct = ContentType.objects.get(model=value)
        return queryset.filter(order_type_id=ct.id)

    def filter_by_order(self, queryset, name, value):
        payee_type = self.request.GET.get('payee_type', False)
        if payee_type:
            ct = ContentType.objects.get(model=payee_type)
            order = apps.get_model(ct.app_label, payee_type)
            order_ids = order.objects.filter(code__icontains=value).values_list('id')
            return queryset.filter(order_id__in=order_ids)

        return queryset

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
