from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.contrib import messages
from django.utils import timezone
from django.db import transaction
from erp_core.pagination import paginate
from .filters import BillFilter
from .forms import BillForm, CodeSettingForm, PayeeForm, PreBillForm
from .models import Bill, CodeSetting, BillPaymentTerm, PAYEE_TYPES
# from supplier.models import SupplierContact
from decimal import Decimal
from django.forms import modelform_factory
from erp_core.models import PaymentTerm

# Create your views here.


def index(request, payee_type=None, order_type=None):
    f = BillFilter(request.GET, request=request)
    bills, pagination = paginate(request, queryset=f.qs, per_page=5, default=True)
    context = {
        'bills': bills,
        'payee_types': PAYEE_TYPES,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, "bills/list.html", context)


@transaction.atomic
def pre_create(request):

    form = PreBillForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            order_type = form.cleaned_data['order_type']
            payee_type = form.cleaned_data['payee_type']
            return redirect(reverse("expense:bill:create", args=[payee_type, order_type]))

    context = {
        'form': form,
        'form_url': reverse('expense:bill:pre-create'),
    }
    return render(request, "bill/pre_create.html", context)


@transaction.atomic
def create(request, payee_type, order_type):

    payee_form = PayeeForm(request.POST or None, payee_type=payee_type)
    # order_form = OrderForm(request.POST or None, order_type=order_type)
    bill_form = BillForm(request.POST or None, request.FILES or None)
    payment_terms = PaymentTerm.objects.all()

    if request.method == 'POST':
        if bill_form.is_valid() and payee_form.is_valid():

            bill = bill_form.save(commit=False)
            bill.payee = payee_form.cleaned_data['payee']
            bill.order = order_form.cleaned_data['order']
            bill.save()
            bill.add_payment_terms(request.POST.getlist('payment', []))

            return redirect(reverse("expense:bill:detail", args=[bill.id]))

    context = {
        'bill_form': bill_form,
        'payee_form': payee_form,
        # 'order_form': order_form,
        'payment_terms': payment_terms,
        # 'invoice_shipping_address_form': invoice_shipping_address_form,
        'form_url': reverse('expense:bill:create', args=[payee_type, order_type]),
    }
    return render(request, "bill/edit.html", context)


def detail(request, bill_pk):
    bill = Bill.objects.get(pk=bill_pk)
    context = {
        'bill': bill,
    }
    return render(request, "bill/detail.html", context)


@transaction.atomic
def edit(request, bill_pk):
    bill = get_object_or_404(Bill, pk=bill_pk)
    if bill.status == bill.PAID:
        messages.add_message(request, messages.WARNING, 'Editing is not possible since the bill is already paid')
        return redirect(reverse('expense:bill:detail', args=[bill_pk]))

    Form = modelform_factory(Bill, form=BillForm, exclude=['code'])
    bill_form = Form(request.POST or None, request.FILES or None, instance=bill)

    payment_terms = PaymentTerm.objects.all()

    if request.method == 'POST':
        if bill_form.is_valid():

            bill = bill_form.save(commit=False)
            bill.save()
            bill.add_payment_terms(request.POST.getlist('payment', False))
            return redirect(reverse("expense:bill:detail", args=[bill_pk]))
    context = {
        'bill_form': bill_form,
        'bill': bill,
        'payment_terms': payment_terms,
        # 'invoice_shipping_address_form': invoice_shipping_address_form,
        'form_url': reverse('expense:bill:edit', args=[bill_pk]),
        'edit': 'edit',
    }
    return render(request, "bill/edit.html", context)


@transaction.atomic
def code_settings(request):

    if CodeSetting.objects.exists():
        code_form = CodeSettingForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingForm(request.POST or None)
    if request.method == "POST":
        if code_form.is_valid():
            code_settings = code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return redirect(reverse('expense:bill:list'))
    context = {
        'code_form': code_form,
    }

    return render(request, "bill/code_setting.html", context)


@transaction.atomic
def delete(request, bill_pk):
    bill = get_object_or_404(Bill, pk=bill_pk)

    if bill.status == bill.PAID:
        messages.add_message(request, messages.WARNING, 'Deletion is not possible')
        return redirect(reverse('expense:bill:detail', args=[bill_pk]))

    bill.delete()
    return redirect(reverse('expense:bill:list'))


@transaction.atomic
def add_payment_terms(request, bill_pk):
    bill = get_object_or_404(Bill, pk=bill_pk)
    payment_term_id_list = [pt.id for pt in bill.payment_terms.all()]
    payment_terms = PaymentTerm.objects.exclude(id__in=payment_term_id_list).all()

    if request.method == 'POST':
        bill.add_payment_terms(request.POST.getlist('payment', False))
        return redirect(reverse('expense:bill:detail', args=[bill_pk]))

    context = {
        'payment_terms': payment_terms,
        'bill': bill,
        'form_url': reverse('expense:bill:add-payment-terms', args=[bill_pk]),
    }
    return render(request, "bill/add_payment_terms.html", context)


@transaction.atomic
def delete_payment_term(request, bill_payment_term_pk):

    bill_payment_term = get_object_or_404(BillPaymentTerm, pk=bill_payment_term_pk)

    if bill_payment_term.status == BillPaymentTerm.NOT_PAID:
        bill_payment_term.delete()
    else:
        messages.add_message(request, messages.WARNING, 'Deletion is not Possible')
    return redirect(reverse('expense:bill:detail', args=[bill_payment_term.bill.pk]))
