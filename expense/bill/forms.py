from django.contrib.contenttypes.models import ContentType
from django.forms import BaseInlineFormSet, ValidationError
from django.apps import apps
from django import forms

from .models import Bill, CodeSetting, BillReceipt, ORDER_TYPES, PAYEE_TYPES
from supplier.models import Supplier
# from raw_material.purchase_order.models import PurchaseOrder

from django.forms import ModelChoiceField


class PreBillForm(forms.Form):

    order_type = forms.ChoiceField(
        choices=ORDER_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    payee_type = forms.ChoiceField(
        choices=PAYEE_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )


class BillForm(forms.ModelForm):

    class Meta:
        model = Bill
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control", 'id': "code"}),
            # "order_number": forms.TextInput(attrs={'class': "form-control", 'id': "order_number"}),
            "tax": forms.NumberInput(attrs={'class': "form-control", 'id': "tax", 'required': "required"}),
            "tax_inclusive": forms.CheckboxInput(attrs={'id': "tax_inclusive"}),
            "amount": forms.NumberInput(
                attrs={'class': "form-control", 'id': "amount", 'required': "required"}),
            "created_by": forms.Select(
                attrs={'class': "form-control", 'id': "sales_person", 'required': "required"}),
            # "payment_terms": forms.SelectMultiple(attrs={'class': "form-control", "data-toggle": "select", "id": "payment_terms",  'multiple': "multiple",'required': "required"}),
            "billed_date": forms.DateInput(
                attrs={'class': "form-control", 'required': "required", 'type': "text", 'id': "billed_date"}),
            "due_date": forms.DateInput(
                attrs={'class': "form-control", 'required': "required", 'type': "text", 'id': "due_date"}),
        }
        fields = ['code', 'tax', 'amount', 'due_date', 'billed_date', 'tax_inclusive']


class PayeeForm(forms.Form):
    payee = forms.ModelChoiceField(
        queryset=Supplier.objects.none(),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    def __init__(self, *args, **kwargs):
        payee_type = kwargs.pop('payee_type')
        z = ContentType.objects.get(model=payee_type)
        PayedTo = apps.get_model(z.app_label, payee_type)
        queryset = PayedTo.objects.all()
        super(PayeeForm, self).__init__(*args, **kwargs)
        self.fields['payee'].queryset = queryset
    

# class OrderForm(forms.Form):
#     order = forms.ModelChoiceField(
#         queryset=PurchaseOrder.objects.none(),
#         # required=True,
#         widget=forms.Select(attrs={'class': 'form-control'}),
#     )
#
#     def __init__(self, *args, **kwargs):
#         order_type = kwargs.pop('order_type')
#         # z = ContentType.objects.get(model=order_type)
#         OrderClass = apps.get_model(order_type)
#         queryset = OrderClass.objects.all()
#         super(OrderForm, self).__init__(*args, **kwargs)
#         self.fields['order'].queryset = queryset


class CodeSettingForm(forms.ModelForm):

    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(
                attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(
                attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(
                attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


class BillReceiptForm(forms.ModelForm):

    class Meta:
        model = BillReceipt
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control", 'id': "code"}),
            "amount": forms.NumberInput(
                attrs={'class': "form-control", 'id': "amount", 'required': "required"}),
            "received_date": forms.DateInput(
                attrs={'class': "form-control received_date", 'type': "text"}),
        }
        fields = ['amount', 'document', 'code', 'received_date', 'code']


class CustomInlineFormset(BaseInlineFormSet):
    def clean(self):
        super(CustomInlineFormset, self).clean()
        # example custom validation across forms in the formset
        # amount = 0
        # for form in self.forms:
        #     amount += form.cleaned_data['amount']
        #     if amount >  self.instance.amount:
        #         raise ValidationError("The sum of all the receipts is greater than payment term amount")

        # your custom formset validation
