from django.shortcuts import render, reverse, redirect
from django.db import transaction
from django.forms import inlineformset_factory
from erp_core.pagination import paginate
from .filters import BillFilter
from .forms import BillReceiptForm, CustomInlineFormset
from .models import Bill, CodeSetting, BillPaymentTerm, BillReceipt
from supplier.models import Supplier
from django.shortcuts import get_object_or_404
from django.contrib import messages

import os
from django.conf import settings
from django.http import HttpResponse, Http404
# Create your views here.


def index(request, bill_payment_term_pk):
    bill_payment_term = get_object_or_404(BillPaymentTerm, id=bill_payment_term_pk)
    receipts = bill_payment_term.receipts.all()

    context = {
        'receipts': receipts,
        'bill_payment_term': bill_payment_term,
        'bill': bill_payment_term.bill,
    }

    return render(request, "bill/receipt/list.html", context)


@transaction.atomic
def create(request, bill_payment_term_pk):
    bill_payment_term = get_object_or_404(BillPaymentTerm, id=bill_payment_term_pk)
    receipt_form = BillReceiptForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':

        if receipt_form.is_valid():
            receipt = receipt_form.save(commit=False)
            receipt.created_by = request.user
            receipt.payment_term = bill_payment_term
            receipt.save()

            return redirect(reverse('expense:bill:payment_term:receipt:list', args=[bill_payment_term.id]))

    context = {
        'bill': bill_payment_term.bill,
        'bill_payment_term': bill_payment_term,
        'receipt_form': receipt_form,
        'form_url': reverse("expense:bill:payment_term:receipt:create", args=[bill_payment_term_pk])
    }
    return render(request, "bill/receipt/edit.html", context)


@transaction.atomic
def edit(request, bill_payment_term_pk, receipt_pk):
    receipt = get_object_or_404(BillReceipt, id=receipt_pk)
    receipt_form = BillReceiptForm(request.POST or None, request.FILES or None, instance=receipt)

    if request.method == 'POST':
        if receipt_form.is_valid():
            receipt_form.save()

            return redirect(reverse('expense:bill:payment_term:receipt:list', args=[bill_payment_term_pk]))

    context = {
        'bill': receipt.payment_term.bill,
        'bill_payment_term': receipt.payment_term,
        'receipt_form': receipt_form,
        'form_url': reverse("expense:bill:payment_term:receipt:edit", args=[bill_payment_term_pk, receipt_pk])
    }
    return render(request, "bill/receipt/edit.html", context)


def download(request, receipt_pk):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="image/jpeg")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


@transaction.atomic
def delete(request, bill_payment_term_pk, receipt_pk):
    receipt = get_object_or_404(BillReceipt, id=receipt_pk)
    bill_payment_term = receipt.payment_term
    if request.method == "POST":
        receipt.delete()
        bill_payment_term.set_balance()
        return redirect(reverse('expense:bill:payment_term:receipt:list', args=[bill_payment_term_pk]))
    context = {
        'receipt': receipt,
        'form_url': reverse('expense:bill:payment_term:receipt:delete', args=[bill_payment_term_pk, receipt_pk])
    }
    return render(request, "bill/receipt/delete.html", context)
