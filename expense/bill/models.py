from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.contrib.auth.models import User
from erp_core.models import (
    ErpAbstractBaseModel, BaseCodeSetting, PaymentType, PaymentTerm
)

import uuid

ORDER_TYPES = (
    ('raw_material_purchase_order.purchaseorder', 'Raw Material Purchase Order'),
    ('inventory.inventorypurchaseorder', 'Product Purchase Order'),
    ('finished_goods', 'Finished Goods Purchase Order'),
)

PAYEE_TYPES = (
    ('supplier', 'Supplier'),
    ('vendor', 'Vendor'),
)

class Bill(ErpAbstractBaseModel):
    PAID = 'paid'
    PARTIALLY_PAID = 'partially paid'
    NOT_PAID = 'not paid'
    STATUSES = (
        (PAID, 'Paid'),
        (PARTIALLY_PAID, 'Partially Paid'),
        (NOT_PAID, 'Not Paid'),
    )

    def newCode():
        return CodeSetting.new_code()

    code = models.CharField(max_length=100, unique=True, default=newCode)

    status = models.CharField(max_length=30, choices=STATUSES, default=NOT_PAID)

    tax_inclusive = models.BooleanField(default=False)
    tax = models.IntegerField(default=0)

    amount = models.DecimalField(max_digits=20, decimal_places=2)
    balance = models.DecimalField(max_digits=20, decimal_places=2,default=0)

    billed_date = models.DateField(default=timezone.now)
    due_date = models.DateField(null=True, blank=True)
    paid_date = models.DateField(null=True, blank=True)

    # payee
    payee_type = models.ForeignKey(ContentType, related_name="payee_bills")
    payee_id = models.UUIDField()

    def get_payee(self):
        return self.payee_type.get_object_for_this_type(pk=self.payee_id)

    def set_payee(self, payee):
        self.payee_type = ContentType.objects.get_for_model(type(payee))
        self.payee_id = payee.pk

    payee = property(get_payee, set_payee)

    # order
    order_type = models.ForeignKey(ContentType, related_name="order_bills")
    order_id = models.UUIDField()

    def get_order(self):
        return self.order_type.get_object_for_this_type(pk=self.order_id)

    def set_order(self, order):
        self.order_type = ContentType.objects.get_for_model(type(order))
        self.order_id = order.pk
        self.amount = order.total_amount

    order = property(get_order, set_order)

    def __str__(self):
        return str(self.code) + "-" + str(self.payee)

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()
            self.set_balance()

        super(Bill, self).save(*args, **kwargs)

    def get_amount(self):
        if self.tax_inclusive is False and self.tax > 0:
            return self.amount + self.tax
        return self.amount

    def can_make_payment(self):
        return self.status is not self.PAID

    def set_balance(self):
        # print("bill: ", self.amount_paid)
        self.balance = self.get_amount() - self.amount_paid
        self.set_status()
        return self

    @property
    def amount_paid(self):
        if self.payment_terms.exists():
            return self.payment_terms.aggregate(total_amount=models.Sum('amount'))['total_amount'] - self.payment_terms.aggregate(balance_sum=models.Sum('balance'))['balance_sum']
        return 0

    def set_status(self):
        if self.balance < self.get_amount():
            self.status = self.PARTIALLY_PAID
        if self.balance <= 0:
            self.status = self.PAID
            self.paid_date = timezone.now().date()

    def add_payment_terms(self, payment_terms):
        for payment in payment_terms:
            BillPaymentTerm.objects.update_or_create(payment_term_id=payment, bill=self)


class BillPaymentTerm(ErpAbstractBaseModel):
    PAID = 'paid'
    PARTIALLY_PAID = 'partially paid'
    NOT_PAID = 'not paid'
    STATUSES = (
        (PAID, 'Paid'),
        (PARTIALLY_PAID, 'Partially Paid'),
        (NOT_PAID, 'Not Paid'),
    )
    bill = models.ForeignKey(Bill, related_name="payment_terms")
    payment_term = models.ForeignKey(PaymentTerm, related_name="bills")
    date = models.DateField(default=timezone.now)
    status = models.CharField(max_length=20, choices=STATUSES, default=NOT_PAID)
    amount = models.DecimalField(max_digits=20, decimal_places=2,default=0)
    balance = models.DecimalField(max_digits=20, decimal_places=2,default=0)

    def save(self, *args, **kwargs):
        if self.created_at is None:
            self.amount = self.payment_term_amount()
            self.balance = self.amount
        super(BillPaymentTerm, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.bill) + "- " + str(self.payment_term)

    def isNotPaid(self):
        return self.status != self.PAID

    def totalAmountPaid(self):
        return self.amount - self.balance

    def payment_term_amount(self):
        return (self.bill.get_amount() * self.payment_term.percentage_value) / 100

    def set_balance(self):
        # update balance
        self.balance = self.amount - self.amount_paid
        if self.balance < 0:
            self.balance = 0
        self.set_status()
        self.save()

        # update bill fields
        self.bill.set_balance().save()

        return self

    @property
    def amount_paid(self):
        return self.receipts.aggregate(paid_amount=models.Sum('amount'))['paid_amount']

    def set_status(self):
        if self.balance <= 0:
            self.status = self.PAID
        else:
            self.status = self.PARTIALLY_PAID
        return self


class CodeSetting(BaseCodeSetting):
    pass


class BillReceipt(ErpAbstractBaseModel):
    code = models.CharField(max_length=255)
    payment_term = models.ForeignKey(BillPaymentTerm, related_name="receipts")
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    received_date = models.DateField(default=timezone.now)
    document = models.FileField(upload_to='bill/receipts/%Y/%m/%d/', null=True, blank=True)
    created_by = models.ForeignKey(User, related_name="%(class)ss")

    def __str__(self):
        return str(self.payment_term.bill) + "-" + str(self.payment_term) + "-" + str(self.code)

    def save(self, *args, **kwargs):
        super(BillReceipt, self).save(*args, **kwargs)

        # update payterm fields
        self.payment_term.set_balance()
