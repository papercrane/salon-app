from django import forms
from django.forms import ModelForm
from .models import Expense
# from .models import Enquiry
# from .models import Activity
# from tax.models import Tax
from expense.bill.models import ORDER_TYPES, PAYEE_TYPES
from django.apps import apps
from django.contrib.contenttypes.models import ContentType
from supplier.models import Supplier


# class SalesReimbursementForm(ModelForm):
#     class Meta:
#         model = SalesReimbursement
#
#         widgets = {
#
#             "photo": forms.FileInput(attrs={'class': "form-control1", 'required': "required"}),
#             "date_and_time": forms.TextInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
#             "reimbursement_type": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),
#             "enquiry": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             "activity": forms.Select(attrs={'id': "select2", 'class': 'form-control1', "multiple": False}),
#             "title": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
#
#
#         }
#         fields = ['photo', 'date_and_time', 'reimbursement_type',
#                   'amount', 'enquiry', 'activity', 'title']
#
#     def __init__(self, *args, **kwargs):
#         current_user = kwargs.pop('current_user')
#         super(SalesReimbursementForm, self).__init__(*args, **kwargs)
#         self.fields['enquiry'].queryset = Enquiry.objects.filter(
#             employee=current_user)
#
#         self.fields['activity'].queryset = Activity.objects.filter(
#             employee=current_user)
#

class ExpenseForm(ModelForm):
    # tax = forms.ModelMultipleChoiceField(
    #     required=False,
    #     queryset=Tax.objects.all(),
    #     widget=forms.SelectMultiple(attrs={'id': "tax", 'class': 'form-control1'}),
    # )

    class Meta:
        model = Expense

        widgets = {

            "attachment": forms.FileInput(attrs={'class': "form-control1"}),
            "date": forms.TextInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
            "expense_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required", 'min': 0}),
            "paid": forms.CheckboxInput(attrs={}),
            # "vendor": forms.Select(attrs={'id': "vendor", 'class': 'form-control1', "multiple": False}),
            "customer": forms.Select(attrs={'id': "customer", 'class': 'form-control1', "multiple": False}),
            "note": forms.Textarea(attrs={'class': "form-control", 'id': "note", 'rows': 3}),
            "reference": forms.TextInput(attrs={'class': "form-control", 'id': "refernce"}),

        }
        fields = ['attachment', 'date', 'expense_type',
                  'amount', 'paid', 'customer', 'note', 'reference']


class ExpenseFormsetForm(ModelForm):
    class Meta:
        model = Expense

        widgets = {

            "date": forms.TextInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
            "expense_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required", 'min': 0}),
            "paid": forms.CheckboxInput(attrs={}),
            # "vendor": forms.Select(attrs={'id': "vendor", 'class': 'form-control1', "multiple": False}),
            "note": forms.Textarea(attrs={'class': "form-control", 'id': "note", 'rows': 1}),

        }
        fields = ['date', 'expense_type',
                  'amount', 'paid', 'note']


class ExpenserForm(forms.Form):

    payee_type = forms.ChoiceField(
        choices=PAYEE_TYPES,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )


class PayeeForm(forms.Form):
    payee = forms.ModelChoiceField(
        queryset=Supplier.objects.none(),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    def __init__(self, *args, **kwargs):
        payee_type = kwargs.pop('payee_type')
        z = ContentType.objects.get(model=payee_type)
        PayedTo = apps.get_model(z.app_label, payee_type)
        queryset = PayedTo.objects.all()
        super(PayeeForm, self).__init__(*args, **kwargs)
        self.fields['payee'].queryset = queryset
        if payee_type == 'supplier':
            self.fields['payee'].label = 'Supplier'
        else:
            self.fields['payee'].label = 'Vendor'
