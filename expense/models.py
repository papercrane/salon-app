from django.db import models
from erp_core.models import ErpAbstractBaseModel
from employee.models import Employee
from vendor.models import Vendor
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MinValueValidator
# Create your models here.
import uuid
from location.models import Location


PAYEE_TYPES = (
    ('employee', 'Employee'),
    ('vehicleasset', 'VehicleAsset'),
)

class Expense(ErpAbstractBaseModel):

    FOOD = "Food"
    DIESEL = "Diesel"
    PETROL = "Petrol"
    STAFF_ADVANCE = "Staff Advance"
    TRAVELING = "Traveling"
    SELL_VEHICLE = "Sell Vehicle"

    TYPES = (
        (FOOD, "Food"),
        (DIESEL, "Diesel"),
        (PETROL, "Petrol"),
        (STAFF_ADVANCE, "Staff Advance"),
        (TRAVELING, "Traveling"),
        (SELL_VEHICLE, "Sell Vehicle"),

    )
    expense_type = models.ForeignKey('TypeofExpense')
    name = models.CharField(max_length=255, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
    income_or_expense = models.BooleanField(default=True)
    date = models.DateField(null=True, blank=True)

    # payee
    payee_type = models.ForeignKey(ContentType, related_name="employee_or_vehicle")
    payee_id = models.UUIDField()

    paid = models.BooleanField(default=True)
    reference = models.CharField(max_length=255, null=True, blank=True)
    attachment = models.FileField(upload_to='media/expense/', null=True, blank=True)
    note = models.CharField(max_length=255, null=True, blank=True)
    customer = models.ForeignKey('customer.Customer', null=True, blank=True)
    # tax = models.ManyToManyField('tax.Tax', related_name='expense_tax')

    def get_payee(self):
        return self.payee_type.get_object_for_this_type(pk=self.payee_id)

    def set_payee(self, payee):
        self.payee_type = ContentType.objects.get_for_model(type(payee))
        self.payee_id = payee.pk

    payee = property(get_payee, set_payee)

    def __str__(self):
        return str(self.expense_type) + "-" + str(self.amount)

    class Meta:
        permissions = (
            ("view_expense_list", "Can view expense list"),
        )


class TypeOfExpense(ErpAbstractBaseModel):

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name