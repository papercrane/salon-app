from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Expense',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="clipboard",

            children=[
                item(
                    'Record Expense',
                    'expense:supplier_or_vendor',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="calendar-check-o",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Add Bulk',
                    'expense:bulk_supplier_or_vendor',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="handshake-o",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'View Expenses',
                    'expense:index',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="user",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Bill',
                    'bill:list',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="product-hunt",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
            ]

        )
    ]),

)
