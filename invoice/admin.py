from django.contrib import admin
from invoice.models import Invoice, InvoiceCodeSetting, InvoiceMailSetting, Achievements, InvoicePaymentTerms, \
    InvoicePaymentReceipt, InvoiceBillingAddress, InvoiceShippingAddress, InvoiceItem, CustomerPurchasedPackage


class InvoiceItemAdmin(admin.ModelAdmin):
    model = InvoiceItem
    list_display=('total_price','content_object')


class InvoiceAdmin(admin.ModelAdmin):
    model = Invoice
    list_display=('customer','amount','tax_amount')



admin.site.register(InvoiceCodeSetting)
admin.site.register(InvoiceMailSetting)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(InvoicePaymentTerms)
admin.site.register(Achievements)
admin.site.register(InvoiceBillingAddress)
admin.site.register(InvoiceShippingAddress)
admin.site.register(InvoiceItem,InvoiceItemAdmin)
admin.site.register(InvoicePaymentReceipt)
admin.site.register(CustomerPurchasedPackage)