from datetime import datetime, date
from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.shortcuts import get_object_or_404, render, redirect

from calendarApp.models import Appointment
from calendarApp.views import send_sms
from discount.models import Discount
from employee.models import Employee
from erp_core.models import PaymentTerm
from invoice.models import InvoicePaymentTerms, InvoicePaymentReceipt, InvoiceMailSetting, CustomerPurchasedPackage
from invoice.forms import InvoiceMailSettingsForm, InvoiceCodeSetting, InvoiceCodeSettingsForm, PaymentMethodForm, \
    InvoiceProductForm
from django.forms import inlineformset_factory
from django.core.mail import EmailMultiAlternatives
from easy_pdf.rendering import render_to_pdf
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
from erp_core.pagination import paginate
from invoice.filters import InvoiceFilter, LocationFilter
from location.models import Location, Office
from location.forms import LocationForm
from invoice.forms import InvoiceServiceForm, InvoiceForm
from invoice.models import Invoice, InvoiceItem
from service.models import Service
from product.models import Product
from datetime import datetime
from customer.forms import ContactAddressForm, CustomerForm
from customer.models import Customer
from django.db.models import Sum, Count
from django.contrib.auth.decorators import login_required, permission_required
from easy_pdf.views import PDFTemplateView


# listing all invoices

@login_required
@permission_required('invoice.view_invoice_list', raise_exception=True)
def list_view(request):
    if request.user.is_staff:
        f = InvoiceFilter(request.GET, queryset=Invoice.objects.order_by('-created_at'))
    else:
        f = InvoiceFilter(request.GET,
                          queryset=Invoice.objects.filter(supervisor=request.user.employee).order_by('-created_at'))
    invoices, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    context = {
        'invoices': invoices,
        'pagination': pagination,
        'filter': f,

    }
    return render(request, "invoice/list.html", context)


# if invoice_check.enquiry == True and invoice_check.quotation == False:
#     # enquiry = Enquiry.objects.all()
#     # if request.method == 'POST':
#     #     enquiry_pk = request.POST.get('enquiry', False)
#     #     if Invoice.objects.filter(quotation__enquiry__pk=enquiry_pk).exists():
#     #         messages.add_message(request, messages.INFO, 'Invoice created before')
#     #     else:
#     #         return HttpResponseRedirect(reverse('invoice:edit', args=[enquiry_pk]))
#     # context = {
#     #     'enquiries': enquiry,

#     # }
#     # return render(request, "invoice/invoice_enquiry.html", context)
#     pass
# elif invoice_check.quotation is True:
#     # quotation = Quotation.objects.filter(active=True, status="accepted")
#     # if request.method == 'POST':
#     #     quotation_pk = request.POST.get('quotation', False)
#     #     if Invoice.objects.filter(quotation__pk=quotation_pk).exists():
#     #         messages.add_message(request, messages.INFO, 'Invoice created before')
#     #     else:
#     #         return HttpResponseRedirect(reverse('invoice:edit', args=[quotation_pk]))
#     # context = {
#     #     'quotations': quotation,
#     # }
#     # return render(request, "invoice/invoice_quotation.html", context)
#     pass
# else:
#     # invoice_check.invoice == True and invoice_check.enquiry == False and invoice_check.quotation == False:
# return HttpResponseRedirect(reverse('invoice:create'))


def check_for_designer_package(selected_item, invoice):
    if selected_item.name == "Designer Sinora Package":
        invoice.customer.designer_sinora_package_date = timezone.now().date() + timezone.timedelta(days=45)
        invoice.customer.save()
    return True


@login_required
@permission_required('invoice.change_invoice', raise_exception=True)
@transaction.atomic
def createinvoice(request, pk=None):
    items_array = []
    product_items_array = []
    item_count = 0
    week_set = ['Sunday', 'Monday', 'Tuesday']

    if pk:
        customer = Customer.objects.get(pk=pk)
        invoice_form = InvoiceForm(initial={'customer': customer})

    else:
        invoice_form = InvoiceForm(request.POST or None)

    # ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm, extra=1,
    #                                              can_delete=True)
    # product_formset = ProductInlineFormSet(request.POST or None)

    if request.GET.get('appointment_pk') and request.method == "GET":
        appointment = get_object_or_404(Appointment, pk=request.GET.get('appointment_pk'))

        content_type = ContentType.objects.get_for_model(Service)

        invoice_form = InvoiceForm(initial={'customer': appointment.customer})
        items = [{'service_prdt': appointment.service.pk, 'content_type': content_type,
                  'unit_price': appointment.service.price_hour, 'employee': appointment.employee} for appointment in
                 appointment.services.all()]

        ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
                                                     extra=0, can_delete=True, min_num=len(items), validate_min=True)
        service_formset = ServiceInlineFormSet(initial=items, prefix='service_item')
    else:
        ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
                                                     extra=1, can_delete=True)
        service_formset = ServiceInlineFormSet(request.POST or None, prefix='service_item')

    if request.method == "POST":
        if invoice_form.is_valid() and service_formset.is_valid():
            invoice = invoice_form.save(commit=False)
            invoice.customer = get_object_or_404(Customer, pk=request.POST.get('customer'))
            invoice.supervisor = get_object_or_404(Employee, pk=request.POST.get('supervisor'))
            invoice.save()
            if request.POST.get('appointment_pk') and request.POST.get('appointment_pk') != 'False':
                appointment = get_object_or_404(Appointment, pk=request.POST.get('appointment_pk'))

                appointment.status = Appointment.CLOSED
                appointment.save()
            for item_form in service_formset:
                try:
                    items_array.append(item_form.cleaned_data['service_prdt'])
                except KeyError as error:
                    pass

            services = service_formset.save(commit=False)

            for service in services:
                service.invoice = invoice
                selected_item = get_object_or_404(Service, pk=items_array[item_count])
                service.content_object = selected_item
                service.object_id = selected_item.pk
                item_count += 1
                service.setTotalPrice()
                service.save()
                invoice.invoiceTotal().save()

            # item_count = 0

            # for item_form in product_formset:
            #     try:
            #         product_items_array.append(item_form.cleaned_data['prdt'])
            #     except KeyError as error:
            #         pass
            # products = product_formset.save(commit=False)
            # for product in products:
            #     product.invoice = invoice
            #     selected_item = get_object_or_404(Product, pk=product_items_array[item_count])
            #     product.content_object = selected_item
            #     product.object_id = selected_item.pk
            #     item_count += 1
            #     product.setTotalPrice()
            #     product.save()
            #     invoice.invoiceTotal().save()

            if invoice.discount:
                discount = invoice.discount
                if discount.active == False or discount.offer_count == 0 or invoice.amount < discount.minimum_amount:
                    invoice.discount = None
                    messages.add_message(request, messages.INFO,
                                         'Invoice created successfully ,But this offer is not available now')

                else:
                    invoice.discount_percentage = discount.percentage
                    including_tax = invoice.amount + (invoice.amount * Decimal(.05))

                    invoice.discount_amount = including_tax * (discount.percentage / 100)
                    invoice.save()

                    discount.offer_count -= 1
                    discount.save()
                    messages.add_message(request, messages.INFO, 'Invoice created successfully')

            invoice.calcultate_grandtotal_and_tax().save()
            message = "Hi " + str(
                invoice.customer) + ",Thank you for choosing Sinora Beauty Salon. Billable amount - AED" + str(
                round(invoice.amount, 2)) + " VAT - AED" + str(
                round(invoice.tax_amount, 2)) + "  Total Amount - AED" + str(
                round(invoice.grand_total, 2))

            send_sms(request, message=message, number=invoice.customer.phone)

            return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

            # if discount.name == "Buddy Deal":
            #     if invoice.customer.buddy_deal == False:
            #         invoice.customer.buddy_deal = True
            #         invoice.customer.save()
            # elif discount.name == "Designer Sinora Package":
            #     if invoice.items.count() >= discount.offer_count and invoice.amount >= discount.minimum_amount:
            #         if not invoice.customer.designer_sinora_package_date:
            #             invoice.customer.designer_sinora_package_date = timezone.now().date()
            #             invoice.customer.save()
            #         else:
            #             difference = timezone.now().date() - invoice.customer.designer_sinora_package_date
            #             if difference.days >= 45:
            #                 invoice.customer.designer_sinora_package_date = timezone.now().date()
            #                 invoice.customer.save()
            #
            # else:
            #     if invoice.amount >= discount.minimum_amount:
            #         if datetime.now().date().strftime('%A') not in week_set:
            #             if not (datetime.now().time().hour >= 9 and datetime.now().time().hour <= 19):
            #                 invoice.discount = None
            #                 invoice.calcultate_grandtotal_and_tax().save()
            #                 invoice.save()
            #                 messages.add_message(request, messages.INFO, 'This offer is not available now')
            #                 return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.pk]))

            #     invoice.discount_percentage = discount.percentage
            #     including_tax = invoice.amount + (invoice.amount * Decimal(.05))
            #
            #     invoice.discount_amount = including_tax * (discount.percentage / 100)
            #     invoice.save()
            #
            # invoice.calcultate_grandtotal_and_tax().save()
            #
            # return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

    context = {
        # 'product_formset': product_formset,
        'form_url': reverse_lazy('invoice:createinvoice'),
        'type': "Create",
        'invoice_form': invoice_form,
        'products': Product.objects.all(),
        'services': Service.objects.all(),
        'service_formset': service_formset,
        'appointment_pk': request.GET.get('appointment_pk', False)

    }
    return render(request, "invoice/createinvoic.html", context)


@transaction.atomic
def createcustomer(request):
    customer_form = CustomerForm(request.POST or None)
    customer_address_form = ContactAddressForm(request.POST or None)
    appointment = request.GET.get('create')

    if request.method == "POST":
        # app_qs = request.GET.get('app')
        if customer_form.is_valid() and customer_address_form.is_valid():
            customer = customer_form.save()
            address = customer_address_form.save(commit=False)
            address.customer = customer
            address.save()
            if appointment == 'appointment':
                custom_url = reverse('calendarapp:create') + '?customer=' + str(customer.pk)
                messages.add_message(request, messages.SUCCESS, "Customer created successfully")
                return HttpResponseRedirect(custom_url)
            else:
                messages.add_message(request, messages.SUCCESS, "Customer created successfully")
                return HttpResponseRedirect(reverse('invoice:createinvoice', args=[customer.pk]))

    context = {
        'customer_form': customer_form,
        'customer_address_form': customer_address_form,
        'form_url': reverse_lazy('invoice:createcustomer'),
        'invoicecustomer': 'yes',
        'appointment': appointment,
        'create': "Create"

    }
    return render(request, "customer/edit.html", context)


@login_required
@permission_required('invoice.change_invoice', raise_exception=True)
@transaction.atomic
def edit(request, pk):
    items_array = []
    items_type = []
    itemlist = []
    item_count = 0

    service_itemlist = []
    service_items_array = []
    service_items_type = []
    service_item_count = 0

    week_set = ['Sunday', 'Monday', 'Tuesday']

    invoice = Invoice.objects.get(pk=pk)

    for item in invoice.items.filter(content_type__app_label="product"):
        item_data = {'unit_price': item.unit_price,
                     'hours_or_sqfeet': item.hours_or_sqfeet, 'total_price': item.total_price, 'prdt': item.object_id}
        itemlist.append(item_data)
    prod_length = len(itemlist)

    for item in invoice.items.filter(content_type__app_label="service"):
        service_item_data = {'unit_price': item.unit_price,
                             'hours_or_sqfeet': item.hours_or_sqfeet, 'total_price': item.total_price,
                             'service_prdt': item.object_id, 'employee': item.employee}
        service_itemlist.append(service_item_data)

    service_length = len(service_itemlist)

    invoice_form = InvoiceForm(request.POST or None, instance=invoice)

    if prod_length == 0:
        ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm,
                                                     extra=1, can_delete=True)
    else:

        ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm,
                                                     extra=0, can_delete=True, min_num=prod_length, validate_min=True)

    product_formset = ProductInlineFormSet(request.POST or None, initial=itemlist)

    if service_length == 0:
        ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
                                                     extra=1, can_delete=True)

    else:

        ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
                                                     extra=0, can_delete=True, min_num=service_length,
                                                     validate_min=True)

    # ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
    #                                              extra=0, can_delete=True, min_num=service_length, validate_min=True)

    service_formset = ServiceInlineFormSet(request.POST or None, initial=service_itemlist, prefix="service_item")

    if request.method == "POST":
        print(product_formset.errors)
        print(service_formset.errors)

        if product_formset.is_valid() and invoice_form.is_valid() and service_formset.is_valid():
            invoice = invoice_form.save()
            invoice.items.all().delete()

            # for item_form in product_formset:
            #     items_array.append(item_form.cleaned_data['prdt'])

            for item_form in product_formset:
                try:
                    items_array.append(item_form.cleaned_data['prdt'])
                except KeyError as error:
                    pass

            for item_form in service_formset:
                try:
                    service_items_array.append(item_form.cleaned_data['service_prdt'])
                except KeyError as error:
                    pass

            # for item_form in service_formset:
            #     service_items_array.append(item_form.cleaned_data['service_prdt'])

            products = product_formset.save(commit=False)
            for product in products:
                product.invoice = invoice
                selected_item = get_object_or_404(Product, pk=items_array[item_count])
                product.content_object = selected_item
                product.object_id = selected_item.pk
                item_count += 1
                product.invoice = invoice
                product.setTotalPrice()
                product.save()
                invoice.invoiceTotal().save()

            services = service_formset.save(commit=False)
            for service in services:
                selected_item = get_object_or_404(Service, pk=service_items_array[service_item_count])
                service.content_object = selected_item
                service.object_id = selected_item.pk
                service_item_count += 1
                service.invoice = invoice
                service.setTotalPrice()
                service.save()
                invoice.invoiceTotal().save()
            if invoice.discount:
                discount = invoice.discount
                if discount.active == False or discount.offer_count == 0 or invoice.amount < discount.minimum_amount:
                    invoice.discount = None
                    messages.add_message(request, messages.INFO,
                                         'Invoice created successfully ,But this offer is not available now')

                else:
                    invoice.discount_percentage = discount.percentage
                    including_tax = invoice.amount + (invoice.amount * Decimal(.05))

                    invoice.discount_amount = including_tax * (discount.percentage / 100)
                    invoice.save()

                    discount.offer_count -= 1
                    discount.save()
                    messages.add_message(request, messages.INFO, 'Invoice created successfully')

            invoice.calcultate_grandtotal_and_tax().save()
            return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

            # if invoice.discount:
            #
            #     discount = invoice.discount
            #     if discount.name == "Buddy Deal":
            #         if invoice.customer.buddy_deal == False:
            #             invoice.customer.buddy_deal = True
            #             invoice.customer.save()
            #     elif discount.name == "Designer Sinora Package":
            #         if invoice.items.count() >= discount.offer_count and invoice.amount >= discount.minimum_amount:
            #             if not invoice.customer.designer_sinora_package_date:
            #                 invoice.customer.designer_sinora_package_date = timezone.now().date()
            #                 invoice.customer.save()
            #             else:
            #                 difference = timezone.now().date() - invoice.customer.designer_sinora_package_date
            #                 if difference.days >= 45:
            #                     invoice.customer.designer_sinora_package_date = timezone.now().date()
            #                     invoice.customer.save()
            #
            #     else:
            #         if invoice.amount >= discount.minimum_amount:
            #             if datetime.now().date().strftime('%A') not in week_set:
            #                 if not (datetime.now().time().hour >= 9 and datetime.now().time().hour <= 19):
            #                     invoice.discount = None
            #                     invoice.save()
            #                     messages.add_message(request, messages.INFO, 'This offer is not available now')
            #         return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.pk]))
            #
            #     invoice.discount_percentage = discount.percentage
            #     invoice.discount_amount = (invoice.amount * (discount.percentage / 100))
            #     invoice.save()
            # invoice.calcultate_grandtotal_and_tax().save()

            return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

    context = {
        'product_formset': product_formset,
        'service_formset': service_formset,
        'form_url': reverse_lazy('invoice:edit', args=[pk]),
        'type': "Edit",
        'invoice_form': invoice_form,
        'products': Product.objects.all(),
        'services': Service.objects.all()

    }

    return render(request, "invoice/createinvoic.html", context)


@login_required
@permission_required('invoice.view_invoice_list', raise_exception=True)
def detail(request, pk):
    invoice = Invoice.objects.get(pk=pk)
    services = InvoiceItem.objects.filter(invoice=invoice)
    logo = Office.objects.last()
    location = Location.objects.last()
    employees = Employee.objects.all()

    context = {
        'invoice': invoice,
        'services': services,
        'logo': logo,
        'location': location,
        'employees': employees
    }
    return render(request, "invoice/detail.html", context)


# @login_required
# @permission_required('invoice.view_invoice_list', raise_exception=True)
# def detail(request, pk):
# 	week_set = ['Sunday', 'Monday', 'Tuesday']
# 	invoice = Invoice.objects.get(pk=pk)
# 	services = InvoiceItem.objects.filter(invoice=invoice)
# 	context = {
# 		'invoice': invoice,
# 		'services': services,
# 	}
# 	return render(request, "invoice/detail.html", context)

@login_required
@permission_required('invoice.delete_invoice', raise_exception=True)
def delete(request, pk):
    invoice = Invoice.objects.get(pk=pk)
    if invoice.paid == True:
        messages.add_message(request, messages.WARNING, 'Deletion is not possible')
        return HttpResponseRedirect(reverse('invoice:detail', args=[pk]))
    invoice.delete()
    return HttpResponseRedirect(reverse('invoice:list'))


def mail_settings(request):
    mail_form = InvoiceMailSettingsForm(request.POST or None)
    if request.method == "POST":

        if mail_form.is_valid():
            mail_settings = mail_form.save(commit=False)
            mail_settings.save()
            mail_form.save_m2m()
            return HttpResponseRedirect(reverse('invoice:list'))

    context = {

        'mail_form': mail_form,
    }

    return render(request, "invoice/mail_settings.html", context)


# Sending invoice
def invoice_mail(request, pk):
    invoice = Invoice.objects.get(pk=pk)
    if invoice.customer.email:
        attachement_content = {'invoice': invoice}
        pdf_attachment = render_to_pdf("invoice/invoice_pdf.html", attachement_content, encoding='utf-8')
        htmly = get_template('invoice/email.html')
        d = Context({'invoice': invoice})
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives("Invoice", "Invoice", "info@sinora.com", [invoice.customer.email])
        msg.attach_alternative(html_content, "text/html")
        msg.attach("invoice.pdf", pdf_attachment, 'application/pdf')
        msg.send()
        messages.add_message(request, messages.SUCCESS, "Invoice send successfully")
        invoice.send_mail = True
        invoice.save()
    else:
        messages.add_message(request, messages.ERROR, "Customer does not have an Email ID!")

    return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))


# Generating pdf


def settings_listing(request):
    locations = Location.objects.all()
    Context = {'locations': locations}
    return render(request, "invoice/settings_menu.html", Context)


def location_listing(request):
    f = LocationFilter(request.GET, queryset=Location.objects.all().order_by("-area"))
    locations, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    Context = {
        'locations': locations,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, "invoice/location_listing.html", Context)


@transaction.atomic
def edit_location(request, pk=None):
    location = get_object_or_404(Location, id=pk)
    location_form = LocationForm(request.POST or None, instance=location)
    if request.method == "POST":
        if location_form.is_valid():
            location_form.save()
            messages.add_message(request, messages.SUCCESS, "Location edited successfully")
            return HttpResponseRedirect(reverse('invoice:edit_location', args=[location.id]))

    context = {
        'location_form': location_form,
        'form_url': reverse_lazy('invoice:edit_location', args=[location.pk]),
        'type': "Edit",
        'location': location,

    }
    return render(request, "invoice/add_location.html", context)


@transaction.atomic
def create_location(request, enquiry_pk=None):
    location_form = LocationForm(request.POST or None)
    if request.method == "POST":

        if location_form.is_valid():
            location = location_form.save()
            messages.add_message(request, messages.SUCCESS, "Location created successfully")
    context = {
        'location_form': location_form,
        'form_url': reverse_lazy('invoice:create_location'),
        'type': "Create"

    }
    return render(request, "invoice/add_location.html", context)


def code_settings(request):
    if InvoiceCodeSetting.objects.exists():
        code_form = InvoiceCodeSettingsForm(request.POST or None, instance=InvoiceCodeSetting.objects.last())
    else:
        code_form = InvoiceCodeSettingsForm(request.POST or None)
    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Tyhpanks for ")
            return HttpResponseRedirect(reverse('invoice:list'))

    context = {
        'code_form': code_form,
    }

    return render(request, "invoice/code_setting.html", context)


# def code_settings(request):
#     if InvoiceCodeSetting.objects.exists():
#         code_form = InvoiceCodeSettingsForm(request.POST or None, instance=InvoiceCodeSetting.objects.last())
#     else:
#
#         code_form = InvoiceCodeSettingsForm(request.POST or None)
#     if request.method == "POST":
#         if code_form.is_valid():
#             code_form.save()
#             return HttpResponseRedirect(reverse('invoice:list'))


@login_required
@permission_required('employee.view_report', raise_exception=True)
def daily_report(request, from_date=None, to_date=None):
    from_date = request.GET.get('date')
    if from_date:
        from_date = datetime.strptime(from_date, "%Y-%m-%d").date()
    else:
        from_date = timezone.now().date()
    invoice_items = InvoiceItem.objects.filter(created_at__date=from_date)
    products_invoices = invoice_items.filter(content_type__model="product").aggregate(
        Sum('total_price'), Sum('hours_or_sqfeet'))
    services_invoices = invoice_items.filter(content_type__model="service").aggregate(
        Sum('total_price'), Sum('hours_or_sqfeet'))

    invoices = Invoice.objects.filter(created_at__date=from_date).aggregate(total_tax=Sum('tax_amount'),
                                                                            total_discount=Sum('discount_amount'),
                                                                            total_cp=Sum('amount'),
                                                                            total_sp=Sum('after_adjustment'))

    context = {
        'from_date': from_date,
        'invoices': invoices,
        'services_invoices': services_invoices,
        'products_invoices': products_invoices

    }
    return render(request, "invoice/daily_report.html", context)


class InvoicePDFView(PDFTemplateView):
    template_name = "invoice/invoice_pdf.html"

    def get_context_data(self, **kwargs):
        pk = kwargs.get('pk', False)
        return super(InvoicePDFView, self).get_context_data(
            pagesize="A4",
            title="Invoice",
            invoice=Invoice.objects.get(pk=pk),
            services=InvoiceItem.objects.filter(invoice=Invoice.objects.get(pk=pk)),
            location=Location.objects.last(),
            logo=Office.objects.last(),

        )


def invoice_discount(request, pk, customer_pk):
    discount = get_object_or_404(Discount, pk=pk)

    # service_count = int(request.GET.get('service_count', None))
    total_amount = Decimal(request.GET.get('total_amount', None))
    is_true = False
    # customer = get_object_or_404(Customer, pk=customer_pk)

    # week_set = ['Sunday', 'Monday', 'Tuesday']

    if discount.active == False or discount.offer_count == 0 or total_amount < discount.minimum_amount:
        is_true = True

    # elif total_amount < discount.minimum_amount:
    #     is_true = True

    # elif datetime.now().date().strftime('%A') not in week_set:
    #     is_true = True
    #
    # elif not (datetime.now().time().hour < 9 or datetime.now().time().hour > 19):
    #         is_true = True

    data = {
        'percentage': discount.percentage,
        'is_true': is_true
    }
    return JsonResponse(data)


@login_required
def redeem(request):
    item = get_object_or_404(InvoiceItem, pk=request.POST.get('servicename'))
    purchased = get_object_or_404(CustomerPurchasedPackage, invoice=item.invoice)
    if purchased.crossed_validity:
        item.completed = True
        messages.add_message(request, messages.ERROR, 'Package Expired')
    else:
        item.completed = True
        employee = get_object_or_404(Employee, pk=request.POST.get('employee'))
        item.employee = employee
        messages.add_message(request, messages.SUCCESS, 'Redeem Successful')

    item.save()

    return HttpResponseRedirect(reverse('invoice:detail', args=[item.invoice.id]))


# @login_required
# @permission_required('invoice.change_invoice', raise_exception=True)
# @transaction.atomic
# def createinvoicefix(request, pk=None):
#     if request.method == "POST":
#         invoice_form = InvoiceForm(request.POST)
#         ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm, extra=0,
#                                                      can_delete=True)
#
#         ServiceInlineFormset = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
#                                                      extra=0, can_delete=True, validate_min=True)
#         product_formset = ProductInlineFormSet(request.POST)
#         service_formset = ServiceInlineFormset(request.POST)
#         if invoice_form.is_valid() and product_formset.is_valid() and service_formset.is_valid():
#             invoice = invoice_form.save(commit=False)
#             invoice.customer = get_object_or_404(Customer, pk=request.POST.get('customer'))
#             invoice.supervisor = get_object_or_404(Employee, pk=request.POST.get('supervisor'))
#             invoice.save()
#             if request.POST.get('appointment_pk'):
#                 appointment = get_object_or_404(Appointment, pk=request.GET.get('appointment_pk'))
#                 appointment.status = Appointment.CLOSED
#                 appointment.save()
#                 product_formset.save()
#                 service_formset.save()
#             return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))
#     else:
#         items = []
#         if pk:
#             customer = Customer.objects.get(pk=pk)
#             invoice_form = InvoiceForm(initial={'customer': customer})
#         elif request.GET.get('appointment_pk'):
#             appointment = get_object_or_404(Appointment, pk=request.GET.get('appointment_pk'))
#
#             content_type = ContentType.objects.get_for_model(Service)
#
#             invoice_form = InvoiceForm(initial={'customer': appointment.customer})
#             items = [{'service_prdt': appointment.service.pk, 'content_type': content_type,
#                       'unit_price': appointment.service.price_hour, 'hours_or_sqfeet': 1,
#                       'total_price': appointment.service.price_hour} for appointment in appointment.services.all()]
#         else:
#             invoice_form = InvoiceForm()
#
#         ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm, extra=1,
#                                                      can_delete=True)
#         product_formset = ProductInlineFormSet()
#
#         ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
#                                                      extra=0, can_delete=True, min_num=len(items), validate_min=True)
#         service_formset = ServiceInlineFormSet(initial=items, prefix='service_item')


# context = {
#     'product_formset': product_formset,
#     'form_url': reverse_lazy('invoice:createinvoice'),
#     'type': "Create",
#     'invoice_form': invoice_form,
#     'products': Product.objects.all(),
#     'services': Service.objects.all(),
#     'service_formset': service_formset,
#     'appointment_pk': request.GET.get('appointment_pk')
#
# }
# if pk:
#     customer = Customer.objects.get(pk=pk)
#     invoice_form = InvoiceForm(initial={'customer': customer})
#
# else:
#     invoice_form = InvoiceForm(request.POST or None)
#
# ProductInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceProductForm, extra=1,
#                                              can_delete=True)
# product_formset = ProductInlineFormSet(request.POST or None)
# appointment = None
# if request.GET.get('appointment_pk'):
#     appointment = get_object_or_404(Appointment, pk=request.GET.get('appointment_pk'))
#
#     content_type = ContentType.objects.get_for_model(Service)
#
#     invoice_form = InvoiceForm(initial={'customer': appointment.customer})
#     items = [{'service_prdt': appointment.service.pk, 'content_type': content_type,
#               'unit_price': appointment.service.price_hour, 'hours_or_sqfeet': 1,
#               'total_price': appointment.service.price_hour} for appointment in appointment.services.all()]
#
#     ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
#                                                  extra=0, can_delete=True, min_num=len(items), validate_min=True)
#     service_formset = ServiceInlineFormSet(initial=items, prefix='service_item')
# else:
#     ServiceInlineFormSet = inlineformset_factory(Invoice, InvoiceItem, form=InvoiceServiceForm,
#                                                  extra=1, can_delete=True)
#     service_formset = ServiceInlineFormSet(request.POST or None, prefix='service_item')
#
# if request.method == "POST":
#
#     if invoice_form.is_valid() and product_formset.is_valid() and service_formset.is_valid():
#         invoice = invoice_form.save(commit=False)
#         invoice.customer = get_object_or_404(Customer, pk=request.POST.get('customer'))
#         invoice.supervisor = get_object_or_404(Employee, pk=request.POST.get('supervisor'))
#         invoice.save()
#         if appointment:
#             appointment.status = Appointment.CLOSED
#             appointment.save()
#         product_formset.save()
#         service_formset.save()
#         return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))
#
# context = {
#     'product_formset': product_formset,
#     'form_url': reverse_lazy('invoice:createinvoice'),
#     'type': "Create",
#     'invoice_form': invoice_form,
#     'products': Product.objects.all(),
#     'services': Service.objects.all(),
#     'service_formset': service_formset,
#     'appointment_pk':request.GET.get('appointment_pk')
#
# }
# return render(request, "invoice/createinvoic.html", context)

def check_status(request):
    customer = request.GET.get('customer')
    mobile = request.GET.get('mobile')
    invoice_object = Invoice.objects.filter(customer__name=customer, customer__phone=mobile)

    if invoice_object:
        invoice = 'yes'
    else:
        invoice = 'no'

    data = {'invoice': invoice}
    return JsonResponse(data)
