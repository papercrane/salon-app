# from django.db import transaction
# from rest_framework import serializers
# from invoice.models import Invoice, InvoicePaymentTerms
# from erp_core.models import PaymentTerm, PaymentType
# from employee.serializers import EmployeeSerializer
# from customer.serializers import ContactViewSerializer
# from customer.models import Contact
# from quotation.serializers import QuotationViewSerializer
# # from django.shortcuts import get_object_or_404
# from product.serializers import ProductViewSerializer


# class PaymentSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = PaymentTerm
#         fields = ('term', 'percentage_value', 'type')


# class InvoicePaymentSerializer(serializers.ModelSerializer):
#     # invoice = InvoiceSerializer(many=False, read_only=True)
#     payment_terms = PaymentSerializer(many=False, read_only=True)

#     class Meta:
#         model = InvoicePaymentTerms
#         fields = ('payment_terms', 'date', 'status', 'amount', 'balance')


# class InvoiceSerializer(serializers.ModelSerializer):
#     contact = ContactViewSerializer(many=False, read_only=True)
#     sales_person = EmployeeSerializer(many=False, read_only=True)
#     quotation = QuotationViewSerializer(many=False, read_only=True)
#     payment_term = InvoicePaymentSerializer(many=True, read_only=True)

#     class Meta:
#         model = Invoice
#         fields = ('id', 'code', 'contact', 'sales_person', 'quotation', 'shipping_charges', 'paid',
#                   'paid_date', 'tax', 'amount_paid', 'due_date', 'credit_periods', 'amount', 'payment_term')


# class ResultSeriallizer(serializers.Serializer):
#     result = InvoiceSerializer(many=False, read_only=True)
