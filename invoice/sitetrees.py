from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.

        item(
            'Invoice',
            'invoice:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="info-circle",

            children=[
                # item(
                #     'Create Invoice',
                #     'invoice:invoice_redirect',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),
                # item(
                #     'Invoice List',
                #     'invoice:list',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),



            ]

        ),

        # item(
        #     'Reminder',
        #     'invoice:remainder',
        #     in_menu=True,
        #     in_sitetree=True,
        #     access_loggedin=True,
        #     icon_class="clock-o",

        #     children=[
        #         # item(
        #         #     'Create Invoice',
        #         #     'invoice:invoice_redirect',
        #         #     in_menu=True,
        #         #     in_sitetree=True,
        #         #     access_loggedin=True,
        #         #     icon_class="print",
        #         #     # access_by_perms=['enquiry.view_quote_list'],

        #         # ),
        #         # item(
        #         #     'Invoice List',
        #         #     'invoice:list',
        #         #     in_menu=True,
        #         #     in_sitetree=True,
        #         #     access_loggedin=True,
        #         #     icon_class="print",
        #         #     # access_by_perms=['enquiry.view_quote_list'],

        #         # ),



        #     ]

        # ),
         item(
            'Settings',
            'invoice:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="cog",

            children=[
            ]

        ),
        # item(
        #     'Permission',
        #     'employee:employee_permission',
        #     in_menu=True,
        #     in_sitetree=True,
        #     access_loggedin=True,
        #     icon_class="lock",

        #     children=[
        #     ]

        # ),

        item(
            'Report',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="file",

            children=[
                item(
                    'Daily Report',
                    'invoice:daily_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="calendar-check-o",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Sales By Customer',
                    'customer:sales_by_customer',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="handshake-o",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Sales By Employee',
                    'employee:sales_by_employee',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="user",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Sales By Product',
                    'product:sales_by_product',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="product-hunt",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Sales By Service',
                    'service:sales_by_service',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="wrench",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
            ]
        )
    ]),
    # ... You can define more than one tree for your app.


tree('sidebar_qusais_admin', items=[
        # Then define items and their children with `item` function.
        item(
            'Invoice',
            'invoice:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="handshake-o",
            # access_by_perms=['project.view_project_list'],

        ),

    ]),






)
