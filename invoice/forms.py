from django import forms
from django.db.models import Q
from django.forms import ModelForm
from invoice.models import Invoice, InvoiceMailSetting, InvoiceCodeSetting, InvoiceShippingAddress, \
    InvoiceBillingAddress, InvoiceItem
from product.models import Product
from service.models import Service
from django.db.models import Q


class InvoiceEditForm(ModelForm):
    class Meta:
        model = Invoice
        widgets = {
            "contact": forms.Select(attrs={'class': "form-control", 'id': "contact"}),
            "code": forms.TextInput(attrs={'class': "form-control", 'id': "code"}),
            "due_date": forms.DateInput(
                attrs={'class': "form-control", 'required': "required", 'type': "text", 'id': "duedate"}),

        }
        fields = ['due_date', ]


class InvoiceMailSettingsForm(ModelForm):
    class Meta:
        model = InvoiceMailSetting
        widgets = {"subject": forms.TextInput(attrs={'class': "form-control", 'id': "subject", 'required': "required"}),
                   "content": forms.Textarea(attrs={'class': "form-control", 'id': "content", 'required': "required"}),
                   "cc": forms.Select(attrs={'class': "form-control", 'id': "cc"}),
                   "group_cc": forms.SelectMultiple(
                       attrs={'class': "form-control", "data-toggle": "select", "id": "group_cc",
                              'multiple': "multiple"}),
                   }
        fields = ['subject', 'content', 'cc', 'group_cc']


class InvoiceCodeSettingsForm(ModelForm):
    # pass
    class Meta:
      model = InvoiceCodeSetting
      widgets = {"prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
                 "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
                 "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
                 }
      fields = ['prefix', 'count_index', 'no_of_characters']


class AddressModelChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, address):
        return "%s, %s, %s, %s, %s, %s" % (
            address.addressline1, address.addressline2, address.area, address.country, address.state, address.city)


# class InvoiceShippingAddressSelectForm(forms.Form):
#     shipping_address = AddressModelChoiceField(
#         queryset=InvoiceShippingAddress.objects.none(),
#         required=False,
#         widget=forms.Select(
#             attrs={'class': 'form-control', 'id': "shipping_address"}),
#     )

#     def __init__(self, *args, **kwargs):
#         contact = kwargs.pop('contact', None)
#         super(InvoiceShippingAddressSelectForm, self).__init__(*args, **kwargs)
#         if contact is not None:
#             self.fields['shipping_address'].queryset = contact.addresses.all()


# class InvoiceShippingAddressForm(ModelForm):

#     class Meta:
#         model = InvoiceShippingAddress
#         widgets = {
#             "addressline1": forms.TextInput(attrs={'class': "form-control1", "id": "shipping-addressline1"}),
#             "addressline2": forms.TextInput(attrs={'class': "form-control1", "id": "shipping-addressline2"}),
#             "area": forms.TextInput(attrs={'class': "form-control1", "id": "shipping-area"}),
#             "zipcode": forms.NumberInput(attrs={'class': "form-control1", "id": "shipping-zipcode"}),
#             "country": forms.Select(attrs={'class': "form-control1", 'required': "required", 'id': "shipping-country"}),
#             "state": forms.Select(attrs={'class': "form-control1 chained", 'required': "required", 'id': "shipping-state"}),
#             "city": forms.Select(attrs={'class': "form-control1 chained ", 'required': "required", 'id': "shipping-city"}),
#         }
#         exclude = ['invoice']


class InvoiceBillingAddressForm(ModelForm):
    class Meta:
        model = InvoiceBillingAddress
        widgets = {
            "addressline1": forms.TextInput(attrs={'class': "form-control1", "id": "billing-addressline1"}),
            "addressline2": forms.TextInput(attrs={'class': "form-control1", "id": "billing-addressline2"}),
            "area": forms.TextInput(attrs={'class': "form-control1", "id": "billing-area"}),
            "zipcode": forms.NumberInput(attrs={'class': "form-control1", "id": "billing-zipcode", 'min': 0}),
            "country": forms.Select(attrs={'class': "form-control1", 'required': "required", 'id': "billing-country"}),
            "state": forms.Select(
                attrs={'class': "form-control1 chained", 'required': "required", 'id': "billing-state"}),
            "city": forms.Select(
                attrs={'class': "form-control1 chained ", 'required': "required", 'id': "billing-city"}),
        }
        exclude = ['invoice']


class InvoiceItemForm(ModelForm):
    pass

    # class Meta:
    #   model = InvoiceItem

    #   widgets = {
    #       # "service": forms.Select(attrs={'class': "form-control product", "style": "width: 100%;", 'required': "required"}),
    #       # "price": forms.NumberInput(attrs={'class': "form-control price", 'id': "price", 'required': "required"}),
    #       # "quantity": forms.NumberInput(attrs={'class': "form-control quantity", 'id': "quantity", 'required': "required"}),
    #       # "expected_date": forms.DateInput(attrs={'class': "form-control tdate", "placeholder": "expected delivery date"}),
    #       # "total_price": forms.TextInput(attrs={'name': 'total', 'class': "form-control total", 'required': "required"}),
    #       # "discount": forms.NumberInput(attrs={'class': "form-control discount"}),
    #       # "discount_type": forms.Select(attrs={'class': "form-control discount_type"}),
    #   }

    #   fields = ['service', ]


class PaymentMethodForm(ModelForm):
    class Meta:
        model = Invoice
        widgets = {
            # "sales_person": forms.Select(attrs={'class': "form-control", 'id': "sales_person", 'required': "required"}),
            "payment_method": forms.Select(attrs={'class': "form-control", 'id': "payment_method"}),
            # "shipping_charges": forms.NumberInput(attrs={'class': "form-control", 'id': "shipping_charges", 'required': "required"}),
            # "tax": forms.NumberInput(attrs={'class': "form-control", 'id': "tax", 'required': "required"}),

        }
        fields = ['payment_method', ]


class InvoiceServiceForm(ModelForm):
    service_prdt = forms.ChoiceField(choices=[],
                                  widget=forms.Select(attrs={'class': 'form-control service'}))

    def __init__(self, *args, **kwargs):
        super(InvoiceServiceForm, self).__init__(*args, **kwargs)
        choices = []
        for obj1 in Service.objects.all():
            choices.append((obj1.id, obj1.name))
        self.fields['service_prdt'].choices = choices
        # self.fields['unit_price'].name = "service_unit_price"
        # self.fields['total_price'].name = "service_total_price"
        # self.fields['hours_or_sqfeet'].name = "service_quantity"
        self.fields['unit_price'].label = 'Unit Price'

    class Meta:
        model = InvoiceItem
        widgets = {
            "unit_price": forms.NumberInput(
                attrs={'class': "form-control service_unit_price",'name':"service_unit_price",'id': "service_unit_price", 'min': 0}),
            "total_price": forms.NumberInput(
                attrs={'class': "form-control service_total_price",'name':"service_total_price",'id': "service_total_price", 'min': 0}),
            "hours_or_sqfeet": forms.NumberInput(
                attrs={'class': "form-control service_quantity",'name':"service_quantity",'id': "service_quantity", 'min': 0}),
            "employee": forms.Select(
                attrs={'class': "form-control employee", 'id': "employee"}),
            
           

        }
        fields = ['unit_price', 'total_price', 'hours_or_sqfeet','employee' ]


class InvoiceForm(ModelForm):
    class Meta:
        model = Invoice
        widgets = {
            "discount": forms.Select(
                attrs={'class': "form-control content_type", 'id': "discount"}),
            "customer": forms.Select(
                attrs={'class': "form-control customer", 'id': "customer", 'required': "required"}),
            "supervisor": forms.Select(
                attrs={'class': "form-control supervisor", 'id': "supervisor", 'required': "required"}),
            "code": forms.TextInput(attrs={'name': 'code', 'class': "form-control code", 'required': "required", 'readonly':'readonly'}),
            "paid_date": forms.TextInput(
                attrs={'class': "form-control pldate", 'required': "required", 'type': "text", 'id': "pldate"}),
            "date": forms.TextInput(
                attrs={'class': "form-control pldate", 'required': "required", 'type': "text", 'id': "pldate"}),
            # "grand_total": forms.NumberInput(
            #     attrs={'class': "form-control grand_total", 'required': "required", 'type': "number",'step':0.0001}),
            "after_adjustment": forms.NumberInput(
                attrs={'class': "form-control after_adjustment", 'required': "required", 'type': "text",
                       'id': "after_adjustment", 'min': 0}),

        }
        fields = ['discount', 'customer', 'supervisor', 'code', 'paid_date','after_adjustment','date']


class InvoiceProductForm(ModelForm):
    prdt = forms.ChoiceField(choices=[],
                             widget=forms.Select(attrs={'class': 'form-control product'}))

    def __init__(self, *args, **kwargs):
        super(InvoiceProductForm, self).__init__(*args, **kwargs)
        choices = []
        for obj2 in Product.objects.all():
            choices.append((obj2.id, obj2.name))
        self.fields['prdt'].choices = choices

    class Meta:
        model = InvoiceItem
        widgets = {
            "unit_price": forms.NumberInput(
                attrs={'class': "form-control unit_price", 'id': "unit_price", 'min': 0}),
            "total_price": forms.NumberInput(
                attrs={'class': "form-control total_price", 'id': "total_price", 'min': 0}),
            "hours_or_sqfeet": forms.NumberInput(
                attrs={'class': "form-control quantity", 'id': "quantity", 'min': 0}),


        }
        fields = ['unit_price', 'total_price', 'hours_or_sqfeet' ]
