from django.apps import AppConfig


class InvoiceConfig(AppConfig):
    name = 'invoice'

    def ready(self):

        from .models import InvoiceCodeSetting,ReceiptCodeSetting,InvoicePaymentReceipt
        count = 1
        if not InvoiceCodeSetting.objects.count() > 0:
            InvoiceCodeSetting.objects.update_or_create(count=count, defaults={'count': count})
        count2 = 1
        if not ReceiptCodeSetting.objects.count() > 0:
            ReceiptCodeSetting.objects.update_or_create(count=count2, defaults={'count': count2})

