import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from invoice.models import Invoice
from customer.models import Customer
from location.models import Location

class LocationFilter(django_filters.FilterSet):

    area = django_filters.CharFilter(label="area", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Location
        fields = ['area']

    def filter_by_contact(self, queryset, area, value):
        return queryset.filter(
            Q(area__icontains=value) | Q(area__icontains=value)
        )

    def order_by_field(self, queryset, area, value):
        return queryset.order_by(value)


class InvoiceFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))

    # amount = django_filters.NumberFilter(widget=forms.Select(attrs={'class': 'form-control'}))
    # amount__gt = django_filters.NumberFilter(name='amount', lookup_expr='gt',
    #                                          widget=forms.NumberInput(attrs={'class': 'form-control'}))
    # amount__lt = django_filters.NumberFilter(name='amount', lookup_expr='lt',
    #                                          widget=forms.NumberInput(attrs={'class': 'form-control'}))
    paid = django_filters.BooleanFilter(
        name="paid",
        widget=widgets.BooleanWidget(attrs={'class': 'form-control input-sm'})
    )

    customer = django_filters.ModelChoiceFilter(
        queryset=Customer.objects.all(),
        # method='filter_by_contact',
        widget=forms.Select(attrs={'class': 'form-control input-sm', 'style': "width: 100%;"})
    )

    created_at = django_filters.DateTimeFromToRangeFilter(
        name='created_at', widget=widgets.RangeWidget(attrs={'type': 'hidden','class': 'form-control'})
    )
    # due_date = django_filters.DateTimeFromToRangeFilter(
    #     name='due_date', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    # order_by_amount = django_filters.ChoiceFilter(label="amount", name="amount", method='order_by_field', choices=(
    #     ('amount', 'Ascending'), ('-amount', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))
    # order_by_due_date = django_filters.ChoiceFilter(label="Due Date", name="due_date", method='order_by_field', choices=(
    #     ('due_date', 'Ascending'), ('-due_date', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))
    # order_by_credit_periods = django_filters.ChoiceFilter(label="Credit Periods", name="credit_periods", method='order_by_field', choices=(
    #     ('credit_periods', 'Ascending'), ('-credit_periods', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))
    # order_by_created_at = django_filters.ChoiceFilter(label="Created At", name="created_at", method='order_by_field', choices=(
    #     ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Invoice
        fields = ['code']

    # def filter_by_contact(self, queryset, name, value):
    #     return queryset.filter(
    #         Q(contact__name=value) | Q(contact__id=value.id)
    #     )

    # def order_by_field(self, queryset, name, value):
    #     return queryset.order_by(value)
