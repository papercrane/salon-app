from django.conf.urls import url
from invoice import invoice_view


app_name = "invoice"



urlpatterns = [
    url(r'^invoice/create/$', invoice_view.createinvoice, name="createinvoice"),
    url(r'^invoice/(?P<pk>[^/]+)/createinvoice/$', invoice_view.createinvoice, name="createinvoice"),
    url(r'^invoice/createcustomer/$', invoice_view.createcustomer, name="createcustomer"),
    url(r'^invoice/(?P<pk>[^/]+)/edit/$', invoice_view.edit, name='edit'),
    url(r'^invoice/(?P<pk>[^/]+)/detail/$', invoice_view.detail, name='detail'),
    url(r'^invoice/(?P<pk>[^/]+)/delete/$', invoice_view.delete, name='delete'),
    url(r'^invoices/$', invoice_view.list_view, name='list'),
    url(r'^invoice/mail/settings/$', invoice_view.mail_settings, name='mail_settings'),
    url(r'^invoice/(?P<pk>[^/]+)/mail/$', invoice_view.invoice_mail, name='invoice_mail'),
    url(r'^invoice/code/settings/$', invoice_view.code_settings, name='code_settings'),
    url(r'^settings/$', invoice_view.settings_listing, name="settings"),
    url(r'^settings/locations/$', invoice_view.location_listing, name="location_listing"),
    url(r'^settings/locations/(?P<pk>[^/]+)/$', invoice_view.edit_location, name="edit_location"),
    url(r'^settings/add/$', invoice_view.create_location, name="create_location"),
    url(r'^sales/daily/report/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', invoice_view.daily_report, name="daily_report"),
    url(r'^sales/daily/report/$', invoice_view.daily_report, name="daily_report"),
    url(r'^invoice/(?P<pk>[^/]+)/detail.pdf/$', invoice_view.InvoicePDFView.as_view(), name="invoice_pdf"),
    url(r'^invoice/discount/(?P<pk>[^/]+)/(?P<customer_pk>[^/]+)/$', invoice_view.invoice_discount, name="invoice_discount"),
    url(r'^redeem/$', invoice_view.redeem, name='redeem'),
    url(r'^check_status/$', invoice_view.check_status),

]

