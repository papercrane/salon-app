from datetime import date

from django.db import models
from django.db.models import Q
from erp_core.models import ErpAbstractBaseModel, PaymentTerm, BaseModel, BaseCodeSetting
from employee.models import Employee
from customer.models import Customer
from address.models import Address as BaseAddress
from erp_core.models import DISCOUNT_PERCENTAGE
from django.core.validators import MinValueValidator
from discount.models import Discount
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from decimal import Decimal

from package.models import Package
from django.utils import timezone

class Invoice(ErpAbstractBaseModel):

    def newCode():
        return InvoiceCodeSetting.new_code()

    CASH = 'Cash'
    CHEQUE = 'Cheque'
    ONLINE_TRANSFER = 'Online Transfer'
    CHOICES = (

        (CASH, 'Cash'),
        (CHEQUE, 'Cheque'),
        (ONLINE_TRANSFER, 'Online Transfer'),
    )

    code = models.CharField(max_length=100, unique=True, default=newCode)
    customer = models.ForeignKey(Customer, related_name="%(class)ss")
    supervisor = models.ForeignKey(Employee, related_name="%(class)ss")
    paid = models.BooleanField(default=True)
    paid_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    credit_periods = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
    amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True,
                                 default=0, validators=[MinValueValidator(0)])
    payment_method = models.CharField(max_length=10, choices=CHOICES, default=CASH)
    discount = models.ForeignKey(Discount, null=True, blank=True, related_name="invoice_discount")
    discount_percentage = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    discount_amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    tax_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    grand_total = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    # office = models.ForeignKey(Office,null=True,blank=True)
    after_adjustment = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    package_invoice = models.BooleanField(default=False)
    date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.code

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            InvoiceCodeSetting.incrementCountIndex()

        return super(Invoice, self).save(*args, **kwargs)

    def invoiceTotal(self):

        items = self.items.all()
        total_amount = 0
        for item in items:
            total_amount += item.total_price
        self.amount = total_amount
        return self

    def after_discount(self):
        if not self.amount:
            self.amount = 0
        if not self.discount_amount:
            self.discount_amount = 0

        after_discount = self.amount - self.discount_amount
        return after_discount

    def calcultate_grandtotal_and_tax(self):
        if not self.amount:
            self.amount = 0
        if not self.discount_amount:
            self.discount_amount = 0

        vat = self.amount * round(Decimal(.05), 2)
        self.tax_amount = vat

        grand_total = self.amount + vat
        after_discount = grand_total - self.discount_amount

        self.grand_total = after_discount

        return self

    def calculate_adjustment(self):
        return self.after_adjustment - self.grand_total

    def total_cost_price(self):
        return self.amount + self.tax_amount

    class Meta:
        permissions = (
            ("view_invoice_list", "Can view invoice list"),

        )


class InvoicePaymentTerms(ErpAbstractBaseModel):
    PAID = 'paid'
    PARTIALLY_PAID = 'partially paid'
    NOT_PAID = 'not paid'
    CHOICES = (
        (PAID, 'Paid'),
        (PARTIALLY_PAID, 'Partially Paid'),
        (NOT_PAID, 'Not Paid'),
    )
    invoice = models.ForeignKey(Invoice, related_name="payment_term")
    payment_terms = models.ForeignKey(PaymentTerm, related_name="payments", null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=20, choices=CHOICES, default=NOT_PAID)
    amount = models.DecimalField(max_digits=50, decimal_places=4, null=True, blank=True,
                                 default=0, validators=[MinValueValidator(0)])
    balance = models.DecimalField(max_digits=50, decimal_places=4, null=True, blank=True,
                                  default=0, validators=[MinValueValidator(0)])

    def __str__(self):
        return str(self.invoice) + "- " + str(self.payment_terms)

    def isNotPaid(self):
        return self.status != self.NOT_PAID

    def totalAmountPaid(self):
        return self.amount - self.balance


class InvoiceMailSetting(models.Model):
    subject = models.CharField(max_length=20, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    cc = models.ForeignKey(Employee, related_name="%(class)ss", null=True, blank=True)
    group_cc = models.ManyToManyField(Employee, blank=True)


class InvoiceCodeSetting(BaseCodeSetting):
    pass


class Achievements(ErpAbstractBaseModel):
    invoice = models.ForeignKey(Invoice)
    employee = models.ForeignKey(Employee)
    sales_amount = models.DecimalField(max_digits=50, decimal_places=2, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.employee) + "-" + " from " + str(self.date)


class InvoiceShippingAddress(BaseAddress):
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE, related_name="shipping_address")


class InvoiceBillingAddress(BaseAddress):
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE, related_name="billing_address")


class InvoiceItem(BaseModel):
    @staticmethod
    def roundOff(price):
        return round(price, 2)

    limit = models.Q(app_label='product', model='product') | models.Q(app_label='service', model='service')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     limit_choices_to=limit)
    object_id = models.UUIDField(null=True, blank=True)

    content_object = GenericForeignKey('content_type', 'object_id')

    unit_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[
        MinValueValidator(0)], null=True, blank=True)
    hours_or_sqfeet = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)], null=True,
                                          blank=True, verbose_name="quantity")
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name="items", null=True, blank=True)
    total_price = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True,
                                      validators=[MinValueValidator(0)])
    completed = models.BooleanField(default=True)
    employee = models.ForeignKey(Employee,null=True)

    def __str__(self):
        return str(self.content_type) + "-" + str(self.invoice)

    def save(self, *args, **kwargs):
        return super(InvoiceItem, self).save(*args, **kwargs)

    def setAfterDiscount(self):
        if self.discount_type == DISCOUNT_PERCENTAGE:
            after_discount = self.price * (1 - self.discount / 100)
        else:
            after_discount = self.price - self.discount
        self.after_discount = self.roundOff(after_discount)
        return self

    def setTotalPrice(self):
        if not self.hours_or_sqfeet:
            self.hours_or_sqfeet = 1
        self.total_price = self.unit_price * self.hours_or_sqfeet
        return self

    def getPrice(self):
        return self.after_discount

    # def setPrice(self):

    #     self.total_price = self.price * self.quantity
    #     self.save()


# model for set default mail configuration


class InvoicePaymentReceipt(BaseModel):
    def newCode():
        return ReceiptCodeSetting.new_code()

    code = models.CharField(max_length=255, default=newCode, unique=True, null=True, blank=True)
    invoice = models.OneToOneField(Invoice, related_name='receipt', null=True, blank=True)
    # payment_term = models.ForeignKey(InvoicePaymentTerms)
    amount = models.DecimalField(max_digits=50, decimal_places=2, null=True,
                                 blank=True, validators=[MinValueValidator(0)])
    date = models.DateField(null=True, blank=True)

    def __str__(self):
        # return str(self.invoice) + "-" + str(self.payment_term) + "-" + str(self.code)
        return str(self.code)

    # def totalAmountPaid(self):
    #     return self.payment_term.amount - self.payment_term.balance

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            ReceiptCodeSetting.incrementCountIndex()
        return super(InvoicePaymentReceipt, self).save(*args, **kwargs)


class ReceiptCodeSetting(ErpAbstractBaseModel):
    count = models.IntegerField(default=0)

    @classmethod
    def new_code(cls):
        code = ""
        if cls.objects.exists():
            code_setting = cls.objects.last()
            code = code_setting.generateCode()
        return code

    def generateCode(self):
        # code_prefix = self.prefix
        count = self.count
        # no_of_chars = self.no_of_characters
        return str("RECE") + str(format(count, '0'))

    @classmethod
    def incrementCountIndex(cls):
        if cls.objects.exists():
            code_setting = cls.objects.last()
            code_setting.count = models.F('count') + 1
            code_setting.save()


class CustomerPurchasedPackage(models.Model):
    package = models.ForeignKey(Package, related_name='purchased_packages')
    invoice = models.ForeignKey(Invoice, related_name='packages')
    validity_date = models.DateField()

    @property
    def crossed_validity(self):
        return date.today() > self.validity_date

