from django import template

register = template.Library()




@register.simple_tag
def compare_uuid(arg1, arg2):

	if str(arg1) == str(arg2):
		return "selected"
	return ""


@register.simple_tag
def pag_count(arg1, arg2,arg3):
	present_count = arg1+(arg2-1)*arg3
	return present_count


