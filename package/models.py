from django.core.validators import MinValueValidator
from django.db import models

# Create your models here.
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting



class Package(ErpAbstractBaseModel):

    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField("Package Price", max_digits=20, decimal_places=2, null=True, blank=True,
                                validators=[MinValueValidator(0)])
    active = models.BooleanField(default=True)
    validity_days = models.PositiveIntegerField(null=True)
    minimum_purchase_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True,
                                                  blank=True,
                                                  validators=[MinValueValidator(0)])
    no_services = models.PositiveIntegerField("No of Services can be Purchased", default=1)

    def __str__(self):
        return self.name
