from django import forms
from django.forms import ModelForm

from employee.models import Employee
from service.models import Service
from .models import Package

class PackageServiceForm(forms.Form):
    service = forms.ModelChoiceField(queryset=Service.objects.all(),required=True,widget=forms.Select(attrs={'class': 'form-control service','required':'required'}))
    employee = forms.ModelChoiceField(queryset=Employee.objects.all(),required=False,widget=forms.Select(attrs={'class': 'form-control service'}))


class PackageForm(ModelForm):
    class Meta:
        model = Package

        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "description": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "validity_days": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "minimum_purchase_amount": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "no_services": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
            "active": forms.CheckboxInput(),
        }
        fields = ['name', 'price', 'description', 'validity_days', 'minimum_purchase_amount', 'no_services', 'active']