# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2018-12-31 13:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('package', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='no_services',
            field=models.PositiveIntegerField(default=1, verbose_name='No of Services can be Purchased'),
        ),
    ]
