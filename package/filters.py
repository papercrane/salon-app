import django_filters
from django import forms
from package.models import Package


class PackageFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    price = django_filters.NumberFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Package
        fields = ['name', 'price']

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
