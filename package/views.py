from django.contrib import messages
from django.contrib.auth.decorators import login_required
from decimal import Decimal

from django.db import transaction
from django.utils import timezone

from .forms import *
from erp_core.pagination import paginate
from .filters import PackageFilter
from django.contrib.contenttypes.models import ContentType
from django.db.models import Sum
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render

# Create your views here.
from django.urls import reverse, reverse_lazy

from customer.models import Customer
from employee.models import Employee
from invoice.forms import InvoiceForm
from invoice.models import InvoiceItem, CustomerPurchasedPackage, Invoice
from package.forms import PackageServiceForm
from package.models import Package
from service.models import Service


@transaction.atomic
@login_required
def buy_package(request, pk):
    package = get_object_or_404(Package, pk=pk)
    if request.GET.get("customer_pk"):
        customer = Customer.objects.get(pk=request.GET.get("customer_pk"))
        invoice_form = InvoiceForm(initial={'customer': customer})
    else:
        invoice_form = InvoiceForm(request.POST or None)

    service_formset = formset_factory(PackageServiceForm, min_num=package.no_services, can_delete=False, extra=0)
    service_formset = service_formset(request.POST or None)
    if request.method == "POST" and invoice_form.is_valid() and service_formset.is_valid():
        customer = Customer.objects.get(pk=request.POST.get('customer'))
        invoice = invoice_form.save(commit=False)
        invoice.customer = customer
        invoice.supervisor = get_object_or_404(Employee, pk=request.POST.get('supervisor'))
        invoice.package_invoice = True
        invoice.save()

        unit_price = package.price / package.no_services
        content_type = get_object_or_404(ContentType, model='service')
        for service_form in service_formset:
            item = InvoiceItem.objects.create(invoice=invoice, content_type=content_type,
                                              content_object=service_form.cleaned_data['service'],
                                              unit_price=unit_price, hours_or_sqfeet=1, completed=False,employee=service_form.cleaned_data['employee'])
            item.total_price = item.unit_price * item.hours_or_sqfeet
            item.save()
        invoice.amount = invoice.items.aggregate(Sum('unit_price'))['unit_price__sum']
        invoice.discount_amount = invoice.amount - package.price
        invoice.tax_amount = invoice.amount * Decimal(.05)
        invoice.grand_total = package.price + invoice.tax_amount
        invoice.after_adjustment = invoice.grand_total
        invoice.save()
        CustomerPurchasedPackage.objects.create(invoice=invoice,
                                                validity_date=invoice.paid_date + timezone.timedelta(days=45),
                                                package=package)
        messages.add_message(request, messages.SUCCESS, "Package bought successfully")
        return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

    context = {
        'service_formset': service_formset,
        'invoice_form': invoice_form,
        'package': package.price,
        'selling_price': round(package.price + (package.price * Decimal(.05)), 2),
        'form_url': reverse_lazy('package:buy_package', args=[pk]),
        'services': Service.objects.all(),
        'minimum_purchase_amount': package.minimum_purchase_amount,
        'type':"Create",

    }
    return render(request, "package/buy_package.html", context)


@login_required
def create(request):
    form = PackageForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Package created successfully")
            return HttpResponseRedirect(reverse('package:list'))
    context = {
        "form": form,
        'form_url': reverse_lazy('package:create'),
        "type": "Create"
    }
    return render(request, 'package/create.html', context)


@login_required
def edit(request, pk):
    package = Package.objects.get(id=pk)
    form = PackageForm(
        request.POST or None, instance=package)
    if request.method == "POST":
        form.save()
        messages.add_message(request, messages.SUCCESS, "Package edited successfully")
        return HttpResponseRedirect(reverse('package:list'))
    context = {
        "form": form,
        'form_url': reverse_lazy('package:edit', args=[pk]),
        "type": "Edit",
        'product': package,
    }
    return render(request, 'package/create.html', context)


@login_required
def list(request):
    f = PackageFilter(request.GET, queryset=Package.objects.all().order_by("-created_at"))
    packages, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    context = {
        "packages": packages,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, 'package/list.html', context)


@login_required
def delete(request, pk):
    if request.method == 'POST':
        package = get_object_or_404(Package, pk=pk)
        package.delete()
        messages.add_message(request, messages.SUCCESS, "Package deleted successfully")
        return HttpResponseRedirect(reverse('package:list'))



@transaction.atomic
@login_required
def edit_purchased_package(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)

    purchased_package = CustomerPurchasedPackage.objects.get(invoice__pk=invoice.pk)
    if purchased_package.crossed_validity:
        messages.add_message(request, messages.ERROR, "Package validity Expired on " + str(purchased_package.validity_date))
        return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))
    invoice_items = invoice.items.filter(completed=False)
    if not invoice_items:
        messages.add_message(request, messages.ERROR, "Package Used!!")
        return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

    invoice_form = InvoiceForm(request.POST or None, instance=invoice)

    service_formset = formset_factory(PackageServiceForm, max_num=purchased_package.package.no_services,
                                      can_delete=False, extra=0)
    serv = []
    for ser in invoice_items.values('services__id','employee'):
        serv.append({'service': ser['services__id'],'employee':ser['employee']})
    service_formset = service_formset(request.POST or None, initial=serv)
    if request.method == "POST" and invoice_form.is_valid() and service_formset.is_valid():
        customer = Customer.objects.get(pk=request.POST.get('customer'))
        invoice = invoice_form.save(commit=False)
        invoice.customer = customer
        invoice.supervisor = get_object_or_404(Employee, pk=request.POST.get('supervisor'))
        invoice.save()
        unit_price = purchased_package.package.price / purchased_package.package.no_services
        content_type = get_object_or_404(ContentType, model='service')
        invoice_items.delete()

        for service_form in service_formset:
            item = InvoiceItem.objects.create(content_object=service_form.cleaned_data['service'],
                                              unit_price=unit_price, hours_or_sqfeet=1, completed=False,
                                              invoice=invoice, content_type=content_type,employee=service_form.cleaned_data['employee'] )
            item.total_price = item.unit_price * item.hours_or_sqfeet
            item.save()
        invoice.amount = invoice.items.aggregate(Sum('unit_price'))['unit_price__sum']
        invoice.discount_amount = invoice.amount - purchased_package.package.price
        invoice.tax_amount = invoice.amount * Decimal(.05)
        invoice.grand_total = purchased_package.package.price + invoice.tax_amount
        invoice.after_adjustment = invoice.grand_total
        invoice.save()
        messages.add_message(request, messages.SUCCESS, "Package edited successfully")
        return HttpResponseRedirect(reverse('invoice:detail', args=[invoice.id]))

    context = {
        'service_formset': service_formset,
        'invoice_form': invoice_form,
        'package': purchased_package.package.price,
        'selling_price': round(purchased_package.package.price + (purchased_package.package.price * Decimal(.05)), 2),
        'form_url': reverse_lazy('package:editpackage', args=[pk]),
        'type':'Edit'

    }
    return render(request, "package/buy_package.html", context)
