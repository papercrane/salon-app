from django.conf.urls import url
from package import views

app_name = "package"


urlpatterns = [
    url(r'^add/$', views.create, name='create'),
    url(r'^$', views.list, name='list'),
    url(r'^(?P<pk>[^/]+)/delete/$', views.delete, name='delete'),
    url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name='edit'),
    url(r'^buy/package/(?P<pk>[^/]+)/$', views.buy_package, name='buy_package'),
    url(r'^(?P<pk>[^/]+)/invoice_edit/$', views.edit_purchased_package, name='editpackage'),

]

