from django.conf.urls import include, url
from supplier import supplier, contact, address

app_name = "supplier"


urlpatterns = [
    url(r'^suppliers/$', supplier.index, name="list"),
    url(r'^supplier/create/$', supplier.create, name="create"),
    url(r'^supplier/(?P<pk>[^/]+)/$', supplier.detail, name="detail"),
    url(r'^supplier/(?P<pk>[^/]+)/edit/$', supplier.edit, name="edit"),
    url(r'^supplier/contact/(?P<pk>[^/]+)/$', contact.detail, name="supplier-contact-detail"),
    url(r'^supplier/contact/(?P<pk>[^/]+)/addresses/create/$', address.create, name="address_create"),
    url(r'^supplier/contact/addresses/(?P<pk>[^/]+)/edit/$', address.edit, name="address_edit"),
    url(r'^(?P<pk>[^/]+)/delete/$', supplier.delete, name="supplier_delete"),



]
