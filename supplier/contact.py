# Create your views here.
from django.db.models import F
from django.db import transaction
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.forms import inlineformset_factory
from django.template.loader import get_template
from django.template import Context
# from django.utils import timezone
from erp_core.pagination import paginate


from supplier.models import SupplierAddress, Supplier
# from customer.forms import ContactAddressForm, CustomerForm, ContactForm
# from customer.filters import CustomerFilter


# def list(request):
#     print(request.GET)

#     f = CustomerFilter(request.GET)

#     customers, pagination = paginate(request, queryset=f.qs, per_page=2, default=True)

#     print(f.form)
#     context = {
#         'customers': customers,
#         # 'status': Customer.STATUSES,
#         'pagination': pagination,
#         'filter': f,
#     }

#     return render(request, "customer/list.html", context)


@transaction.atomic
def create(request, enquiry_pk=None):
    pass

    # ContactInlineFormSet = inlineformset_factory(
    #     Customer, Contact, form=ContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    # contact_formset = ContactInlineFormSet(request.POST or None)

    # customer_form = CustomerForm(request.POST or None)
    # if request.method == "POST":

    #     if customer_form.is_valid() and contact_formset.is_valid():
    #         customer = customer_form.save()

    #         contacts = contact_formset.save(commit=False)
    #         for contact in contacts:
    #             contact.customer = customer
    #             contact.save()

    #         return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    # context = {
    #     'customer_form': customer_form,
    #     'contact_formset': contact_formset,
    #     'form_url': reverse_lazy('customer:create'),
    #     'type': "Create"

    # }
    # return render(request, "customer/edit.html", context)


def detail(request, pk):
    contact = get_object_or_404(SupplierContact, id=pk)
    addresses = contact.supplier_addresses
    context = {
        'contact': contact,
        'addresses': addresses,
    }
    return render(request, "contacts/detail.html", context)


@transaction.atomic
def edit(request, pk=None):
    pass

    # customer = get_object_or_404(Customer, id=pk)

    # customer_form = CustomerForm(request.POST or None, instance=customer)

    # ContactInlineFormSet = inlineformset_factory(
    #     Customer, Contact, form=ContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    # contact_formset = ContactInlineFormSet(request.POST or None, instance=customer)

    # pprint.pprint(request.POST)
    # # print(contact_formset)
    # if request.method == "POST":

    #     if customer_form.is_valid() and contact_formset.is_valid():
    #         customer_form.save()

    #         contact_formset.save()

    #         return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    # context = {
    #     'customer_form': customer_form,
    #     'contact_formset': contact_formset,
    #     'form_url': reverse_lazy('customer:edit', args=[customer.pk]),
    #     'type': "Create"

    # }
    # return render(request, "customer/edit.html", context)


def delete(request, pk):
    if request.method == "POST":
        customer = get_object_or_404(Customer, id=pk)
        customer.delete()
    return HttpResponseRedirect(reverse('customer:list'))
