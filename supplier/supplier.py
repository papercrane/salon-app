from django.db import transaction
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from erp_core.pagination import paginate


from supplier.models import SupplierAddress, Supplier
from supplier.forms import SupplierContactAddressForm, SupplierForm
from supplier.filters import SupplierFilter
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages

@login_required
@permission_required('supplier.view_supplier_list', raise_exception=True)
def index(request):
    f = SupplierFilter(request.GET, queryset=Supplier.objects.all().order_by("-created_at"))
    # suppliers = Supplier.objects.all()

    suppliers, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
    context = {
        'suppliers': suppliers,
        # 'status': Customer.STATUSES,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "supplier/list.html", context)

@login_required
@permission_required('supplier.change_supplier', raise_exception=True)
@transaction.atomic
def create(request, enquiry_pk=None):

    # ContactInlineFormSet = inlineformset_factory(
    #     Supplier, SupplierContact, form=SupplierContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    # contact_formset = ContactInlineFormSet(request.POST or None)

    supplier_form = SupplierForm(request.POST or None)
    supplier_address_form = SupplierContactAddressForm(request.POST or None)

    if request.method == "POST":

        if supplier_form.is_valid() and supplier_address_form.is_valid():
            supplier = supplier_form.save()

            address = supplier_address_form.save(commit=False)
            address.supplier = supplier
            address.save()
            messages.add_message(request, messages.SUCCESS, "Supplier created successfully")
            return HttpResponseRedirect(reverse('supplier:detail', args=[supplier.pk]))

    context = {
        'supplier_form': supplier_form,
        'supplier_address_form': supplier_address_form,
        'form_url': reverse_lazy('supplier:create'),
        'type': "Create"

    }
    return render(request, "supplier/edit.html", context)

@login_required
@permission_required('supplier.view_supplier_list', raise_exception=True)
def detail(request, pk):
    supplier = get_object_or_404(Supplier, id=pk)
    address = supplier.supplier_addresses
    context = {
        'supplier': supplier,
        'address': address,

    }
    return render(request, "contacts/detail.html", context)


@login_required
@permission_required('supplier.change_supplier', raise_exception=True)
@transaction.atomic
def edit(request, pk=None):
    
    supplier = get_object_or_404(Supplier, id=pk)
    supplier_address = SupplierAddress.objects.filter(supplier=supplier)[0]
    supplier_form = SupplierForm(request.POST or None, instance=supplier)
    supplier_address_form = SupplierContactAddressForm(request.POST or None,instance=supplier_address)

    # ContactInlineFormSet = inlineformset_factory(
    #     Supplier, SupplierContact, form=SupplierContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    # contact_formset = ContactInlineFormSet(request.POST or None, instance=supplier)

    if request.method == "POST":

        if supplier_form.is_valid() and supplier_address_form.is_valid():
            supplier_form.save()
            supplier_address_form.save()
            messages.add_message(request, messages.SUCCESS, "Supplier edited successfully")
            return HttpResponseRedirect(reverse('supplier:detail', args=[supplier.id]))

    context = {
        'supplier_form': supplier_form,
        'supplier_address_form': supplier_address_form,
        'form_url': reverse_lazy('supplier:edit', args=[supplier.pk]),
        'type': "Edit",
        'supplier': supplier,

    }
    return render(request, "supplier/edit.html", context)

@login_required
@permission_required('supplier.delete_supplier', raise_exception=True)
def delete(request, pk):
    if request.method == "POST":
        supplier = get_object_or_404(Supplier, id=pk)
        supplier.delete()
    messages.add_message(request, messages.SUCCESS, "Supplier deleted successfully")
    return HttpResponseRedirect(reverse('supplier:list'))
