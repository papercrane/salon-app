import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from supplier.models import Supplier


class SupplierFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(label="code", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    name = django_filters.CharFilter(label="Supplier name", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    # contact = django_filters.CharFilter(label="Contact Name", method='filter_by_contact')

    # created_at = django_filters.DateTimeFromToRangeFilter(
    #     name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Supplier
        fields = ['code', 'name']

    # def filter_by_contact(self, queryset, name, value):
    #     return queryset.filter(
    #         Q(supplier_contacts__name__icontains=value) | Q(supplier_contacts__supplier__name__icontains=value)
    #     ).distinct()

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
