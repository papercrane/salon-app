from django.conf import settings
from django.db import models
from django.shortcuts import reverse
from address.models import Address as AbstractAddress, Contact as AbstractContact
from erp_core.models import ErpAbstractBaseModel, BaseModel
from django.db import transaction


class Supplier(BaseModel):
    code = models.CharField(max_length=100, unique=True, null=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    whatsup_no = models.CharField(max_length=14, null=True, blank=True)
    phone = models.CharField("Phone Number", max_length=20,null=True,blank=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    refered_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE, null=True, blank=True)

    # category = models.ManyToManyField('customer.Category')

    def __str__(self):
        name = self.name
        if self.code:
            name += "(" + str(self.code) + ")"
        return name

    @transaction.atomic
    def delete(self, force_delete=False, *args, **kwargs):
        self.supplier_addresses.delete()
        return super(BaseModel, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ("view_supplier_list", "Can view supplier list"),
            
        )


# class SupplierContact(AbstractContact):

#     GENDER = (
#         ('MALE', 'MALE'),
#         ('FEMALE', 'FEMALE'),
#     )

#     gender = models.CharField(max_length=20, choices=GENDER, default=GENDER[0][0])

#     name = models.CharField(max_length=255, null=True, blank=False)
#     email = models.EmailField(null=True, blank=True)
#     supplier = models.ForeignKey('supplier.Supplier', on_delete=models.CASCADE, related_name="supplier_contacts")
#     designation = models.CharField(max_length=100, default="Owner", null=True, blank=False)
#     dob = models.DateField(null=True, blank=True)

#     def __str__(self):
#         return str(self.name + '(' + self.designation + '), ' + self.supplier.name)

#     # label="Job Title"
#     @transaction.atomic
#     def delete(self, force_delete=False, *args, **kwargs):
#         self.supplier_addresses.delete()
#         return super(BaseModel, self).delete(*args, **kwargs)

#     def get_absolute_url(self):
#         return reverse("supplier:supplier-contact-detail", args=[self.id])


class SupplierAddress(AbstractAddress):
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE,
                                null=True, blank=False, related_name="supplier_addresses")
    billing = models.BooleanField(default=False)

    def __str__(self):
        address_fields = ['addressline1', 'addressline2', 'area', 'country', 'state', 'city']

        address_strings = []
        for field in address_fields:
            if hasattr(self, field):
                if getattr(self, field, None):
                    address_strings.append(str(getattr(self, field)))
        connector = " ,"
        return connector.join(address_strings)
        # "%s, %s, %s, %s, %s, %s" % (self.addressline1, self.addressline2, self.area, self.country, self.state, self.city)

    def save(self, *args, **kwargs):
        if self.billing is True:
            self.supplier.supplier_addresses.update(billing=False)
        return super(SupplierAddress, self).save(*args, **kwargs)
