from django import forms
from django.forms import ModelForm
from supplier.models import Supplier, SupplierAddress


class SupplierForm(ModelForm):

    class Meta:
        model = Supplier
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "email": forms.TextInput(attrs={'class': "form-control"}),
            "phone": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "whatsup_no": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['name', 'email', 'phone', 'whatsup_no']
        help_texts = {
            'phone': 'Phone number with country prefix.',
        }


# class SupplierContactForm(ModelForm):

#     class Meta:
#         model = SupplierContact
#         widgets = {
#             "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
#             "email": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
#             "phone": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
#             "alternate_phone": forms.TextInput(attrs={'class': "form-control"}),
#             "designation": forms.TextInput(attrs={'class': "form-control"}),
#             "dob": forms.DateInput(attrs={'class': "form-control tdate", 'id': "dob"}),
#             "gender": forms.Select(attrs={'class': "form-control"}),
#         }
#         fields = ['name', 'gender', 'email', 'designation', 'phone', 'alternate_phone', 'dob']


class SupplierContactAddressForm(ModelForm):

    class Meta:
        model = SupplierAddress
        widgets = {
            "addressline1": forms.TextInput(attrs={'class': "form-control", "id": "addressline1"}),
            "addressline2": forms.TextInput(attrs={'class': "form-control", "id": "addressline2"}),
            "area": forms.TextInput(attrs={'class': "form-control", "id": "addressline2"}),
            "zipcode": forms.NumberInput(attrs={'class': "form-control", "id": "zipcode", 'min': 0}),
            "country": forms.Select(attrs={'class': "form-control", 'id': "country"}),
            "state": forms.Select(attrs={'class': "form-control chained", 'id': "state"}),
            "city": forms.Select(attrs={'class': "form-control chained ", 'id': "city"}),
            "billing": forms.CheckboxInput(attrs={'id': "billing"}),
        }
        fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'billing', 'country', 'state', 'city']

        # exclude = ['supplier']
