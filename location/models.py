import base64

import requests
from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from django.core.validators import MinValueValidator
from erp_core.models import ErpAbstractBaseModel


class Country(models.Model):
    name = models.CharField(max_length=100, unique=True)

    # zone = models.ForeignKey(Zone, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=100, unique=True)

    # zone = models.ForeignKey(Zone, on_delete=models.CASCADE)

    country = models.ForeignKey(Country, on_delete=models.CASCADE, default=True, related_name="states")

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=100, unique=True)

    # zone = models.ForeignKey(Zone, on_delete=models.CASCADE)

    state = models.ForeignKey(State, on_delete=models.CASCADE, default=True, related_name="cities")

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=100, unique=True)
    area = models.CharField(max_length=100)

    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    state = ChainedForeignKey(State,
                              chained_field="country",
                              chained_model_field="country",
                              show_all=False,
                              auto_choose=True,
                              sort=True, on_delete=models.CASCADE)

    city = ChainedForeignKey(City, chained_field="state", chained_model_field="state", on_delete=models.CASCADE)
    trn_no = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name + ' ' + self.area


class Office(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, unique=True)
    buildingno = models.TextField(blank=True, null=True, verbose_name="Address")
    phone_number = models.CharField(max_length=20,blank=True, null=True,)
    alternate_phone = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=20, null=True, blank=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    logo = models.FileField(upload_to='logo/%Y/%m/%d', null=True)
    timing = models.TextField(null=True, blank=True)
    website = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_logobase64(self):
        return base64.b64encode(requests.get(self.logo.url).content)
