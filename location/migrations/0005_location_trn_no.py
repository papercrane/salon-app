# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2019-03-08 16:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0004_office_timing'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='trn_no',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
