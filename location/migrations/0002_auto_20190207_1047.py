# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2019-02-07 10:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='office',
            name='logo',
            field=models.FileField(null=True, upload_to='logo/%Y/%m/%d'),
        ),
        migrations.AlterField(
            model_name='location',
            name='name',
            field=models.CharField(default=1, max_length=100, unique=True),
            preserve_default=False,
        ),
    ]
