from django import forms
from django.forms import ModelForm
from location.models import *

class LocationForm(ModelForm):

    class Meta:
        model = Location
        widgets = {
            "country": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "state": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "city": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "area": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['country', 'state', 'city','area']
