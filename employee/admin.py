from django.contrib import admin
from .models import *
from guardian.admin import GuardedModelAdmin


class EmployeeAdmin(GuardedModelAdmin):
    pass


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(AddressForEmployee)
admin.site.register(ContactForEmployee)
