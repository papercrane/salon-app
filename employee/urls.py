from django.conf.urls import include, url
from employee import views

app_name = "employee"



urlpatterns = [
    url(r'^employee/', include([
        url(r'^$', views.index, name="list"),
        url(r'^select_list/$', views.select2_list, name="select_list"),
        url(r'^select_employee/$', views.select_employee, name="select_employee"),
        url(r'^select_designation/$', views.select_designation, name="select_designation"),
        url(r'^create/$', views.create, name='create'),
        url(r'^(?P<pk>[^/]+)/edit/', views.edit, name='edit'),
        url(r'^(?P<pk>[^/]+)/delete/', views.delete, name='delete'),
        url(r'^(?P<pk>[^/]+)/profile/', views.profile, name='profile'),
        url(r'^findoffice/', views.select_office, name="select_office"),
        url(r'^permissions/(?P<pk>[^/]+)/$', views.employee_permission, name='employee_permission'),
        url(r'^reports/salesbyemployee/$', views.sales_by_employee, name='sales_by_employee'),
        url(r'^reports/salesbyemployee/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', views.sales_by_employee, name="sales_by_employee"),
    ])),

]
