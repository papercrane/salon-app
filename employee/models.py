from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from address.models import *
from company.models import *
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator
from location.models import Office


class AddressForEmployee(Address):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_address")
    landmark = models.CharField(max_length=255, blank=True, null=True)


class ContactForEmployee(Contact):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_contact")


class Employee(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    MARITALSTATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )

    BLOODGROUP = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    )

    LICENSE = 'License'
    PASSPORT = 'Passport'
    IDPROOF = (
        (LICENSE, 'License'),
        (PASSPORT, 'Passport')
    )

    code = models.CharField(max_length=255, unique=True)
    photo = models.FileField(upload_to='prof/%Y/%m/%d', blank=True, null=True)
    dob = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=8, choices=SEX)
    status = models.BooleanField(default=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)

    primary_designation = models.ForeignKey(
        Designation, on_delete=models.CASCADE, null=True, blank=True)

    # secondary_designation = models.ManyToManyField(
    #     Designation, related_name="employee_secondary_designation", blank=True)

    reporting_officer = models.ForeignKey(
        'employee.Employee', on_delete=models.CASCADE, blank=True, null=True)

    id_proof_type = models.CharField(max_length=100, choices=IDPROOF, default=LICENSE, null=True, blank=True)
    id_proof_no = models.CharField(max_length=200, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True, validators=[MinValueValidator(0)])
    joining_date = models.DateField(null=True, blank=True)
    salary_date = models.DateField(null=True, blank=True)
    office = models.ForeignKey(Office, null=True, blank=False)

    class Meta:
        permissions = (
            ("view_employee_list", "Can view employee list"),
            ("view_employee_details", "Can view employee details"),
            ("can_access", "Can access Employee Section"),
            ("reporting_officier", "I am a reporting officier"),
            ("is_supervisor", "I am a supervisor"),
            ("view_report", "View report")
        )

    def get_absolute_url(self):
        return reverse('employee:profile', args=[self.code])

    def __str__(self):
        return str(self.user.first_name) + " (" + str(self.code) + ")"


class EmployeeCode(ErpAbstractBaseModel):
    character_set = models.CharField(max_length=100)


