import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from employee.models import Employee
from company.models import Designation


class EmployeeFilter(django_filters.FilterSet):

    code = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(lookup_expr='icontains', label="Name Contains", name="user__first_name",
                                                 widget=forms.TextInput(attrs={'class': 'form-control'}))

    designation = django_filters.ModelChoiceFilter(
        queryset=Designation.objects.all(),
        name="primary_designation",
        label="Select Designation",
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Employee
        fields = ['code']
