from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q, functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from guardian.shortcuts import get_objects_for_user, assign_perm
from django.contrib import messages

from erp_core.pagination import paginate, cust_paginate
from .forms import *
from employee.filters import EmployeeFilter
from django.db.models import Sum, Count, F
from django.utils import timezone
from invoice.models import Invoice, InvoiceItem


@login_required
@permission_required('employee.can_access', raise_exception=True)
def index(request):
    f = EmployeeFilter(request.GET, queryset=get_objects_for_user(request.user, 'employee.view_employee_details',accept_global_perms=False))
    employees, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    colors = ['.active','.success','.info','.warning','.danger']
    context = {
        'employees': employees,
        'pagination': pagination,
        'filter': f,
        'colors':colors,

    }
    return render(request, "employee/index.html", context)


@login_required
def select2_list(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            Q(user__username__icontains=employee_name) | Q(user__first_name__icontains=employee_name)
        ).annotate(
            text=functions.Concat('user__username', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)

@login_required
def select_employee(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            user__first_name__contains=employee_name
        ).annotate(
            text=functions.Concat('user__first_name', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)

@login_required
def select_designation(request):
    office_name = request.GET.get('office_name', False)
    if office_name:
        office = Office.objects.get(name=office_name)
        designations = Designation.objects.filter(office=office
                                                  ).annotate(
            text=('name')
        ).values('id', 'text')
        return JsonResponse(list(designations), safe=False)
    return JsonResponse(list(), safe=False)

@login_required
def select_office(request):
    offices = list(Office.objects.filter(name__contains=request.GET.get("office")).values('id', 'name').distinct())
    if request.is_ajax():
        return JsonResponse(offices, safe=False)


@login_required
@permission_required('employee.change_employee', raise_exception=True)
def create(request):
    form1 = UserForm(request.POST or None)
    form2 = EmployeeForm(request.POST or None, request.FILES or None)
    form3 = AddressForEmployeeForm(request.POST or None)
    form4 = ContactForEmployeeForm(request.POST or None)

    if request.method == "POST":

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            if employee.reporting_officer:
                # timesheet = TimeSheet.objects.update_or_create(employee=employee, approved_by=employee.reporting_officer)
                permission = Permission.objects.get(codename="reporting_officier")
                employee.reporting_officer.user.user_permissions.add(permission)
            if employee.primary_designation:
                for pr in employee.primary_designation.permissions.all():
                    employee.user.user_permissions.add(pr)

            messages.add_message(request, messages.INFO, 'Successfully Created')

            return HttpResponseRedirect(reverse('employee:profile', args=[employee.code]))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form_url': reverse_lazy('employee:create')

    }

    return render(request, 'employee/create.html', context)



@login_required
def edit(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    photo = employee.photo

    if request.method == "POST":
        form1 = UserEditForm(request.POST, instance=employee.user)
        form2 = EmployeeForm(request.POST, request.FILES, instance=employee)
        form3 = AddressForEmployeeForm(request.POST, instance=employee.employee_address)
        form4 = ContactForEmployeeForm(request.POST, instance=employee.employee_contact)

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():

            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            if employee.photo == "":
                employee.photo = photo
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            messages.add_message(request, messages.SUCCESS, "Employer edited successfully")
            return HttpResponseRedirect(reverse('employee:profile', args=[employee.code]))

    else:

        form1 = UserEditForm(instance=employee.user)
        form2 = EmployeeForm(instance=employee)
        if employee.employee_address:
            form3 = AddressForEmployeeForm(instance=employee.employee_address)
        if employee.employee_contact:
            form4 = ContactForEmployeeForm(instance=employee.employee_contact)

    context = {

        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'employee':employee,
        'form_url': reverse_lazy('employee:edit', args=[employee.code])

    }

    return render(request, 'employee/create.html', context)


@login_required
@permission_required('employee.delete_employee', raise_exception=True)
def delete(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    employee.user.delete()
    employee.delete()
    messages.add_message(request, messages.SUCCESS, "Employer deleted successfully")
    return HttpResponseRedirect(reverse('employee:list'))


@login_required
@permission_required('employee.can_access', raise_exception=True)
def profile(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    return render(request, 'employee/profile.html', {'employee': employee})


@login_required
def employee_permission(request, pk):
    can_access = Permission.objects.get(codename="can_access")
    change_employee = Permission.objects.get(codename="change_employee")
    delete_employee = Permission.objects.get(codename="delete_employee")
    view_customer_list = Permission.objects.get(codename="view_customer_list")
    change_customer = Permission.objects.get(codename="change_customer")
    delete_customer = Permission.objects.get(codename="delete_customer")
    view_supplier_list = Permission.objects.get(codename="view_supplier_list")
    change_supplier = Permission.objects.get(codename="change_supplier")
    delete_supplier = Permission.objects.get(codename="delete_supplier")
    view_product_list = Permission.objects.get(codename="view_product_list")
    change_product = Permission.objects.get(codename="change_product")
    delete_product = Permission.objects.get(codename="delete_product")
    view_service_list = Permission.objects.get(codename="view_service_list")
    change_service = Permission.objects.get(codename="change_service")
    delete_service = Permission.objects.get(codename="delete_service")
    view_appointment_list = Permission.objects.get(codename="view_appointment_list")
    change_appointment = Permission.objects.get(codename="change_appointment")
    delete_appointment = Permission.objects.get(codename="delete_appointment")
    view_invoice_list = Permission.objects.get(codename="view_invoice_list")
    change_invoice = Permission.objects.get(codename="change_invoice")
    delete_invoice = Permission.objects.get(codename="delete_invoice")
    view_expense_list = Permission.objects.get(codename="view_expense_list")
    change_expense = Permission.objects.get(codename="change_expense")
    delete_expense = Permission.objects.get(codename="delete_expense")
    view_discount_list = Permission.objects.get(codename="view_discount_list")
    view_report = Permission.objects.get(codename="view_report")

    employee = get_object_or_404(Employee, pk=pk)
    if employee.user.has_perm('can_access'):
        emp_acc = True
    else:
        emp_acc = False

    if request.method == "POST":
        employee_permission = request.POST.get('permission')
        assign_perm('view_task', employee.user, employee)
        permission = Permission.objects.get(codename=employee_permission)
        # if permission in employee.user.user_permissions.all():
        #     employee.user.user_permissions.remove(permission)
        # else:
        #     employee.user.user_permissions.add(permission)
    # import guardian.templatetags.guardian_tags.get_obj_perms
    context = {
        'user': employee.user,
        'employee':employee,
        'emp_acc': emp_acc,
        'can_access': can_access,
        'change_employee': change_employee,
        'delete_employee': delete_employee,
        'view_customer_list': view_customer_list,
        'change_customer': change_customer,
        'delete_customer': delete_customer,
        'view_supplier_list': view_supplier_list,
        'change_supplier': change_supplier,
        'delete_supplier': delete_supplier,
        'view_product_list': view_product_list,
        'change_product': change_product,
        'delete_product': delete_product,
        'view_service_list': view_service_list,
        'change_service': change_service,
        'delete_service': delete_service,
        'view_appointment_list': view_appointment_list,
        'change_appointment': change_appointment,
        'delete_appointment': delete_appointment,
        'view_invoice_list': view_invoice_list,
        'change_invoice': change_invoice,
        'delete_invoice': delete_invoice,
        'view_expense_list': view_expense_list,
        'change_expense': change_expense,
        'delete_expense': delete_expense,
        'view_discount_list': view_discount_list,
        'view_report': view_report,

    }

    return render(request, "employee/permission.html", context)


@login_required
@permission_required('employee.view_report', raise_exception=True)
def sales_by_employee(request, from_date=None, to_date=None):
    if not from_date:
        from_date = timezone.now().date()
        to_date = timezone.now().date()
    invoices = InvoiceItem.objects.filter(created_at__date__range=[from_date, to_date]).values('employee__user__first_name','services__name').annotate(tp=Count('content_type'))
    totalservice = InvoiceItem.objects.filter(created_at__date__range=[from_date, to_date]).values('employee__user__first_name').annotate(tp=Count('content_type'))
    context = {
        'from_date': from_date,
        'to_date': to_date,
        'invoices': invoices,
        'totalservice':totalservice,
    }
    return render(request, "employee/sales_by_employee.html", context)
