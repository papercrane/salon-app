from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.forms import ModelForm
from employee.models import *


class UserForm(UserCreationForm):
    class Meta:
        model = User
        widgets = {"username": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "email": forms.TextInput(attrs={'class': "form-control", 'type': "email", 'required': "required"}),
                   "first_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "password1": forms.TextInput(
                       attrs={'class': "form-control", 'id': "exampleInputPassword1", 'placeholder': "Password"}),
                   "password2": forms.TextInput(
                       attrs={'class': "form-control", 'required': "required", 'placeholder': "Password"}),
                   }
        fields = ['username', 'email', 'first_name', 'password1', 'password1']


class UserEditForm(UserChangeForm):
    class Meta:
        model = User
        widgets = {"username": forms.TextInput(attrs={'class': "form-control"}),
                   "email": forms.TextInput(attrs={'class': "form-control", 'type': "email"}),
                   "first_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "password": forms.TextInput(attrs={'class': "form-control"}),
                   }
        fields = ['username', 'email', 'first_name', 'password']


class EmployeeForm(ModelForm):
    reporting_officer = forms.ModelChoiceField(
        required=False,
        label="Reports To",
        queryset=Employee.objects.none(),
        widget=forms.Select(attrs={'class': 'form-control', 'width': "100%"}),
    )

    class Meta:
        model = Employee
        widgets = {"code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
                   "dob": forms.DateInput(attrs={'class': "form-control", "id": "pldate"}),
                   "gender": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),
                   "photo": forms.FileInput(attrs={'class': "form-control"}),
                   "primary_designation": forms.Select(
                       attrs={'class': "form-control", "data-toggle": "select", 'required': "required"}),
                   # "secondary_designation": forms.SelectMultiple(attrs={'class': "form-control", "data-toggle": "select", "id": "secondary_desig", 'multiple': "TRUE"}),
                   "reporting_officer": forms.SelectMultiple(
                       attrs={'class': "form-control", "data-toggle": "select", 'multiple': "TRUE"}),
                   "id_proof_type": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),
                   "id_proof_no": forms.TextInput(
                       attrs={'class': "form-control", 'required': "required", 'id': "id_proof_no"}),
                   "joining_date": forms.DateInput(
                       attrs={'class': "form-control", "id": "joining_date", 'required': "required"}),
                   "salary_date": forms.DateInput(
                       attrs={'class': "form-control", "id": "salary_date", 'required': "required"}),
                   "age": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
                   "office": forms.Select(
                       attrs={'class': "form-control", "data-toggle": "select", 'required': "required"}),

                   }
        fields = ['code', 'dob', 'gender', 'photo', 'primary_designation', 'reporting_officer', 'id_proof_type',
                  'id_proof_no', 'joining_date', 'salary_date', 'age', 'office'
                  ]

    def __init__(self, *args, **kwargs):
        # manager_qs = kwargs.pop('manager_qs')
        super(EmployeeForm, self).__init__(*args, **kwargs)
        self.set_reporting_officer_queryset()

    def set_reporting_officer_queryset(self):
        # print(self.data)
        if self.instance.id:
            self.fields['reporting_officer'].queryset = Employee.objects.filter(id=self.instance.reporting_officer_id)
        if self.data:
            if self.data['reporting_officer']:
                self.fields['reporting_officer'].queryset = Employee.objects.filter(id=self.data['reporting_officer'])


class UserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name')


class AddressForEmployeeForm(ModelForm):
    class Meta:
        model = AddressForEmployee

        widgets = {"addressline1": forms.TextInput(attrs={'class': "form-control", }),
                   "addressline2": forms.TextInput(attrs={'class': "form-control"}),
                   # "area": forms.Select(attrs={'class': "form-control", }),
                   "zipcode": forms.NumberInput(attrs={'class': "form-control", 'min': 0}),
                   # "city": forms.{Select(attrs='class': "chained form-control", }),
                   # "state": forms.Select(attrs={'class': "chained form-control", }),
                   # "country": forms.Select(attrs={'class': "chained form-control",}),
                   "landmark": forms.TextInput(attrs={'class': "form-control", }),

                   }
        fields = ['addressline1', 'addressline2', 'zipcode', 'city', 'state', 'country', 'landmark']


class ContactForEmployeeForm(ModelForm):
    class Meta:
        model = ContactForEmployee

        widgets = {"phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required", 'min': 0}),
                   # "alternate_phone": forms.NumberInput(attrs={'class': "form-control", }),
                   }
        fields = ['phone']
        help_texts = {
            'phone': 'Phone number with country prefix.',
        }
