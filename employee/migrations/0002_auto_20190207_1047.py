# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2019-02-07 10:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='staffinsurance',
            name='employee',
        ),
        migrations.DeleteModel(
            name='StaffInsurance',
        ),
    ]
