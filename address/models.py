import uuid

from django.db import models
from smart_selects.db_fields import ChainedForeignKey

from erp_core.models import ErpAbstractBaseModel, BaseModel


class Area(models.Model):
    name = models.CharField("Area", max_length=200)



class Address(BaseModel):

    addressline1 = models.CharField("Addressline 1", max_length=200)
    addressline2 = models.CharField("Addressline 2", max_length=200)
    area = models.CharField(max_length=100, null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)

    country = models.ForeignKey('location.Country', on_delete=models.CASCADE,default="UAE")

    state = models.ForeignKey('location.State',on_delete=models.CASCADE,default="Dubai")

    city =  models.ForeignKey('location.City',on_delete=models.CASCADE,default="Dubai")


class Contact(BaseModel):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    phone = models.CharField("Phone Number", max_length=20)
    # alternate_phone = models.CharField("Alternate Phone Number", max_length=20, null=True, blank=True)
