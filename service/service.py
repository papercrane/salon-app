from decimal import Decimal

from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from service.forms import ServiceForm, CodeSettingsForm, ServiceTypeForm
from django.shortcuts import get_object_or_404, render, redirect
from erp_core.pagination import paginate
from service.models import Service, CodeSetting, ServiceType
from django.http import HttpResponseRedirect, JsonResponse
from service.filters import ServiceFilter
from django.db import transaction
from django.forms import inlineformset_factory
from django.utils import timezone
from invoice.models import InvoiceItem
from django.db.models import Sum, F
from django.contrib import messages

@login_required
def get_price(request):
    if request.GET.get('prev'):
        pre_service = get_object_or_404(Service, pk=request.GET.get('prev'))
        if request.GET.get('current'):
            print("prev & curr")
            print("pre",pre_service)
            currnt_service = get_object_or_404(Service, pk=request.GET.get('current'))
            print("cuttnt",currnt_service)
            if currnt_service.price_hour < Decimal(request.GET.get('minimum_purchase_amount')):
                message = "minimum amount of service selected should be " + request.GET.get('minimum_purchase_amount')
                data = {'status': 'false', 'message': message}
                return JsonResponse(data, status=500)
            print("currnt inpt", Decimal(request.GET.get('cp')), "pre", pre_service.price_hour, "currnt",
                  currnt_service.price_hour)
            cp = Decimal(request.GET.get('cp')) - pre_service.price_hour + currnt_service.price_hour
        else:
            print("pre only")
            print("input" + request.GET.get('cp'), "prev", pre_service.price_hour)
            cp = Decimal(request.GET.get('cp')) - pre_service.price_hour
    else:
        if request.GET.get("id"):
            service = get_object_or_404(Service, pk=request.GET.get("id"))
            if service.price_hour < Decimal(request.GET.get('minimum_purchase_amount')):
                message = "minimum amount  of service selected should be" + request.GET.get('minimum_purchase_amount')
                data = {'status': 'false', 'message': message}
                return JsonResponse(data, status=500)
            print("no prev")
            print("input", Decimal(request.GET.get('cp')), "currnt", service.price_hour)
            cp = Decimal(request.GET.get('cp')) + service.price_hour

        else:
            print("no serv,default")
            cp = 0
            print(cp)

    data = {'cp': cp}
    print("last",cp)
    return JsonResponse(data)


@login_required
def dashboard(request):
    return render(request, "service/dashboard.html")


@login_required
@permission_required('service.view_service_list', raise_exception=True)
def index(request):
    f = ServiceFilter(request.GET, queryset=ServiceType.objects.all().order_by("-created_at"))

    services, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
    context = {
        'services': services,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "service/index.html", context)


# @login_required
# def create(request):
#     serviceform = ServiceForm(request.POST or None)

#     if request.method == "POST":

#         if serviceform.is_valid():

#             service = serviceform.save(commit=False)
#             service.save()
#             serviceform.save_m2m()

#             return HttpResponseRedirect(reverse('service:detail', args=[service.id]))

#     context = {
#         'serviceform': serviceform,
#         'form_url': reverse_lazy('service:create'),
#         'type': "add",

#     }

#     return render(request, "service/edit.html", context)


@login_required
@permission_required('service.change_service', raise_exception=True)
def create(request):
    service_type_form = ServiceTypeForm(request.POST or None)
    ServiceInlineFormSet = inlineformset_factory(ServiceType, Service, form=ServiceForm, extra=0, can_delete=True,
                                                 min_num=1, validate_min=True)
    service_formset = ServiceInlineFormSet(request.POST or None)

    if request.method == "POST":

        if service_type_form.is_valid() and service_formset.is_valid():

            service_type = service_type_form.save()
            services = service_formset.save(commit=False)
            for service in services:
                service.service_type = service_type
                service.save()
            messages.add_message(request, messages.SUCCESS, "Service created successfully")
            return HttpResponseRedirect(reverse('service:detail', args=[service_type.id]))

    context = {
        'service_type_form': service_type_form,
        'service_formset': service_formset,
        # 'service_type':service_type,
        'form_url': reverse_lazy('service:create'),
        'type': "add",

    }

    return render(request, "service/service_create.html", context)


@login_required
@permission_required('service.change_service', raise_exception=True)
def edit(request, pk):
    service_type = get_object_or_404(ServiceType, pk=pk)
    service_type_form = ServiceTypeForm(request.POST or None, instance=service_type)
    ServiceInlineFormSet = inlineformset_factory(ServiceType, Service, form=ServiceForm, extra=0, can_delete=True,
                                                 min_num=1, validate_min=True)
    service_formset = ServiceInlineFormSet(request.POST or None, instance=service_type)

    if request.method == "POST":

        if service_type_form.is_valid() and service_formset.is_valid():
            service_type = service_type_form.save()
            services = service_formset.save()
            messages.add_message(request, messages.SUCCESS, "Service edited successfully")
            return HttpResponseRedirect(reverse('service:detail', args=[service_type.id]))

    context = {
        'service_type_form': service_type_form,
        'service_formset': service_formset,
        'service_type': service_type,
        'form_url': reverse_lazy('service:edit', args=[service_type.id]),
        'type': "edit",

    }
    return render(request, "service/service_create.html", context)


@login_required
@permission_required('service.view_service_list', raise_exception=True)
def detail(request, pk):
    service_type = get_object_or_404(ServiceType, pk=pk)

    context = {
        'service_type': service_type,
    }

    return render(request, "service/detail.html", context)


@login_required
@permission_required('service.delete_service', raise_exception=True)
def delete(request, pk):
    if request.method == 'POST':
        service = get_object_or_404(ServiceType, pk=pk)
        service.delete()
        messages.add_message(request, messages.SUCCESS, "Service deleted successfully")
        return HttpResponseRedirect(reverse('service:list'))


@login_required
@transaction.atomic
def create_checklist(request, service):
    service = get_object_or_404(Service, pk=service)
    ChecklistInlineFormSet = inlineformset_factory(
        Service, ServiceChecklist, form=ChecklistForm, extra=0, min_num=1, can_delete=True, validate_min=True)
    checklist_formset = ChecklistInlineFormSet(request.POST or None, instance=service)

    if request.method == "POST":
        if checklist_formset.is_valid():
            checklists = checklist_formset.save(commit=False)
            for checklist in checklists:
                checklist.service = service
                checklist.save()
            return HttpResponseRedirect(reverse('service:view_checklist', args=[service.pk]))
    context = {
        'checklist_formset': checklist_formset,
        'type': 'add',
        'form_url': reverse_lazy('service:create_checklist', args=[service.pk]),
    }
    return render(request, "checklist/create.html", context)


@login_required
def view_checklist(request, service):
    checklist_entries = ServiceChecklist.objects.filter(service_id=service)
    context = {
        'checklist_entries': checklist_entries,
        'service': service,
    }
    return render(request, "checklist/checklist.html", context)


@login_required
def delete_checklist(request, service):
    checklist_entries = ServiceChecklist.objects.filter(service_id=service)
    checklist_entries.delete()
    return HttpResponseRedirect(reverse('service:list'))


@login_required
def add_options(request, entry):
    entry = get_object_or_404(ServiceChecklist, pk=entry)
    OptionInlineFormSet = inlineformset_factory(
        ServiceChecklist, ChecklistOptions, form=ChecklistOptionsForm, extra=0, can_delete=True, min_num=2)
    if entry.option_set:
        option_formset = OptionInlineFormSet(request.POST or None, instance=entry)
    else:
        option_formset = OptionInlineFormSet(request.POST or None)

    if request.method == "POST":
        if entry.field == ServiceChecklist.OPTIONS:
            if option_formset.is_valid():
                options = option_formset.save(commit=False)
                for option in options:
                    option.query = entry
                    option.save()
        return HttpResponseRedirect(reverse('service:view_checklist', args=[entry.service_id]))
    context = {
        'option_formset': option_formset,
        'form_url': reverse_lazy('service:add_options', args=[entry.pk]),
    }
    return render(request, "checklist/edit_options.html", context)


def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            messages.add_message(request, messages.SUCCESS, "Successfully created code format")
            return redirect(reverse('service:create'))

    context = {
        'code_form': code_form,
    }

    return render(request, "service/code_setting.html", context)


@login_required
@permission_required('employee.view_report', raise_exception=True)
def sales_by_service(request, from_date=None, to_date=None):
    if not from_date:
        from_date = timezone.now().date()
        to_date = timezone.now().date()
    services = InvoiceItem.objects.filter(invoice__created_at__date__range=[from_date, to_date],
                                          content_type__model='service').values('services__name').annotate(
        quantity=Sum('hours_or_sqfeet'), total_cp=Sum('total_price'))
    context = {
        'from_date': from_date,
        'to_date': to_date,
        'services': services
    }
    return render(request, "service/sales_by_service.html", context)
