from django.conf.urls import include, url

from service import service, service_type

app_name = "service"

service_type_patterns = ([
    url(r'^asset_type/', include([

        url(r'^create/$', service_type.create, name="create"),

    ])),

], 'service_type')


urlpatterns = [
    url(r'^get_price/$', service.get_price),

    url(r'^service/', include([
        url(r'^index/$', service.index, name="list"),
        url(r'^dashboard/$', service.dashboard, name="dashboard"),
        url(r'^create/$', service.create, name="create"),
        url(r'^(?P<pk>[^/]+)/edit/$', service.edit, name="edit"),
        url(r'^(?P<pk>[^/]+)/detail/$', service.detail, name="detail"),
        url(r'^(?P<pk>[^/]+)/delete/$', service.delete, name="delete"),
        url(r'^code/settings/$', service.code_settings, name='code_settings'),
        url(r'^(?P<service>[^/]+)/create/checklist/$', service.create_checklist, name="create_checklist"),
        url(r'^(?P<service>[^/]+)/checklist$', service.view_checklist, name="view_checklist"),
        url(r'^(?P<service>[^/]+)/checklist/delete$', service.delete_checklist, name="delete_checklist"),
        url(r'^(?P<entry>[^/]+)/add/option$', service.add_options, name="add_options"),
        
    ])),
    url(r'^service/', include(service_type_patterns)),
    url(r'^reports/salesbyservice/$', service.sales_by_service, name='sales_by_service'),
    url(r'^reports/salesbyservice/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', service.sales_by_service, name="sales_by_service"),

        




]
