from django import forms
from django.forms import ModelForm
from .models import Service, ServiceType, CodeSetting


class ServiceTypeForm(ModelForm):
    class Meta:
        model = ServiceType
        fields = ('name',)
        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control1", 'placeholder': "E.g. : Hair Care"}),
        }


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


class ServiceForm(ModelForm):
    class Meta:
        model = Service
        widgets = {

            "name": forms.TextInput(attrs={'id': "name", 'class': "form-control", 'required': "required",
                                           'placeholder': "E.g. : Hair wash,U Cut"}),
            "price_hour": forms.NumberInput(attrs={'id': "price_hour", 'class': "form-control", 'min': 0}),

        }

        fields = ['name', 'price_hour']
