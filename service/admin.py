from django.contrib import admin
from service.models import Service, ServiceType
# , ServiceChecklist, ChecklistOptions

admin.site.register(Service)
admin.site.register(ServiceType)
# admin.site.register(ServiceChecklist)
# admin.site.register(ChecklistOptions)


# Register your models here.
