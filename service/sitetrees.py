from sitetree.utils import item, tree

sitetrees = (
    tree('sidebar', items=[
        item(
            'Service',
            'service:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="wrench",

        ),
    ]),
)
