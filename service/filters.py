import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from service.models import Service, ServiceType


class ServiceFilter(django_filters.FilterSet):
    # code = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))
    service = django_filters.CharFilter(
        lookup_expr='icontains',
        name="services__name",
        label="Name",
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    service_type = django_filters.ModelChoiceFilter(
        queryset=ServiceType.objects.all(),
        # method='filter_by_city',
        name="name",
        label="Service Type",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", })
    )

    # created_at = django_filters.DateTimeFromToRangeFilter(name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_created_at = django_filters.ChoiceFilter(
        label="Created date",
        name="created_at",
        method='order_by_field',
        choices=(('created_at', 'Ascending'), ('-created_at', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    # fixed_price = django_filters.BooleanFilter(
    #     name="fixed_price",
    #     widget=widgets.BooleanWidget(attrs={'class': 'form-control'})
    # )

    class Meta:
        model = ServiceType
        fields = ['created_at',]

    # def filter_by_contact(self, queryset, name, value):
    #     return queryset.filter(
    #         Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
    #     )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
