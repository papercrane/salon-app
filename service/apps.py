from django.apps import AppConfig


class ServiceConfig(AppConfig):
    name = 'service'

    def ready(self):
        names = ['Cleaning', 'Maintenance', 'Repairing']

        from .models import ServiceType
        for name in names:
            ServiceType.objects.update_or_create(name=name)
