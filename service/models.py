from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
from invoice.models import InvoiceItem
from product.models import Product


class ServiceType(ErpAbstractBaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)


class Service(ErpAbstractBaseModel):
    def newCode():
        return CodeSetting.new_code()

    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, null=True)
    service_type = models.ForeignKey(ServiceType, on_delete=models.CASCADE, related_name="services")
    is_price_hour = models.BooleanField(default=True)
    price_hour = models.DecimalField(max_digits=20, decimal_places=2, null=True)
    consumables = models.ManyToManyField(Product, related_name="tools", blank=True)
    invoice_services = GenericRelation(InvoiceItem,related_query_name='services')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()

        return super(Service, self).save(*args, **kwargs)

    class Meta:
        permissions = (
            ("view_service_list", "Can view service list"),
            
        )


class CodeSetting(BaseCodeSetting):
    pass
