from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from service.forms import ServiceTypeForm
from django.shortcuts import get_object_or_404, render
from location.models import *
from django.contrib import messages
from erp_core.pagination import paginate
from django.http import HttpResponseRedirect

from service.models import ServiceType
# from asset.filters import AssetFilter


# @login_required
# def index(request):
#     # print(request.GET)

#     f = AssetFilter(request.GET, queryset=Asset.objects.all().order_by("-created_at"))
#     # assets = Asset.objects.all()

#     assets, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
#     context = {
#         'assets': assets,
#         # 'status': Customer.STATUSES,
#         'pagination': pagination,

#         'filter': f,
#     }

#     return render(request, "asset/index.html", context)


@login_required
def create(request):
    servicetypeform = ServiceTypeForm(request.POST or None)

    if request.method == "POST":

        if servicetypeform.is_valid():
            service_type = servicetypeform.save()

            return HttpResponseRedirect(reverse('service:create'))

    context = {
        'servicetypeform': servicetypeform,
        'form_url': reverse_lazy('service:service_type:create'),
        'type': "add",

    }

    return render(request, "service_type/create.html", context)


# @login_required
# def edit(request, pk):
#     asset = get_object_or_404(Asset, pk=pk)
#     assetform = AssetForm(request.POST or None, instance=asset)
#     attributeform = AttributeForm(request.POST or None, instance=asset.attribute)

#     if request.method == "POST":

#         if assetform.is_valid() and attributeform.is_valid():
#             attribute = attributeform.save()
#             asset = assetform.save(commit=False)
#             asset.attribute = attribute
#             asset.save()
#             return HttpResponseRedirect(reverse('asset:list'))

#     context = {
#         'assetform': assetform,
#         'attributeform': attributeform,
#         'form_url': reverse_lazy('asset:edit', args=[asset.pk]),
#         'type': "edit",

#     }

#     return render(request, "asset/create.html", context)


# @login_required
# def detail(request, pk):

#     asset = get_object_or_404(Asset, pk=pk)

#     context = {
#         'asset': asset,
#     }

#     return render(request, "asset/detail.html", context)


# @login_required
# def delete(request, pk):
#     if request.method == 'POST':
#         asset = get_object_or_404(Asset, pk=pk)
#         asset.delete()
#         return HttpResponseRedirect(reverse('asset:list'))
